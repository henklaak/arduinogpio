import numpy as np
from PyQt5.QtCore import QTimer, pyqtSlot
from PyQt5.QtGui import QResizeEvent, QShowEvent, QPainter
from PyQt5.QtWidgets import QDialog

from adcdac import AdcDac
from command_calibrate_dialog_ui import Ui_CommandCalibrateDialog
from dialscene import DialScene


class CommandCalibrateDialog(QDialog, Ui_CommandCalibrateDialog):
    def __init__(self,
                 io: AdcDac,
                 board: int,
                 module: int,
                 port: int,
                 center: float,
                 sweep: float,
                 repeat: int,
                 command: str):
        super().__init__()
        self.setupUi(self)
        self._io = io
        self._command = command

        self._values = np.empty((60, 0))

        self._scene = DialScene(center, sweep)
        self._scene.sweepChanged.connect(self.on_params_changed)
        self._scene.centerChanged.connect(self.on_params_changed)

        self.setWindowTitle(self._command)
        self.graphicsView.setScene(self._scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing)

        self._timer = QTimer()
        self._timer.setInterval(100)

        self._timer.timeout.connect(self.on_timer)
        self._timer.start()

        self.board = board
        self.module = module
        self.port = port
        self.repeat = repeat
        self.on_params_changed()

    def showEvent(self, event: QShowEvent):
        self.graphicsView.fitInView(self._scene.top_item)
        super().showEvent(event)

    def resizeEvent(self, event: QResizeEvent):
        self.graphicsView.fitInView(self._scene.top_item)
        super().resizeEvent(event)

    @pyqtSlot()
    def on_timer(self):
        if self.chkAutodetect.isChecked():
            self.autodetect()
        value = self._io.read_adc(self._module, self._port)
        self._scene.value = value

    @pyqtSlot()
    def on_params_changed(self):
        self.lblCentre.setText(f"{self._scene.center:.2f}")
        self.lblSweep.setText(f"{self._scene.sweep:.2f}")

    @property
    def board(self):
        return self._board

    @board.setter
    def board(self, value):
        self._board = value
        self.lblBoard.setText(f"{self._board:d}")

    @property
    def module(self):
        return self._module

    @module.setter
    def module(self, value):
        self._module = value
        self.lblModule.setText(f"{self._module:d}")

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value
        self.lblPort.setText(f"{self._port:d}")

    @property
    def repeat(self):
        return self.chkRepeat.isChecked()

    @repeat.setter
    def repeat(self, value):
        self._repeat = value
        self.chkRepeat.setChecked(value)

    @property
    def center(self):
        return self._scene.center

    @property
    def sweep(self):
        return self._scene.sweep

    @property
    def command(self):
        return self._command

    @property
    def lower_limit(self):
        return self._scene.lower_limit

    @property
    def upper_limit(self):
        return self._scene.upper_limit

    def autodetect(self):
        values = []
        values.extend(self._io.read_adc(0))
        values.extend(self._io.read_adc(1))
        values.extend(self._io.read_adc(2))

        self._values = np.insert(self._values, 0, values, axis=1)
        if self._values.shape[1] > 20:
            self._values = np.delete(self._values, 20, axis=1)
            ptp = np.ptp(self._values, axis=1)
            maxdelta = np.amax(ptp)
            maxpos = np.argmax(ptp)
            if maxdelta > 0.25:
                self.module = maxpos // 20
                self.port = maxpos % 20

from matplotlib.axes import Axes
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class MplWidget(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        super().__init__(fig)
        self.axes = Axes(fig, (0.1,0.1,0.8,0.8))
        fig.add_axes(self.axes)
        self.axes.set_xbound(0, 10)
        self._input_line = self.axes.axvline(x=0.0, color='C0', label="Input")
        # self._output_line = self.axes.axhline(y=0.5, color='C1', label="Output")
        # self.axes.grid()

        self.input_value = 0
        self.axes.legend()

        # self.add_lut(1, "off",
        #              (0.0, 0.5, 0.5, 1.0, 1.0, 10.0),
        #              (0.0, 0.0, 1.0, 1.0, 0.0, 0.0))
        # self.add_lut(2, "on",
        #              (0.0, 1.0, 1.0, 1.5, 1.5, 10.0),
        #              (0.0, 0.0, 1.0, 1.0, 0.0, 0.0))
        self.add_lut(1, "sim/cockpit2/controls/flap_ratio",
                      (0.00, 0.75, 0.95, 1.05, 1.45, 1.55, 2.00, 10.00),
                      (1.00, 1.00, 1.00, 0.50, 0.50, 0.00, 0.00, 0.00))

    #self.output_range = (-1,1)
        self.axes.legend()

    @property
    def input_value(self):
        return self._input_value

    @input_value.setter
    def input_value(self, input_value):
        self._input_value = input_value
        self._input_line.set(xdata=[input_value, input_value])
        self.draw()

    @property
    def output_range(self):
        return self._output_range

    @output_range.setter
    def output_range(self, values):
        self.axes.set_ybound(values[0], values[1])

    def add_lut(self, index, mnem, xdata, ydata):
        self.axes.plot(xdata, ydata, label=mnem, color=f'C{index:d}', alpha=0.8)

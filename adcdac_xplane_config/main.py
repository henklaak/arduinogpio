import sys

from PyQt5.QtWidgets import QApplication
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg

from adcdac import AdcDac, find_adcdac_device_names
from dataref_calibrate_dialog import DatarefCalibrateDialog

if __name__ == '__main__':
    app = QApplication(sys.argv)

    device_names = find_adcdac_device_names()

    io = AdcDac(device_names[0])
    io.connect()

    # Set everything to input
    for module in range(0, 3):
        for port in range(0, 20):
            io.set_port_mode(module, port, io.PORT_MODE_INPUT)

    ####
    # command = "sim/systems/avionics_on"
    # w = CommandCalibrateDialog(io, 1, 0, 1, 0.75, 0.5, 1, command)
    #
    # result = w.exec()
    # if result:
    #     line = ", ".join((f"{w.board:d}",
    #                       f"{w.module:d}",
    #                       f"{w.port:d}",
    #                       f"{w.lower_limit:.2f}",
    #                       f"{w.upper_limit:.2f}",
    #                       f"{w.repeat:d}",
    #                       f"{w.command:s}"))
    #
    #     print(line)

    if 1:
        dataref = r"sim/cockpit2/engine/actuators/throttle_ratio_all"
        w = DatarefCalibrateDialog(io, 1, 0, 0, 1, dataref)
        result = w.exec()
        if result:
            line = ", ".join((f"{w.board:d}",
                              f"{w.module:d}",
                              f"{w.port:d}",
                              f"{w.direction:d}",
                              f"{w.dataref:s}"))
            print(line)

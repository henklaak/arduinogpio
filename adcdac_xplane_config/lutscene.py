from PyQt5.QtCore import QObject, QRectF, Qt, QLineF, QPointF, pyqtSignal
from PyQt5.QtGui import QColor, QPen, QFont
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsRectItem, QGraphicsLineItem, QGraphicsSimpleTextItem


class LutScene(QGraphicsScene):
    centerChanged = pyqtSignal()
    sweepChanged = pyqtSignal()

    def __init__(self, parent: QObject = None):
        super().__init__(parent)

        self._value = 0.0

        self._rc = QGraphicsRectItem(QRectF(-20, -20, 140, 140))
        self._rc.setPen(QPen(Qt.NoPen))

        border = QGraphicsRectItem(QRectF(0,0,100,100), self._rc)
        border.setPen(QPen(QColor('#404040'), 0.5, Qt.SolidLine))

        self.needle = QGraphicsLineItem(QLineF(QPointF(0, 0), QPointF(0, 100)), self._rc)
        self.needle.setPen(QPen(QColor('#404040'), 1.0, Qt.DashLine, Qt.RoundCap))

        self.lut = QGraphicsLineItem(QLineF(QPointF(0, 0), QPointF(100, 0)), self._rc)
        self.lut.setPen(QPen(QColor('#404040'), 1.0, Qt.DashLine, Qt.RoundCap))

        font = QFont()
        font.setPointSize(5)

        label = QGraphicsSimpleTextItem("0", self._rc)
        label.setFont(font)
        bb = label.boundingRect()
        label.setPos(0 - bb.width()/2, 105 - bb.height()/2)

        label = QGraphicsSimpleTextItem("10", self._rc)
        label.setFont(font)
        bb = label.boundingRect()
        label.setPos(100 - bb.width()/2, 105 - bb.height()/2)

        label = QGraphicsSimpleTextItem("0", self._rc)
        label.setFont(font)
        bb = label.boundingRect()
        label.setPos(-10 - bb.width()/2, 100 - bb.height()/2)

        label = QGraphicsSimpleTextItem("10", self._rc)
        label.setFont(font)
        bb = label.boundingRect()
        label.setPos(-10 - bb.width()/2, 0 - bb.height()/2)

        self.addItem(self._rc)

    @property
    def top_item(self):
        return self._rc

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = max(0.0, min(10.0, value))
        self.needle.setPos(QPointF(100 * self._value / 10.0, 0))
        self.lut.setPos(QPointF(0,100-100 * self._value / 10.0))

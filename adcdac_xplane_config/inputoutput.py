import os
from pathlib import Path

import pandas as pd


def find_xplane_base_path() -> Path:
    base_path = None
    candidate_dirs = [r"C:/Program Files (x86)/Steam/steamapps/common/X-Plane 11",
                      r"D:/X-Plane 11",
                      r"/mnt/lvm/HDD/SteamLibrary/steamapps/common/X-Plane 11"]

    candidate_filenames = [r"X-Plane.exe",
                           r"X-Plane-x86_64.exe",
                           r"X-Plane",
                           r"X-Plane-x86_64"]

    for candidate_dir in candidate_dirs:
        candidate_base_path = Path(candidate_dir)
        for candidate_filename in candidate_filenames:
            candidate_file_path = candidate_base_path / candidate_filename
            if os.path.isfile(candidate_file_path):
                base_path = candidate_base_path

    assert base_path is not None

    return base_path


def load_commands() -> pd.DataFrame:
    base_dir = find_xplane_base_path()
    commands_path = base_dir / "Resources" / "plugins" / "Commands.txt"

    with open(commands_path, "rt") as fp:
        lines = [line.strip() for line in fp.readlines()]

    mnems = []
    remarks = []
    for line in lines:
        elems = line.split(maxsplit=1)

        mnems.append(elems[0])
        remarks.append(elems[1])

    df = pd.DataFrame({"mnem": mnems,
                       "remark": remarks})
    return df


def load_datarefs() -> pd.DataFrame:
    base_dir = find_xplane_base_path()
    datarefs_path = base_dir / "Resources" / "plugins" / "DataRefs.txt"

    with open(datarefs_path, "rt") as fp:
        lines = [line.strip() for line in fp.readlines()]

    mnems = []
    types = []
    writeables = []
    units = []
    remarks = []
    for line in lines[2:]:
        elems = line.split("\t", maxsplit=4)

        mnems.append(elems[0])
        types.append(elems[1])
        writeables.append(elems[2])
        units.append(elems[3] if len(elems) >= 4 else None)
        remarks.append(elems[4] if len(elems) >= 5 else None)

    df = pd.DataFrame({"mnem": mnems,
                       "type": types,
                       "writeable": writeables,
                       "unit": units,
                       "remark": remarks})
    return df


def load_commandsmap() -> pd.DataFrame:
    base_dir = find_xplane_base_path()
    commands_path = base_dir / "Resources" / "plugins" / "AdcDacPlugin" / "64" / "commandsmap.txt"

    with open(commands_path, "rt") as fp:
        lines = [line.strip() for line in fp.readlines()]

    hwids = []
    modules = []
    ports = []
    level_los = []
    level_his = []
    repeats = []
    commands = []
    for line in lines:
        if line.startswith('#'):
            continue

        elems = line.split(',')
        if len(elems) != 7:
            continue

        hwids.append(int(elems[0]))
        modules.append(int(elems[1]))
        ports.append(int(elems[2]))
        level_los.append(float(elems[3]))
        level_his.append(float(elems[4]))
        repeats.append(int(elems[5]))
        commands.append(elems[6])

    df = pd.DataFrame({"hwid": hwids,
                       "module": modules,
                       "port": ports,
                       "level_lo": level_los,
                       "level_hi": level_his,
                       "repeats": repeats,
                       "command": commands})
    return df


def load_datarefsmap() -> pd.DataFrame:
    base_dir = find_xplane_base_path()
    commands_path = base_dir / "Resources" / "plugins" / "AdcDacPlugin" / "64" / "datarefsmap.txt"

    with open(commands_path, "rt") as fp:
        lines = [line.strip('\n') for line in fp.readlines()]

    indx = 0

    while indx < len(lines):
        line = lines[indx]
        indx = indx + 1

        if len(line) == 0 or line.startswith(("#", ' ')):
            continue

        print(line)
        while indx < len(lines):
            line = lines[indx]
            indx = indx + 1

            if line.startswith("#"):
                continue

            if len(line) == 0 or not line.startswith(' '):
                break

            print("---")

    df = pd.DataFrame()
    return df

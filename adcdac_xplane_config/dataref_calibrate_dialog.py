import numpy as np
from PyQt5.QtCore import QTimer, pyqtSlot
from PyQt5.QtGui import QShowEvent, QResizeEvent, QPainter
from PyQt5.QtWidgets import QDialog

from adcdac import AdcDac
from dataref_calibrate_dialog_ui import Ui_DatarefCalibrateDialog
from lutscene import LutScene


class DatarefCalibrateDialog(QDialog, Ui_DatarefCalibrateDialog):
    def __init__(self,
                 io: AdcDac,
                 board: int,
                 module: int,
                 port: int,
                 direction: int,
                 dataref: str,
                 index: int = -1):
        super().__init__()
        self.setupUi(self)
        self._io = io
        self._dataref = dataref
        self._values = np.empty((60, 0))

        self.board = board
        self.module = module
        self.port = port
        self.direction = direction

        self.setWindowTitle(self._dataref)

        self._timer = QTimer()
        self._timer.setInterval(100)

        self._timer.timeout.connect(self.on_timer)
        self._timer.start()

    def showEvent(self, event: QShowEvent):
        super().showEvent(event)

    def resizeEvent(self, event: QResizeEvent):
        super().resizeEvent(event)

    @property
    def board(self):
        return self._board

    @board.setter
    def board(self, value):
        self._board = value
        self.lblBoard.setText(f"{self._board:d}")

    @property
    def module(self):
        return self._module

    @module.setter
    def module(self, value):
        self._module = value
        self.lblModule.setText(f"{self._module:d}")

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value
        self.lblPort.setText(f"{self._port:d}")

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._direction = value

    @property
    def dataref(self):
        return self._dataref

    @pyqtSlot()
    def on_timer(self):
        if self.chkAutodetect.isChecked():
            self.autodetect()
        value = self._io.read_adc(self._module, self._port)
        self.widget.input_value = value

    def autodetect(self):
        values = []
        values.extend(self._io.read_adc(0))
        values.extend(self._io.read_adc(1))
        values.extend(self._io.read_adc(2))

        self._values = np.insert(self._values, 0, values, axis=1)
        if self._values.shape[1] > 20:
            self._values = np.delete(self._values, 20, axis=1)
            ptp = np.ptp(self._values, axis=1)
            maxdelta = np.amax(ptp)
            maxpos = np.argmax(ptp)
            if maxdelta > 0.25:
                self.module = maxpos // 20
                self.port = maxpos % 20

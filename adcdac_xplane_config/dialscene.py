from PyQt5.QtCore import QObject, QRectF, Qt, QLineF, QPointF, pyqtSignal
from PyQt5.QtGui import QColor, QBrush, QPen, QKeyEvent
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsRectItem, QGraphicsEllipseItem, QGraphicsLineItem


class DialScene(QGraphicsScene):
    centerChanged = pyqtSignal()
    sweepChanged = pyqtSignal()

    def __init__(self,
                 center: float,
                 sweep: float,
                 parent: QObject = None):
        super().__init__(parent)

        self._value = 0.0
        self._center = center
        self._sweep = sweep

        self._rc = QGraphicsRectItem(QRectF(-100, -100, 200, 200))
        self._rc.setPen(QPen(Qt.NoPen))

        self._arc = QGraphicsEllipseItem(QRectF(-100, -100, 200, 200), self._rc)
        self._arc.setBrush(QBrush(QColor("#00ff00")))
        self._arc.setPen(QPen(Qt.NoPen))
        self._reset_arc()

        self.needle = QGraphicsLineItem(QLineF(QPointF(0, 0), QPointF(90, 0)), self._rc)
        self.needle.setPen(QPen(QColor('#404040'), 3.0, Qt.SolidLine, Qt.RoundCap))

        for i in range(0, 11):
            line = QGraphicsLineItem(QLineF(QPointF(85, 0), QPointF(95, 0)), self._rc)
            line.setRotation(100 + i / 10.0 * 340)

        for i in range(0, 10):
            line = QGraphicsLineItem(QLineF(QPointF(88, 0), QPointF(92, 0)), self._rc)
            line.setRotation(100 + (i + 0.5) / 10.0 * 340)

        for i in range(0, 10):
            line = QGraphicsLineItem(QLineF(QPointF(90, 0), QPointF(91, 0)), self._rc)
            line.setRotation(100 + (i + 0.25) / 10.0 * 340)
            line = QGraphicsLineItem(QLineF(QPointF(90, 0), QPointF(91, 0)), self._rc)
            line.setRotation(100 + (i + 0.75) / 10.0 * 340)

        self.addItem(self._rc)

    @property
    def top_item(self):
        return self._rc

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = max(0.0, min(10.0, value))
        self.needle.setRotation(100 + self._value / 10.0 * 340)

    @property
    def center(self):
        return self._center

    @center.setter
    def center(self, value):
        self._center = max(0.0, min(10.0, value))
        self._reset_arc()
        self.centerChanged.emit()

    @property
    def sweep(self):
        return self._sweep

    @sweep.setter
    def sweep(self, value):
        self._sweep = max(0.1, min(10.0, value))
        self._reset_arc()
        self.sweepChanged.emit()

    @property
    def lower_limit(self):
        return max(0.0, min(10.0, self._center - self._sweep / 2))

    @property
    def upper_limit(self):
        return max(0.0, min(10.0, self._center + self._sweep / 2))

    def _reset_arc(self):
        ang, spn = self._values_to_angle_span(self._center - self._sweep / 2, self._center + self._sweep / 2)
        self._arc.setStartAngle(ang)
        self._arc.setSpanAngle(spn)

    def keyPressEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_Up:
            self.center = self.center + 0.05
        elif event.key() == Qt.Key_Down:
            self.center = self.center - 0.05
        elif event.key() == Qt.Key_Right:
            self.sweep = self.sweep + 0.05
        elif event.key() == Qt.Key_Left:
            self.sweep = self.sweep - 0.05
        super().keyPressEvent(event)

    @staticmethod
    def _values_to_angle_span(v1: float, v2: float):
        v1 = max(0.0, min(10.0, v1))
        v2 = max(0.0, min(10.0, v2))
        angle = -v1 / 10.0 * 340 + 260
        span = (v2 - v1) / 10.0 * -340
        return int(16 * angle), int(16 * span)

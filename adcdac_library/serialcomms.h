#pragma once

#include <string>
#include <memory>
#include <vector>
#include "timer.h"

class SerialComms
{
public:
    virtual ~SerialComms();

    static std::shared_ptr<SerialComms> create( const std::string &deviceName );

    std::string getDeviceName() const
    {
        return m_deviceName;
    }

    virtual bool isOpen() const = 0;
    virtual void open() = 0;
    virtual void close() = 0;

    virtual void read( uint8_t *data, ssize_t length ) const = 0;
    virtual void write( const uint8_t *data, ssize_t length )  const = 0;

protected:
    SerialComms( const std::string &deviceName );
    std::string m_deviceName;
};

std::vector<std::string> findArduinoDeviceNames();


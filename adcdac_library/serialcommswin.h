#ifndef SERIALCOMMSWIN_H
#define SERIALCOMMSWIN_H

#include <windows.h>
#include "serialcomms.h"

class SerialCommsWin : public SerialComms
{
public:
    SerialCommsWin(const std::string &deviceName );
    virtual ~SerialCommsWin();

    bool isOpen() const override;
    void open() override;
    void close() override;

    void read(uint8_t *data, ssize_t length)  const override;
    void write(const uint8_t *data, ssize_t length)  const override;

private:
    HANDLE hComm;
};



#endif // SERIALCOMMSWIN_H

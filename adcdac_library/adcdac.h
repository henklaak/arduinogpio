#pragma once

#include <string>
#include <memory>
#include <thread>
#include <mutex>
#include <stdint.h>
#include <firmware_globals.h>

class SerialComms;

class AdcDac
{
public:
    AdcDac( const std::string &deviceName );
    virtual ~AdcDac();

    double getPeriod() const noexcept;
    uint32_t getHeartbeat() const noexcept;

    std::string getDeviceName() const noexcept;
    bool isConnected() const noexcept;

    std::string getFwVersion() noexcept;
    bool isFwVersionOk() noexcept;

    void fwReset() noexcept;
    void clearWarnings() noexcept;

    int getIdSelector() const noexcept;
    float getSupplyVoltage( int cs ) const noexcept ;
    float getSupplyCurrent( int cs ) const noexcept ;
    float get10VReferenceVoltage( ) const noexcept;
    float get12VSupplyVoltage( ) const noexcept;
    float getInternalTemperature( int cs ) const noexcept;
    bool getOverTemperatureState( int cs ) const noexcept;
    bool getUnderTemperatureState( int cs ) const noexcept;
    bool getOverCurrentState( int cs ) const noexcept;
    bool getPortOverCurrentState( int cs, int port ) const noexcept;
    int getAnalogInputValue( int cs, int port ) const noexcept;
    int getAnalogOutputValue( int cs, int port ) const noexcept;

    void setAnalogOutputValue( int cs, int port, int value ) noexcept;
    void setPortConfig( int cs, int port, int portConfig ) noexcept;
    uint16_t readRegister( int cs, int port ) noexcept;


private:
    void bg_pollMax() noexcept;

    void bg_getFwVersion();
    void bg_writeReset();
    void bg_writeClearWarnings();

    void bg_readIdSelector();
    void bg_readStates();
    void bg_readOcStates();
    void bg_readSupplies();
    void bg_readTemperatures();
    void bg_readAnalogInputs( uint8_t cs );
    void bg_setAnalogOutput();

    void bg_writePortConfig();

    void bg_readRegister();
    void bg_writeRegister();

    void bg_readHeartbeat();

    std::thread m_pollThread;

    std::shared_ptr<SerialComms> m_serial;
    std::string m_deviceName;
    bool m_running = false;
    bool m_stopRequested = false;
    double m_period = 0;
    uint32_t m_heartbeat = 0;

    union MaxStatus
    {
        struct
        {
            uint16_t dummy0: 5;
            uint16_t overcurrent: 1;
            uint16_t dummy1: 1;
            uint16_t temperature_low: 1;
            uint16_t temperature_high: 1;
            uint16_t dummy2: 6;
            uint16_t supply_low: 1;
        } b;
        uint16_t w;
    };

    // Periodic values
    MaxStatus m_maxstatus[NR_MAX_MODULES] = {};

    int8_t m_idSelector = -1;

    uint16_t m_supplyVoltage[NR_MAX_MODULES];
    uint16_t m_supplyCurrent[NR_MAX_MODULES];
    uint16_t m_supply10V;
    uint16_t m_supply12V;

    uint16_t m_temperature[NR_MAX_MODULES];

    uint8_t m_portMode[NR_MAX_MODULES][NR_PORTS];
    uint16_t m_analogInput[NR_MAX_MODULES][NR_PORTS] = {};
    uint16_t m_analogOutput[NR_MAX_MODULES][NR_PORTS] = {};
    uint32_t m_analogOutputDirty[NR_MAX_MODULES] = {};

    uint32_t m_overCurrent[NR_MAX_MODULES];

    // Request flags and results
    bool m_requestWriteReset = false;

    bool m_requestClearWarnings = false;

    bool m_requestGetFwVersion = false;
    std::string m_fwVersion;

    uint8_t m_writePortConfigRequest[3];
    bool m_requestWritePortConfig = false;

    uint8_t m_writeRegRequest[4];
    bool m_requestWriteReg = false;

    bool m_requestReadReg = false;
    uint8_t m_readRegRequest[4];
};


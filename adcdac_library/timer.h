#pragma once

#include <chrono>

class Timer
{
public:
    Timer();
    virtual ~Timer();

    void restart();
    unsigned long elapsed_s( bool restart = false )  noexcept;
    unsigned long elapsed_ms( bool restart = false )  noexcept;
    unsigned long elapsed_us( bool restart = false )  noexcept;

    static void sleep_s( unsigned long );
    static void sleep_ms( unsigned long );
    static void sleep_us( unsigned long );

private:
    std::chrono::high_resolution_clock::time_point m_start;
};


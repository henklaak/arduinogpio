#include "adcdac.h"
#include <thread>
#include <filesystem>
#include <iostream>
#include "serialcomms.h"
#include "timer.h"

/**************************************************************************************************/
void verify( bool condition, std::string msg )
{
    if( !condition )
    {
        throw std::runtime_error( msg );
    }
}

/**************************************************************************************************/
AdcDac::AdcDac( const std::string &deviceName )
    : m_deviceName( deviceName )
    , m_supplyVoltage{}
    , m_supplyCurrent{}
    , m_supply10V( 0.0 )
    , m_supply12V( 0.0 )
    , m_temperature{}
    , m_portMode{}
    , m_analogInput{}
    , m_analogOutput{}
    , m_overCurrent{}
{
    m_stopRequested = false;
    m_pollThread = std::thread( &AdcDac::bg_pollMax, this );
}

/**************************************************************************************************/
AdcDac::~AdcDac()
{
    m_stopRequested = true;
    m_pollThread.join();
}

/**************************************************************************************************/
/* Public                                                                                         */
/**************************************************************************************************/


/**************************************************************************************************/
std::string AdcDac::getDeviceName() const noexcept
{
    if( m_serial )
    {
        return m_serial->getDeviceName();
    }
    else
    {
        return m_deviceName;
    }
}

/**************************************************************************************************/
bool AdcDac::isConnected() const noexcept
{
    return m_serial && m_serial->isOpen() && !m_stopRequested;
}

/**************************************************************************************************/
void AdcDac::fwReset() noexcept
{
    m_requestWriteReset = true;

    Timer timeout;

    while( !isConnected() )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

        if( timeout.elapsed_ms() > 1000 )
        {
            std::cout << "Timeout in reset()" << std::endl;
            break;
        }
    }

    timeout.restart();

    while( isConnected() && m_requestWriteReset )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

        if( timeout.elapsed_ms() > 1000 )
        {
            std::cout << "Timeout in reset()" << std::endl;
            break;
        }
    }

    m_requestWriteReset = false;
}

/**************************************************************************************************/
void AdcDac::clearWarnings() noexcept
{
    m_requestClearWarnings = true;

    Timer timeout;

    while( isConnected() && m_requestClearWarnings )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

        if( timeout.elapsed_ms() > 1000 )
        {
            std::cout << "Timeout in clearWarnings()" << std::endl;
            break;
        }
    }

    m_requestClearWarnings = false;
}

/**************************************************************************************************/
int AdcDac::getIdSelector() const noexcept
{
    return m_idSelector;
}

/**************************************************************************************************/
uint32_t AdcDac::getHeartbeat() const noexcept
{
    return m_heartbeat;
}

/**************************************************************************************************/
float AdcDac::getSupplyVoltage( int cs ) const noexcept
{
    const float VOLTAGE_REF = 2.5;
    const float R_PORTS = 1 / ( 1 / 20.5 + 1 / 20.5 );
    const float SCALE = VOLTAGE_REF * ( 2.05 + R_PORTS ) / ( 2.05 ) / 4096.0;

    float value = m_supplyVoltage[cs] * SCALE;
    return value;
}
/**************************************************************************************************/
float AdcDac::getSupplyCurrent( int cs ) const noexcept
{
    const float SCALE = 1000.0 / 4096.0;

    float value = m_supplyCurrent[cs] * SCALE;
    return value;
}
/**************************************************************************************************/
float AdcDac::get10VReferenceVoltage( ) const noexcept
{
    const float VOLTAGE_REF = 2.5;
    const float R_10V = 1 / ( 1 / 30.1 + 1 / 30.1 + 1 / 30.1 );
    const float SCALE = VOLTAGE_REF * ( 2.05 + R_10V ) / ( 2.05 ) / 4096.0;

    float value = m_supply10V * SCALE;
    return value;
}
/**************************************************************************************************/
float AdcDac::get12VSupplyVoltage( ) const noexcept
{
    const float VOLTAGE_REF = 2.5;
    const float R_12V = 10.0;
    const float SCALE = VOLTAGE_REF * ( 2.05 + R_12V ) / ( 2.05 ) / 4096.0;

    float value = m_supply12V * SCALE;
    return value;
}

/**************************************************************************************************/
float AdcDac::getInternalTemperature( int cs ) const noexcept
{
    float value = m_temperature[cs] / 8.0;
    return value;
}

/**************************************************************************************************/
bool AdcDac::getOverTemperatureState( int cs ) const noexcept
{
    return m_maxstatus[cs].b.temperature_high;
}
/**************************************************************************************************/
bool AdcDac::getUnderTemperatureState( int cs ) const noexcept
{
    return m_maxstatus[cs].b.temperature_low;
}
/**************************************************************************************************/
bool AdcDac::getOverCurrentState( int cs ) const noexcept
{
    return m_maxstatus[cs].b.overcurrent;
}
/**************************************************************************************************/
bool AdcDac::getPortOverCurrentState( int cs,
                                      int port ) const noexcept
{
    return  m_overCurrent[cs] & ( 1 << port );
}

/**************************************************************************************************/
int AdcDac::getAnalogInputValue( int cs,
                                 int port ) const noexcept
{
    uint16_t value = m_analogInput[cs][port];
    return value;
}

/**************************************************************************************************/
int AdcDac::getAnalogOutputValue( int cs,
                                  int port ) const noexcept
{
    uint16_t value = m_analogOutput[cs][port];
    return value;
}

/**************************************************************************************************/
std::string AdcDac::getFwVersion() noexcept
{
    if( m_fwVersion.empty() )
    {
        m_requestGetFwVersion = true;

        Timer timeout;

        while( isConnected() && m_requestGetFwVersion )
        {
            std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

            if( timeout.elapsed_ms() > 1000 )
            {
                std::cout << "Timeout in getVersion()" << std::endl;
                break;
            }
        }

        m_requestGetFwVersion = false;
    }

    return m_fwVersion;
}

/**************************************************************************************************/
bool AdcDac::isFwVersionOk() noexcept
{
    std::string get = getFwVersion();
    std::string set = FWVERSION_STR;
    auto invalid = get.compare( set );
    return !invalid;
}

/**************************************************************************************************/
void AdcDac::setAnalogOutputValue( int cs,
                                   int port,
                                   int value ) noexcept
{
    m_analogOutput[cs][port] = std::max( 0, std::min( 4095, value ) );
    m_analogOutputDirty[cs] |= ( 1 << port );
}

/**************************************************************************************************/
void AdcDac::setPortConfig( int cs,
                            int port,
                            int portConfig ) noexcept
{
    m_writePortConfigRequest[0] = cs;
    m_writePortConfigRequest[1] = port;
    m_writePortConfigRequest[2] = portConfig;

    m_requestWritePortConfig = true;

    Timer timeout;

    while( isConnected() && m_requestWritePortConfig )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

        if( timeout.elapsed_ms() > 1000 )
        {
            std::cout << "Timeout in setPortConfig()" << std::endl;
            break;
        }
    }

    m_requestWritePortConfig = false;
}

/**************************************************************************************************/
uint16_t AdcDac::readRegister( int cs,
                               int port ) noexcept
{
    m_readRegRequest[0] = cs;
    m_readRegRequest[1] = port;
    m_requestReadReg = true;

    Timer timeout;

    while( isConnected() && m_requestReadReg )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );

        if( timeout.elapsed_ms() > 1000 )
        {
            std::cout << "Timeout in readRegister()" << std::endl;
            break;
        }
    }

    m_requestReadReg = false;

    uint16_t lsb = m_readRegRequest[2];
    uint16_t msb = m_readRegRequest[3];
    uint16_t value = ( msb << 8 ) | lsb;
    return value;
}

/**************************************************************************************************/
double AdcDac::getPeriod() const noexcept
{
    return m_period;
}

/**************************************************************************************************/
/* Private                                                                                        */
/**************************************************************************************************/

/**************************************************************************************************/
void AdcDac::bg_pollMax() noexcept
{
    Timer periodMeasurement;
    Timer lowfreq;
    bool first = true;

    while( !m_stopRequested )
    {
        m_running = true;

        try
        {
            if( !( m_serial && m_serial->isOpen() ) )
            {
                m_serial.reset();
                m_serial = SerialComms::create( m_deviceName );
                m_serial->open();
                m_requestWriteReset = true;
                m_requestGetFwVersion = true;

                try
                {
                    m_fwVersion = "Unknown";
                    bg_getFwVersion();
                    bg_readIdSelector();
                }
                catch( const std::exception &e )
                {
                    m_stopRequested = true; // no point in retrying
                    throw std::runtime_error( "Could not determine firmware version" );
                }

                if( m_fwVersion.compare( FWVERSION_STR ) )
                {
                    m_stopRequested = true; // no point in retrying
                    throw std::runtime_error( "Wrong firmware version" );
                }
            }
            else
            {
                bg_writeReset();
                bg_writeClearWarnings();

                if( first or lowfreq.elapsed_ms() > 200 )
                {
                    bg_readIdSelector();
                    bg_readStates();
                    bg_readOcStates();
                    bg_readSupplies();
                    bg_readTemperatures();
                    lowfreq.restart();
                }

                bg_readAnalogInputs( 0 );
                bg_readAnalogInputs( 1 );
                bg_readAnalogInputs( 2 );
                bg_writePortConfig();
                bg_setAnalogOutput();
                bg_writeRegister();
                bg_readRegister();
                bg_readHeartbeat();

                double newperiod = periodMeasurement.elapsed_us( true ) / 1000000.0;
                m_period = ( 9 * m_period  + newperiod ) / 10.0; // low pass average

                first = false;
            }
        }
        catch( std::exception &e )
        {
            std::cerr  << e.what() << std::endl;
            m_serial->close();
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
        }
        catch( ... )
        {
            std::cerr  << "Unknown exception" << std::endl;
            m_serial->close();
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
        }
    }

    if( m_serial && m_serial->isOpen() )
    {
        m_serial->close();
    }

    m_running = false;
}

/**************************************************************************************************/
void AdcDac::bg_getFwVersion()
{
    if( m_requestGetFwVersion )
    {
        uint8_t cmd[1] = {CMD_GET_VERSION}; // 12
        m_serial->write( cmd, 1 );

        uint8_t ans[34] = {0xff};
        m_serial->read( ans, 2 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_getFwVersion()" );

        uint8_t len = ans[1];
        m_serial->read( ans + 2, len );

        ans[2 + len] = 0;
        m_fwVersion = std::string( ( char * )( ans + 2 ) );

        m_requestGetFwVersion = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_writeReset()
{
    if( m_requestWriteReset )
    {
        uint8_t cmd[1] = {CMD_RESET}; // 0
        m_serial->write( cmd, 1 );

        uint8_t ans[1] = {0xff};
        m_serial->read( ans, 1 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_writeReset()" );

        m_requestWriteReset = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_readIdSelector()
{
    uint8_t cmd[1] = {CMD_ID_SELECTOR}; // 1
    m_serial->write( cmd, 1 );

    uint8_t ans[2] = {0xff};
    m_serial->read( ans, 2 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readIdSelector()" );

    m_idSelector = ans[1];
}

/**************************************************************************************************/
void AdcDac::bg_readAnalogInputs( uint8_t cs )
{
    uint8_t cmd[2] = {CMD_ADC_DATA, 0x00}; // 2
    cmd[1] = cs;
    m_serial->write( cmd, 2 );

    uint8_t ans[45] = {0xff};
    m_serial->read( ans, 5 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readAnalogInputs()" );
    verify( cmd[1] == ans[1], "Incorrect reply in bg_readAnalogInputs()" );
    ans[5] = 0;

    uint32_t mask = *( uint32_t * )( ans + 2 );

    uint8_t idx = 5;
    bool odd = true;

    for( int i = 0; i < NR_PORTS; ++i )
    {
        if( mask & ( 1 << i ) )
        {
            if( odd )
            {
                m_serial->read( ans + idx, 1 );
                m_analogInput[cs][i] = ( ans[idx] << 4 ) + ( ans[idx - 1] >> 4 );
                idx += 1;
                odd = false;
            }
            else
            {
                m_serial->read( ans + idx, 2 );
                m_analogInput[cs][i] = ans[idx]  + ( ( ans[idx + 1] & 0x0f ) << 8 );
                idx += 2;
                odd = true;
            }
        }
    }
}

/**************************************************************************************************/
void AdcDac::bg_readSupplies()
{
    uint8_t cmd[1] = {CMD_SUPPLIES}; // 3
    m_serial->write( cmd, 1 );

    uint8_t ans[17] = {0xff};
    m_serial->read( ans, 17 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readSupplies()" );

    m_supplyVoltage[0] = *( uint16_t * )( ans + 1 );
    m_supplyVoltage[1] = *( uint16_t * )( ans + 3 );
    m_supplyVoltage[2] = *( uint16_t * )( ans + 5 );
    m_supplyCurrent[0]  = *( uint16_t * )( ans + 7 );
    m_supplyCurrent[1]  = *( uint16_t * )( ans + 9 );
    m_supplyCurrent[2]  = *( uint16_t * )( ans + 11 );
    m_supply10V  = *( uint16_t * )( ans + 13 );
    m_supply12V  = *( uint16_t * )( ans + 15 );
}

/**************************************************************************************************/
void AdcDac::bg_readTemperatures()
{
    uint8_t cmd[1] = {CMD_TEMPERATURES}; // 4
    m_serial->write( cmd, 1 );

    uint8_t ans[17] = {0xff};
    m_serial->read( ans, 7 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readTemperatures()" );

    m_temperature[0] = *( uint16_t * )( ans + 1 );
    m_temperature[1] = *( uint16_t * )( ans + 3 );
    m_temperature[2] = *( uint16_t * )( ans + 5 );
}

/**************************************************************************************************/
void AdcDac::bg_readStates()
{
    uint8_t cmd[1] = {CMD_STATE}; // 5
    m_serial->write( cmd, 1 );

    uint8_t ans[17] = {0xff};
    m_serial->read( ans, 7 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readStates()" );

    m_maxstatus[0].w = *( uint16_t * )( ans + 1 );
    m_maxstatus[1].w = *( uint16_t * )( ans + 3 );
    m_maxstatus[2].w = *( uint16_t * )( ans + 5 );
}

/**************************************************************************************************/
void AdcDac::bg_readOcStates()
{
    uint8_t cmd[1] = {CMD_OC_STATE}; // 6
    m_serial->write( cmd, 1 );

    uint8_t ans[13] = {0xff};
    m_serial->read( ans, 13 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_readOcStates()" );

    m_overCurrent[0] = *( uint32_t * )( ans + 1 );
    m_overCurrent[1] = *( uint32_t * )( ans + 5 );
    m_overCurrent[2] = *( uint32_t * )( ans + 9 );
}

/**************************************************************************************************/
void AdcDac::bg_writePortConfig( )
{
    if( m_requestWritePortConfig )
    {
        uint8_t cs = m_writePortConfigRequest[0];
        uint8_t port = m_writePortConfigRequest[1];
        uint8_t portConfig = m_writePortConfigRequest[2];
        uint8_t cmd[3] = {CMD_SET_PORTMODE, cs, 0}; // 8
        cmd[2] = port + ( portConfig << 6 );
        m_serial->write( cmd, 3 );

        uint8_t ans[3] = {0xff};
        m_serial->read( ans, 1 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_writePortConfig()" );

        m_requestWritePortConfig = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_setAnalogOutput( )
{
    // Check if we have dirties....
    for( uint8_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        if( m_analogOutputDirty[cs] != 0x0000 )
        {
            uint32_t mask = m_analogOutputDirty[cs];
            m_analogOutputDirty[cs] = 0x0000;

            uint8_t mask_lo = ( mask >>  0 ) & 0xff;
            uint8_t mask_mi = ( mask >>  8 ) & 0xff;
            uint8_t mask_hi = ( mask >> 16 ) & 0x0f;

            uint8_t cmd[35] = {CMD_DAC_DATA, cs, mask_lo, mask_mi, mask_hi};

            uint8_t idx = 5;
            bool odd = true;

            for( int i = 0; i < NR_PORTS; ++i )
            {
                if( mask & ( 1 << i ) )
                {
                    uint16_t value = m_analogOutput[cs][i];

                    if( odd )
                    {
                        cmd[idx - 1] |= ( value & 0xf ) << 4;
                        cmd[idx]   = ( ( value >> 4 ) & 0xff );
                        idx += 1;
                        odd = false;
                    }
                    else
                    {
                        cmd[idx]   = ( value & 0xff );
                        cmd[idx + 1] = ( value >> 8 ) & 0xf;
                        idx += 2;
                        odd = true;
                    }
                }
            }

            m_serial->write( cmd, idx );

            uint8_t ans[5] = {0xff};
            m_serial->read( ans, 1 );
            verify( cmd[0] == ans[0], "Incorrect reply in bg_writePortConfig()" );

            ans[0] = 0;
        }
    }
}

/**************************************************************************************************/
void AdcDac::bg_writeRegister( )
{
    if( m_requestWriteReg )
    {
        uint8_t cs = m_writeRegRequest[0];
        uint8_t reg = m_writeRegRequest[1];
        uint8_t cmd[5] = {CMD_WRITE_REG, cs, reg, 0, 0}; // 9
        cmd[3] = m_writeRegRequest[2];
        cmd[4] = m_writeRegRequest[3];
        m_serial->write( cmd, 5 );

        uint8_t ans[5] = {0xff};
        m_serial->read( ans, 1 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_writeRegister()" );

        m_requestWriteReg = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_readRegister( )
{
    if( m_requestReadReg )
    {
        uint8_t cs = m_readRegRequest[0];
        uint8_t reg = m_readRegRequest[1];
        uint8_t cmd[3] = {CMD_READ_REG, cs, reg}; // 7
        m_serial->write( cmd, 3 );

        uint8_t ans[3] = {0xff};
        m_serial->read( ans, 3 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_readRegister()" );
        m_readRegRequest[2] = ans[1];
        m_readRegRequest[3] = ans[2];

        m_requestReadReg = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_writeClearWarnings()
{
    if( m_requestClearWarnings )
    {
        uint8_t cmd[1] = {CMD_CLEAR_WARNINGS}; // 11
        m_serial->write( cmd, 1 );

        uint8_t ans[1] = {0xff};
        m_serial->read( ans, 1 );
        verify( cmd[0] == ans[0], "Incorrect reply in bg_writeClearWarnings()" );
        m_requestClearWarnings = false;
    }
}

/**************************************************************************************************/
void AdcDac::bg_readHeartbeat()
{
    uint8_t cmd[1] = {CMD_GET_HEARTBEAT}; // 13
    m_serial->write( cmd, 1 );

    uint8_t ans[5] = {0xff};
    m_serial->read( ans, 5 );
    verify( cmd[0] == ans[0], "Incorrect reply in bg_writeClearWarnings()" );
    m_heartbeat = *( uint32_t * )( ans + 1 );
}



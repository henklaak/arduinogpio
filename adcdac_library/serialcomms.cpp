#include "serialcomms.h"
#include <memory>
#include <filesystem>
#include <iostream>

#ifdef __linux__
#include "serialcommslinux.h"
#endif

#ifdef _WIN32
#include "serialcommswin.h"
#endif

/**************************************************************************************************/
std::shared_ptr<SerialComms> SerialComms::create( const std::string &commPort )
{
#ifdef __linux__
    std::shared_ptr<SerialCommsLinux> ptr;
    ptr.reset( new SerialCommsLinux( commPort ) );
    return ptr;
#endif
#ifdef _WIN32
    std::shared_ptr<SerialCommsWin> ptr;
    ptr.reset( new SerialCommsWin( commPort ) );
    return ptr;
#endif
    return nullptr;
}

/**************************************************************************************************/
SerialComms::SerialComms( const std::string &deviceName )
    : m_deviceName( deviceName )
{
}

/**************************************************************************************************/
SerialComms::~SerialComms()
{
}

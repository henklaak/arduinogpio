#include "serialcommswin.h"
#include <cassert>
#include <unistd.h>
#include <thread>
#include <chrono>
#include <timer.h>
#include <winbase.h>
#include <direct.h>
#include <string>
#include <iostream>
#include <setupapi.h>
#include <initguid.h>
#include <devguid.h>
#include <devpkey.h>
#include <cwchar>
#include <string>

/******************************************************************************/
SerialCommsWin::SerialCommsWin( const std::string &deviceName )
    : SerialComms(deviceName)
    , hComm(INVALID_HANDLE_VALUE)
{
}

/******************************************************************************/
SerialCommsWin::~SerialCommsWin()
{
}


/******************************************************************************/
bool SerialCommsWin::isOpen() const
{
    return hComm != INVALID_HANDLE_VALUE;
}

/******************************************************************************/
void SerialCommsWin::open()
{
    hComm = CreateFileA( m_deviceName.c_str(),
                         GENERIC_READ | GENERIC_WRITE,
                         0,
                         NULL,
                         OPEN_EXISTING,
                         0,
                         NULL );

    if( hComm == INVALID_HANDLE_VALUE )
    {
        throw std::runtime_error( "Cannot open device " + m_deviceName );
    }

    DCB dcb;
    GetCommState( hComm, &dcb );
    dcb.BaudRate = 115200;
    dcb.ByteSize = 8;
    dcb.StopBits = ONESTOPBIT;
    dcb.Parity = NOPARITY;
    SetCommState( hComm, &dcb );

    COMMTIMEOUTS commtimeouts{};

    GetCommTimeouts(hComm, &commtimeouts);
    commtimeouts.ReadTotalTimeoutConstant=1000;
    commtimeouts.ReadTotalTimeoutMultiplier=1;
    commtimeouts.ReadIntervalTimeout=1;
    commtimeouts.WriteTotalTimeoutConstant=1000;
    commtimeouts.WriteTotalTimeoutMultiplier=1;
    assert(SetCommTimeouts(hComm, &commtimeouts));
}

/******************************************************************************/
void SerialCommsWin::close()
{
    CloseHandle( hComm );
    hComm = INVALID_HANDLE_VALUE;
}

/******************************************************************************/
void SerialCommsWin::read(uint8_t *data, ssize_t length) const
{
    ssize_t received = 0;

    Timer timer;
    while( timer.elapsed_ms() < 1000 )
    {
        DWORD cnt=0;
        bool res = ReadFile(hComm, data+received, length-received, &cnt, nullptr);
        if (!res)
        {
            throw std::runtime_error( "Serial read error");
        }

        received += cnt;

        if( received == length )
        {
            break;
        }
    }

    if( received != length )
    {
        throw std::runtime_error( "Received only " + std::to_string( received ) + " of " + std::to_string(
                                      length ) + " bytes" );
    }
}

/******************************************************************************/
void SerialCommsWin::write(const uint8_t *data, ssize_t length) const
{
    ssize_t sent = 0;

    Timer timer;
    while( timer.elapsed_ms() < 1000 )
    {
        DWORD cnt;
        bool res = WriteFile( hComm, data+sent, length-sent, &cnt, NULL );
        if (!res)
        {
            throw std::runtime_error( "Serial write error");
        }
        {
            sent += cnt;
        }
        if( sent == length )
        {
            break;
        }
    }
    if( sent != length )
    {
        throw std::runtime_error( "Sent only " + std::to_string( sent ) + " of " + std::to_string(
                                      length ) + " bytes" );
    }
}


/**************************************************************************************************/
std::vector<std::string> findArduinoDeviceNames()
{
    std::vector<std::string> deviceNames;

    HDEVINFO  hdevinfo;
    SP_DEVINFO_DATA deviceinfo{};
    DEVPROPTYPE type;

    deviceinfo.cbSize = sizeof(SP_DEVINFO_DATA);

    hdevinfo = SetupDiGetClassDevs(&GUID_DEVCLASS_PORTS, NULL, NULL, DIGCF_PRESENT);

    DWORD index=0;
    unsigned char data[512];

    while(SetupDiEnumDeviceInfo(hdevinfo,
                                 index++,
                                 &deviceinfo))
    {
        DWORD len=0;

        if (!SetupDiGetDevicePropertyW(hdevinfo,
                                 &deviceinfo,
                                 &DEVPKEY_Device_HardwareIds,
                                 &type,
                                 data,
                                 512,
                                  &len,
                                  0))
        {
            continue;
        }

        wchar_t *hardwareids= (wchar_t *)data;
        if (!wcsstr(hardwareids, L"VID_2341")) //2341 = Arduino vendor id
        {
            continue;
        }
        if (!wcsstr(hardwareids, L"PID_8057") && !wcsstr(hardwareids, L"PID_0057")) //8057 = Arduino Nano 3 IoT
        {
            continue;
        }
        if (!SetupDiGetDevicePropertyW(hdevinfo,
                                 &deviceinfo,
                                 &DEVPKEY_Device_FriendlyName,
                                 &type,
                                 data,
                                 512,
                                  &len,
                                  0))
        {
            continue;
        }

        wchar_t *friendlyName = (wchar_t *)data;

        wchar_t *comstr = wcsstr(friendlyName, L"(COM");
        if (!comstr)
        {
            continue;
        }

        wchar_t * pEnd;
        unsigned long nr = wcstol(comstr+4, &pEnd, 10);

        if (nr == 0 || pEnd[0] != L')')
        {
            continue;
        }

        std::string ans = std::string("\\\\.\\COM") + std::to_string(nr);
        deviceNames.push_back(ans);
    }

    return deviceNames;
}

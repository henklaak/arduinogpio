#pragma once

#include "serialcomms.h"
#include <string>
#include <vector>

class SerialCommsLinux : public SerialComms
{
public:
    SerialCommsLinux( const std::string &deviceName );
    virtual ~SerialCommsLinux();

    bool isOpen() const override;
    void open()  override;
    void close() override;

    void read( uint8_t *data, ssize_t length )  const override;
    void write( const uint8_t *data, ssize_t length )  const override;


private:
    int m_fd;
};

std::vector<std::string> findArduinoDeviceNames();


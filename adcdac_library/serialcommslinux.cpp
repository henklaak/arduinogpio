#include "serialcommslinux.h"
#include <fcntl.h>
#include <poll.h>
#include <termios.h>
#include <unistd.h>
#include <cassert>
#include <filesystem>
#include <sys/ioctl.h>
#include "timer.h"

namespace stdfs = std::filesystem;

/**************************************************************************************************/
SerialCommsLinux::SerialCommsLinux( const std::string &deviceName )
    : SerialComms( deviceName )
    , m_fd( -1 )

{
}

/**************************************************************************************************/
SerialCommsLinux::~SerialCommsLinux()
{
    if( m_fd >= 0 )
    {
        ::close( m_fd );
        m_fd = -1;
    }
}

/**************************************************************************************************/
bool SerialCommsLinux::isOpen() const
{
    return ( m_fd >= 0 );
}

/**************************************************************************************************/
void SerialCommsLinux::open()
{
    m_fd = ::open( m_deviceName.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK );
    if( m_fd < 0 )
    {
        throw std::runtime_error( "Cannot open device " + m_deviceName );
    }

    struct termios tio {};
    tcgetattr( m_fd, &tio );

    //configure 115k2 N81 (ACM has no real baudrate, USB speed 12 Mbps is used
    tio.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
    tio.c_iflag = IGNPAR;
    tio.c_oflag = 0;
    tio.c_lflag = 0;
    tio.c_cc[VTIME] = 20;  // timeout after 2 seconds
    tio.c_cc[VMIN] = 1;
    tcsetattr( m_fd, TCSANOW, &tio );
    tcflush( m_fd, TCIOFLUSH );
}


/**************************************************************************************************/
void SerialCommsLinux::close()
{
    ::close( m_fd );
    m_fd = -1;
}

/**************************************************************************************************/
void SerialCommsLinux::read( uint8_t *data, ssize_t length ) const
{
    ssize_t received = 0;

    Timer timer;
    while( timer.elapsed_ms() < 1000 )
    {
        fd_set fds;
        FD_ZERO( &fds );
        FD_SET( m_fd, &fds );

        struct timeval timeout = { 1, 0 };
        int ret = select( m_fd + 1, &fds, NULL, NULL, &timeout );
        if( ret > 0 )
        {
            received += ::read( m_fd, data + received, length - received );
            if( received == length )
            {
                break;
            }
        }
    }

    if( received != length )
    {
        throw std::runtime_error( "Received only " + std::to_string( received ) + " of " + std::to_string(
                                      length ) + " bytes" );
    }
}

/**************************************************************************************************/
void SerialCommsLinux::write( const uint8_t *data, ssize_t length ) const
{
    ssize_t sent = 0;

    Timer timer;
    while( timer.elapsed_ms() < 1000 )
    {
        assert( isOpen() );

        ssize_t res = ::write( m_fd, data + sent, length - sent );
        if( res < 0 )
        {
            if( errno != EAGAIN )
            {
                throw std::runtime_error( "Serial write failed" );
            }
        }
        else
        {
            sent += res;
        }
        if( sent == length )
        {
            break;
        }
    }
    if( sent != length )
    {
        throw std::runtime_error( "Sent only " + std::to_string( sent ) + " of " + std::to_string(
                                      length ) + " bytes" );
    }
}


/**************************************************************************************************/
std::vector<std::string> findArduinoDeviceNames()
{
    std::vector<std::string> deviceNames;

    const std::string path = "/dev/serial/by-id";
    if( stdfs::directory_entry( path ).exists() )
    {
        for( const auto & file : stdfs::directory_iterator( path ) )
        {
            const std::string s = "Arduino";
            if( file.path().generic_string().find( s ) != std::string::npos )
            {
                std::string deviceName = file.path().generic_string();
                if( stdfs::is_symlink( file ) )
                {
                    auto symlink = stdfs::read_symlink( file );
                    auto deviceName = file.path().parent_path();
                    deviceName /= symlink;
                    deviceName = stdfs::canonical( deviceName );
                    if( stdfs::exists( deviceName ) )
                    {
                        deviceNames.push_back( deviceName );
                    }
                }
                else
                {
                    deviceNames.push_back( deviceName );
                }
            }
        }
    }

    return deviceNames;
}

#pragma once

#include <QWidget>
#include <QObject>
#include <QPen>
class ForceModel;

class WidgetAilEle : public QWidget
{
    Q_OBJECT
public:
    WidgetAilEle(QWidget *parent= nullptr);
    void paintEvent( QPaintEvent *event ) override;
    void mouseMoveEvent( QMouseEvent *event ) override;
    void mousePressEvent( QMouseEvent *event ) override;
    void mouseReleaseEvent( QMouseEvent *event ) override;

    void setForceModel(ForceModel *force_model);

private:
    QPointF v2p(QPointF voltage);
    QPointF p2v(QPointF pix);

    void updateSetpoints( QMouseEvent *event );

    ForceModel *m_force_model=nullptr;
    QPen m_penGrid;
    QPen m_penPos;
    QPen m_penAutopilot;
    QPen m_penTrim;
};


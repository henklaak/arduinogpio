#include "widgetrud.h"
#include <QPainter>
#include <QMouseEvent>
#include "forcemodel.h"

/**************************************************************************************************/
WidgetRud::WidgetRud( QWidget *parent )
    : QWidget( parent )
{
    m_penGrid.setColor( QColor::fromRgb( 192, 192, 192 ) );
    m_penGrid.setCosmetic( true );
    m_penGrid.setWidthF( 0.2 );

    m_penPos.setColor( QColor::fromRgb( 0, 192, 0 ) );
    m_penPos.setCosmetic( true );
    m_penPos.setWidth( 2 );

    m_penAutopilot.setColor( QColor::fromRgb( 255, 0, 0 ) );
    m_penAutopilot.setCosmetic( true );
    m_penAutopilot.setWidth( 2 );

    m_penTrim.setColor( QColor::fromRgb( 128, 128, 255 ) );
    m_penTrim.setCosmetic( true );
    m_penTrim.setWidth( 2 );
}

/**************************************************************************************************/
void WidgetRud::setForceModel( ForceModel *force_model )
{
    m_force_model = force_model;
}

/**************************************************************************************************/
QPointF WidgetRud::v2p( QPointF value )
{
    QPoint p;
    p.setX( ( value.x() + 1 ) / 2 * width() );
    p.setY( ( value.y() + 1 ) / 2 * height() );
    return p;
}

/**************************************************************************************************/
QPointF WidgetRud::p2v( QPointF pix )
{
    QPointF p;
    p.setX( 2 *  pix.x() / width() - 1 );
    p.setY( 2 * pix.y() / height() - 1 );
    return p;
}

/**************************************************************************************************/
void WidgetRud::paintEvent( QPaintEvent *event )
{
    QPainter painter( this );

    painter.setRenderHint( QPainter::TextAntialiasing );
    painter.setRenderHint( QPainter::Antialiasing );
    int w = width();
    int h = height();

    painter.setPen( m_penGrid );
    painter.drawRect( 0, 0, w, h );

    for( int i = -9; i <= 9; ++i )
    {
        painter.drawLine( v2p( QPointF( i / 10.0, -0.1 ) ), v2p( QPointF( i / 10.0, 0.1 ) ) );
    }

    painter.drawLine( v2p( QPointF( 0, -0.2 ) ), v2p( QPointF( 0, 0.2 ) ) );
    painter.drawLine( v2p( QPointF( -1, 0 ) ), v2p( QPointF( 1, 0 ) ) );

    if( m_force_model )
    {
        {
            auto values = QPointF( m_force_model->getRudTrim(), 0 ) ;
            auto p = v2p( values );

            painter.setPen( m_penTrim );
            painter.drawRect( p.x() - 8, p.y() - 8, 16, 16 );
        }
        {
            auto values = QPointF( m_force_model->getRudAutopilot(), 0 ) ;
            auto p = v2p( values );

            painter.setPen( m_penAutopilot );
            painter.drawEllipse( p.x() - 8, p.y() - 8, 16, 16 );
        }
        {
            auto values = QPointF( m_force_model->getRudPosition(), 0 );
            auto p = v2p( values );

            painter.setPen( m_penPos );
            painter.drawLine( p.x() - 12, p.y() - 12, p.x() + 12, p.y() + 12 );
            painter.drawLine( p.x() + 12, p.y() - 12, p.x() - 12, p.y() + 12 );
        }
    }
}

/**************************************************************************************************/
void WidgetRud::mouseMoveEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetRud::mousePressEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetRud::mouseReleaseEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetRud::updateSetpoints( QMouseEvent *event )
{
    QPointF values = p2v( event->position() );

    if( m_force_model )
    {
        if( event->buttons().testFlag( Qt::LeftButton ) )
        {
            m_force_model->setRudAutopilot( values.x() );
        }

        if( event->buttons().testFlag( Qt::RightButton ) )
        {
            m_force_model->setRudTrim( values.x() );
        }

        if( event->buttons().testFlag( Qt::MiddleButton ) )
        {
            m_force_model->setRudAutopilot( 0.0 );
            m_force_model->setRudTrim( 0.0 );
        }
    }
}

#include "forcemodel.h"
#include <QDebug>
#include "adcdac.h"
#include "serialcomms.h"

struct Port
{
    bool config; // 1 input, 0 output
    int cs;
    int port;
};

Port ports[PORT_MAX] =
{
    {0, 2, 0},
    {1, 2, 1},
    {1, 2, 2},
    {1, 2, 3},
};

struct DiffPort
{
    bool config; // 1 input, 0 output
    int pos_cs;
    int pos_port;
    int neg_cs;
    int neg_port;
};

DiffPort diff_ports[DIFFPORT_MAX] =
{
    {1, 0, 7, 0, 6}, // ail position
    {0, 0, 5, 0, 4}, // ail autopilot
    {0, 0, 3, 0, 2}, // ail trim
    {0, 0, 0, 0, 1}, // ail airspeed

    {1, 0, 12, 0, 13}, // ele position
    {0, 0, 11, 0, 10}, // ele autopilot
    {0, 0, 8, 0, 9},   // ele trim
    {0, 0, 14, 0, 15}, // ele airspeed

    {1, 1, 4, 1, 5},   // rud position
    {0, 1, 3, 1, 2},   // rud autopilot
    {0, 1, 1, 1, 0},   // rud trim
    {0, 1, 10, 1, 11}, // rud airspeed
    {0, 1, 7, 1, 6},   // rud asym thrust
    {0, 1, 8, 1, 9},   // rud flight
};

/**************************************************************************************************/
ForceModel::ForceModel( QObject *parent )
    : QObject{parent}
{
    std::vector<std::string> availableDevices = findArduinoDeviceNames();

    if( availableDevices.size() == 0 )
    {
        throw std::runtime_error( "No AdcDac board connected" );
    }

    m_adcdac = new AdcDac( availableDevices[0] );
    m_adcdac->fwReset();

    if( !m_adcdac->isConnected() )
    {
        throw std::runtime_error( "Could not connect to AdcDac board" );
    }

    if( !m_adcdac->isFwVersionOk() )
    {
        throw std::runtime_error( "Firmware version of AdcDac board not ok" );
    }

    for( size_t i = 0; i < PORT_MAX; ++i )
    {
        auto p = ports[i];

        m_adcdac->setPortConfig( p.cs, p.port, p.config );
    }

    for( size_t i = 0; i < DIFFPORT_MAX; ++i )
    {
        auto dp = diff_ports[i];

        m_adcdac->setPortConfig( dp.pos_cs, dp.pos_port, dp.config );
        m_adcdac->setPortConfig( dp.neg_cs, dp.neg_port, dp.config );
    }
}

/**************************************************************************************************/
void ForceModel::kickWatchdog()
{
    m_watchdog_state = !m_watchdog_state;

    double level = m_watchdog_state ? 4.5 : 5.5;
    auto p = ports[PORT_WATCHDOG];
    m_adcdac->setAnalogOutputValue( p.cs, p.port, level / 10.0  * 4095 );
}

/**************************************************************************************************/
bool ForceModel::getWatchdogFeedback()
{
    auto p = ports[PORT_FEEDBACK];
    double voltage = 10.0 / 4095 * m_adcdac->getAnalogOutputValue( p.cs, p.port );
    return ( voltage > 1.5 ) and ( voltage < 4.5 );
}

/**************************************************************************************************/
bool ForceModel::getEmoState()
{
    auto p = ports[PORT_EMO];
    double voltage = 10.0 / 4095 * m_adcdac->getAnalogOutputValue( p.cs, p.port );
    return ( voltage > 1.5 ) and ( voltage < 4.5 );
}

/**************************************************************************************************/
bool ForceModel::getInrushState()
{
    auto p = ports[PORT_INRUSH];
    double voltage = 10.0 / 4095 * m_adcdac->getAnalogOutputValue( p.cs, p.port );
    return ( voltage > 1.5 ) and ( voltage < 4.5 );
}

/**************************************************************************************************/
double ForceModel::getDiffInputVoltage( DiffPortId diffportid )
{
    auto dp = diff_ports[diffportid];

    auto pos = m_adcdac->getAnalogInputValue( dp.pos_cs, dp.pos_port );
    auto neg = m_adcdac->getAnalogInputValue( dp.neg_cs, dp.neg_port );

    return ( pos - neg ) * 10.0 / 4095;
}

/**************************************************************************************************/
double ForceModel::getDiffOutputVoltage( DiffPortId diffportid )
{
    auto dp = diff_ports[diffportid];

    auto pos = m_adcdac->getAnalogOutputValue( dp.pos_cs, dp.pos_port );
    auto neg = m_adcdac->getAnalogOutputValue( dp.neg_cs, dp.neg_port );

    return ( pos - neg ) * 10.0 / 4095;
}

/**************************************************************************************************/
void ForceModel::setDiffOutputVoltage( DiffPortId diffportid, double voltage )
{
    auto dp = diff_ports[diffportid];

    double half_voltage = voltage / 2.0;
    double pos = 5.0 + half_voltage;
    double neg = 5.0 - half_voltage;

    m_adcdac->setAnalogOutputValue( dp.pos_cs, dp.pos_port, pos / 10.0  * 4095 );
    m_adcdac->setAnalogOutputValue( dp.neg_cs, dp.neg_port, neg / 10.0  * 4095 );
}

/**************************************************************************************************/
double ForceModel::getAilPosition()
{
    return getDiffInputVoltage( DIFFPORT_AIL_POSITION ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setAilAutopilot( double autopilot )
{
    setDiffOutputVoltage( DIFFPORT_AIL_AUTOPILOT, 10.0 * autopilot );
}

/**************************************************************************************************/
double ForceModel::getAilAutopilot()
{
    return getDiffInputVoltage( DIFFPORT_AIL_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getAilAutopilotSetpoint()
{
    return getDiffOutputVoltage( DIFFPORT_AIL_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setAilTrim( double trim )
{
    setDiffOutputVoltage( DIFFPORT_AIL_TRIM, 10.0 * trim );
}

/**************************************************************************************************/
double ForceModel::getAilTrim()
{
    return getDiffInputVoltage( DIFFPORT_AIL_TRIM ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getAilTrimSetpoint()
{
    return getDiffOutputVoltage( DIFFPORT_AIL_TRIM ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setAilAirspeed( double airspeed )
{
    setDiffOutputVoltage( DIFFPORT_AIL_AIRSPEED, 10.0 * airspeed );
}

/**************************************************************************************************/
double ForceModel::getAilAirspeed()
{
    return getDiffInputVoltage( DIFFPORT_AIL_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getAilAirspeedSetpoint()
{
    return getDiffOutputVoltage( DIFFPORT_AIL_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getElePosition()
{
    return getDiffInputVoltage( DIFFPORT_ELE_POSITION ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setEleAutopilot( double autopilot )
{
    setDiffOutputVoltage( DIFFPORT_ELE_AUTOPILOT, 10.0 * autopilot );
}

/**************************************************************************************************/
double ForceModel::getEleAutopilot( )
{
    return getDiffInputVoltage( DIFFPORT_ELE_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getEleAutopilotSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_ELE_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setEleTrim( double trim )
{
    setDiffOutputVoltage( DIFFPORT_ELE_TRIM, 10.0 * trim );
}

/**************************************************************************************************/
double ForceModel::getEleTrim()
{
    return getDiffInputVoltage( DIFFPORT_ELE_TRIM ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getEleTrimSetpoint()
{
    return getDiffOutputVoltage( DIFFPORT_ELE_TRIM ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setEleAirspeed( double airspeed )
{
    setDiffOutputVoltage( DIFFPORT_ELE_AIRSPEED, 10.0 * airspeed );
}

/**************************************************************************************************/
double ForceModel::getEleAirspeed( )
{
    return getDiffInputVoltage( DIFFPORT_ELE_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getEleAirspeedSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_ELE_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudPosition()
{
    return getDiffInputVoltage( DIFFPORT_RUD_POSITION ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setRudAutopilot( double autopilot )
{
    setDiffOutputVoltage( DIFFPORT_RUD_AUTOPILOT, 10.0 * autopilot );
}

/**************************************************************************************************/
double ForceModel::getRudAutopilot( )
{
    return getDiffInputVoltage( DIFFPORT_RUD_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudAutopilotSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_RUD_AUTOPILOT ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setRudTrim( double trim )
{
    setDiffOutputVoltage( DIFFPORT_RUD_TRIM, 10.0 * trim );
}

/**************************************************************************************************/
double ForceModel::getRudTrim()
{
    return getDiffInputVoltage( DIFFPORT_RUD_TRIM ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudTrimSetpoint()
{
    return getDiffOutputVoltage( DIFFPORT_RUD_TRIM ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setRudAirspeed( double airspeed )
{
    setDiffOutputVoltage( DIFFPORT_RUD_AIRSPEED, 10.0 * airspeed );
}

/**************************************************************************************************/
double ForceModel::getRudAirspeed( )
{
    return getDiffInputVoltage( DIFFPORT_RUD_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudAirspeedSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_RUD_AIRSPEED ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setRudAsymmetricThrust( double asym )
{
    setDiffOutputVoltage( DIFFPORT_RUD_ASYM_THRUST, 10.0 * asym );
}

/**************************************************************************************************/
double ForceModel::getRudAsymmetricThrust( )
{
    return getDiffInputVoltage( DIFFPORT_RUD_ASYM_THRUST ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudAsymmetricThrustSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_RUD_ASYM_THRUST ) / 10.0;
}

/**************************************************************************************************/
void ForceModel::setRudFlight( double flight )
{
    setDiffOutputVoltage( DIFFPORT_RUD_FLIGHT, 10.0 * flight );
}

/**************************************************************************************************/
double ForceModel::getRudFlight( )
{
    return getDiffInputVoltage( DIFFPORT_RUD_FLIGHT ) / 10.0;
}

/**************************************************************************************************/
double ForceModel::getRudFlightSetpoint( )
{
    return getDiffOutputVoltage( DIFFPORT_RUD_FLIGHT ) / 10.0;
}

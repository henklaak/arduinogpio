#include "widgetailele.h"
#include <QPainter>
#include <QMouseEvent>
#include "forcemodel.h"

/**************************************************************************************************/
WidgetAilEle::WidgetAilEle( QWidget *parent )
    : QWidget( parent )
{
    m_penGrid.setColor( QColor::fromRgb( 192, 192, 192 ) );
    m_penGrid.setCosmetic( true );
    m_penGrid.setWidthF( 0.2 );

    m_penPos.setColor( QColor::fromRgb( 0, 192, 0 ) );
    m_penPos.setCosmetic( true );
    m_penPos.setWidth( 2 );

    m_penAutopilot.setColor( QColor::fromRgb( 255, 0, 0 ) );
    m_penAutopilot.setCosmetic( true );
    m_penAutopilot.setWidth( 2 );

    m_penTrim.setColor( QColor::fromRgb( 128, 128, 255 ) );
    m_penTrim.setCosmetic( true );
    m_penTrim.setWidth( 2 );
}

/**************************************************************************************************/
void WidgetAilEle::setForceModel( ForceModel *force_model )
{
    m_force_model = force_model;
}

/**************************************************************************************************/
QPointF WidgetAilEle::v2p( QPointF value )
{
    QPoint p;
    p.setX( ( value.x() + 1 ) / 2 * width() );
    p.setY( ( value.y() + 1 ) / 2 * height() );
    return p;
}

/**************************************************************************************************/
QPointF WidgetAilEle::p2v( QPointF pix )
{
    QPointF p;
    p.setX( 2 *  pix.x() / width() - 1 );
    p.setY( 2 * pix.y() / height() - 1 );
    return p;
}

/**************************************************************************************************/
void WidgetAilEle::paintEvent( QPaintEvent *event )
{
    QPainter painter( this );

    painter.setRenderHint( QPainter::TextAntialiasing );
    painter.setRenderHint( QPainter::Antialiasing );
    int w = width();
    int h = height();

    painter.setPen( m_penGrid );
    painter.drawRect( 0, 0, w, h );

    for( int i = -9; i <= 9; ++i )
    {
        painter.drawLine( v2p( QPointF( i / 10.0, -1 ) ), v2p( QPointF( i / 10.0, 1 ) ) );
        painter.drawLine( v2p( QPointF( -1, i / 10.0 ) ), v2p( QPointF( 1, i / 10.0 ) ) );
    }

    painter.drawLine( v2p( QPointF( 0, -1 ) ), v2p( QPointF( 0, 1 ) ) );
    painter.drawLine( v2p( QPointF( -1, 0 ) ), v2p( QPointF( 1, 0 ) ) );

    if( m_force_model )
    {
        {
            auto values = QPointF( m_force_model->getAilTrim(), m_force_model->getEleTrim() ) ;
            auto p = v2p( values );

            painter.setPen( m_penTrim );
            painter.drawRect( p.x() - 8, p.y() - 8, 16, 16 );
        }
        {
            auto values = QPointF( m_force_model->getAilAutopilot(),
                                   m_force_model->getEleAutopilot() ) ;
            auto p = v2p( values );

            painter.setPen( m_penAutopilot );
            painter.drawEllipse( p.x() - 8, p.y() - 8, 16, 16 );
        }
        {
            auto ail = m_force_model->getAilPosition();
            auto ele = m_force_model->getElePosition();

            auto p = v2p( QPointF( ail, ele ) );

            painter.setPen( m_penPos );
            painter.drawLine( p.x() - 12, p.y() - 12, p.x() + 12, p.y() + 12 );
            painter.drawLine( p.x() + 12, p.y() - 12, p.x() - 12, p.y() + 12 );
        }
    }
}

/**************************************************************************************************/
void WidgetAilEle::mouseMoveEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetAilEle::mousePressEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetAilEle::mouseReleaseEvent( QMouseEvent *event )
{
    updateSetpoints( event );
}

/**************************************************************************************************/
void WidgetAilEle::updateSetpoints( QMouseEvent *event )
{
    QPointF values = p2v( event->position() );

    if( m_force_model )
    {
        if( event->buttons().testFlag( Qt::LeftButton ) )
        {
            m_force_model->setAilAutopilot( values.x() );
            m_force_model->setEleAutopilot( values.y() );
        }

        if( event->buttons().testFlag( Qt::RightButton ) )
        {
            m_force_model->setAilTrim( values.x() );
            m_force_model->setEleTrim( values.y() );
        }

        if( event->buttons().testFlag( Qt::MiddleButton ) )
        {
            m_force_model->setAilAutopilot( 0.0 );
            m_force_model->setEleAutopilot( 0.0 );
            m_force_model->setAilTrim( 0.0 );
            m_force_model->setEleTrim( 0.0 );
        }
    }
}


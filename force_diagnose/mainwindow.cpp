#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "forcemodel.h"

#include <QTimer>

/**************************************************************************************************/
MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    setWindowTitle( qApp->applicationName() + " " + qApp->applicationVersion() );

    m_force_model = new ForceModel( this );

    ui->widAilEle->setForceModel( m_force_model );
    ui->widRud->setForceModel( m_force_model );

    QObject::connect( ui->chkLinkAirspeeds, &QCheckBox::clicked, [this]( bool checked )
    {
        if( checked )
        {
            m_force_model->setAilAirspeed( 0 );
            m_force_model->setEleAirspeed( 0 );
            m_force_model->setRudAirspeed( 0 );
        }
    } );

    // Aileron inputs
    QObject::connect( ui->sldAilAutopilot, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setAilAutopilot( value / 100.0 );
    } );
    QObject::connect( ui->sldAilTrim, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setAilTrim( value / 100.0 );
    } );
    QObject::connect( ui->sldAilAirspeed, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setAilAirspeed( value / 100.0 );

        if( ui->chkLinkAirspeeds->isChecked() )
        {
            m_force_model->setEleAirspeed( value / 100.0 );
            m_force_model->setRudAirspeed( value / 100.0 );
        }
    } );

    // Elevator inputs
    QObject::connect( ui->sldEleAutopilot, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setEleAutopilot( value / 100.0 );
    } );
    QObject::connect( ui->sldEleTrim, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setEleTrim( value / 100.0 );
    } );
    QObject::connect( ui->sldEleAirspeed, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setEleAirspeed( value / 100.0 );

        if( ui->chkLinkAirspeeds->isChecked() )
        {
            m_force_model->setAilAirspeed( value / 100.0 );
            m_force_model->setRudAirspeed( value / 100.0 );
        }
    } );

    // Rudder inputs
    QObject::connect( ui->sldRudAutopilot, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setRudAutopilot( value / 100.0 );
    } );
    QObject::connect( ui->sldRudTrim, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setRudTrim( value / 100.0 );
    } );
    QObject::connect( ui->sldRudAirspeed, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setRudAirspeed( value / 100.0 );

        if( ui->chkLinkAirspeeds->isChecked() )
        {
            m_force_model->setAilAirspeed( value / 100.0 );
            m_force_model->setEleAirspeed( value / 100.0 );
        }
    } );
    QObject::connect( ui->sldRudAsymmetricThrust, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setRudAsymmetricThrust( value / 100.0 );
    } );
    QObject::connect( ui->sldRudFlight, &QSlider::sliderMoved, [this]( int value )
    {
        m_force_model->setRudFlight( value / 100.0 );
    } );

    QTimer *m_updater = new QTimer( this );
    QObject::connect( m_updater, &QTimer::timeout,
                      this, &MainWindow::on_update );
    on_update();
    m_updater->start( 100 );
}

/**************************************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
}

/**************************************************************************************************/
void MainWindow::on_update()
{
    if( ui->pbWatchdogToggle->isChecked() )
    {
        m_force_model->kickWatchdog();
    }

    ui->sldAilPosition->setValue( m_force_model->getAilPosition() * 100 );
    ui->valAilPosition->setText( QString().asprintf( "%5.2lf", m_force_model->getAilPosition() ) );
    ui->sldAilAutopilot->setValue( m_force_model->getAilAutopilotSetpoint() * 100 );
    ui->valAilAutopilot->setText( QString().asprintf( "%5.2lf", m_force_model->getAilAutopilot() ) );
    ui->sldAilTrim->setValue( m_force_model->getAilTrimSetpoint() * 100 );
    ui->valAilTrim->setText( QString().asprintf( "%5.2lf", m_force_model->getAilTrim() ) );
    ui->sldAilAirspeed->setValue( m_force_model->getAilAirspeedSetpoint() * 100 );
    ui->valAilAirspeed->setText( QString().asprintf( "%5.2lf", m_force_model->getAilAirspeed() ) );

    ui->sldElePosition->setValue( m_force_model->getElePosition() * 100 );
    ui->valElePosition->setText( QString().asprintf( "%5.2lf", m_force_model->getElePosition() ) );
    ui->sldEleAutopilot->setValue( m_force_model->getEleAutopilotSetpoint() * 100 );
    ui->valEleAutopilot->setText( QString().asprintf( "%5.2lf", m_force_model->getEleAutopilot() ) );
    ui->sldEleTrim->setValue( m_force_model->getEleTrimSetpoint() * 100 );
    ui->valEleTrim->setText( QString().asprintf( "%5.2lf", m_force_model->getEleTrim() ) );
    ui->sldEleAirspeed->setValue( m_force_model->getEleAirspeedSetpoint() * 100 );
    ui->valEleAirspeed->setText( QString().asprintf( "%5.2lf", m_force_model->getEleAirspeed() ) );

    ui->sldRudPosition->setValue( m_force_model->getRudPosition() * 100 );
    ui->valRudPosition->setText( QString().asprintf( "%5.2lf", m_force_model->getRudPosition() ) );
    ui->sldRudAutopilot->setValue( m_force_model->getRudAutopilotSetpoint() * 100 );
    ui->valRudAutopilot->setText( QString().asprintf( "%5.2lf", m_force_model->getRudAutopilot() ) );
    ui->sldRudTrim->setValue( m_force_model->getRudTrimSetpoint() * 100 );
    ui->valRudTrim->setText( QString().asprintf( "%5.2lf", m_force_model->getRudTrim() ) );
    ui->sldRudAirspeed->setValue( m_force_model->getRudAirspeedSetpoint() * 100 );
    ui->valRudAirspeed->setText( QString().asprintf( "%5.2lf", m_force_model->getRudAirspeed() ) );
    ui->sldRudAsymmetricThrust->setValue( m_force_model->getRudAsymmetricThrustSetpoint() * 100 );
    ui->valRudAsymmetricThrust->setText( QString().asprintf( "%5.2lf", m_force_model->getRudAsymmetricThrust() ) );
    ui->sldRudFlight->setValue( m_force_model->getRudFlightSetpoint() * 100 );
    ui->valRudFlight->setText( QString().asprintf( "%5.2lf", m_force_model->getRudFlight() ) );

    ui->lblWatchdogState->setText( m_force_model->getWatchdogFeedback() ? "Active" : "-" );
    ui->lblEmoState->setText( m_force_model->getEmoState() ? "Active" : "-" );
    ui->lblInrushState->setText( m_force_model->getInrushState() ? "Active" : "-" );

    ui->widAilEle->update();
    ui->widRud->update();

    ui->vwChartAil->addSample( m_force_model->getAilPosition(),
                               m_force_model->getAilAutopilot(),
                               m_force_model->getAilTrim() );
    ui->vwChartEle->addSample( m_force_model->getElePosition(),
                               m_force_model->getEleAutopilot(),
                               m_force_model->getEleTrim() );
    ui->vwChartRud->addSample( m_force_model->getRudPosition(),
                               m_force_model->getRudAutopilot(),
                               m_force_model->getRudTrim() );
}

/**************************************************************************************************/
void MainWindow::keyPressEvent( QKeyEvent *event )
{
    ui->pbWatchdogToggle->setChecked( false );

    // airspeeds first
    m_force_model->setAilAirspeed( 0 );
    m_force_model->setEleAirspeed( 0 );
    m_force_model->setRudAirspeed( 0 );

    m_force_model->setAilAutopilot( 0 );
    m_force_model->setAilTrim( 0 );

    m_force_model->setEleAutopilot( 0 );
    m_force_model->setEleTrim( 0 );

    m_force_model->setRudAutopilot( 0 );
    m_force_model->setRudTrim( 0 );
    m_force_model->setRudAsymmetricThrust( 0 );
    m_force_model->setRudFlight( 0 );
}

#pragma once

#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

class ForceModel;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget *parent = nullptr );
    ~MainWindow();

    Ui::MainWindow *ui;
    ForceModel *m_force_model = nullptr;

protected:
    void keyPressEvent( QKeyEvent *event ) override;

private slots:
    void on_update();
};

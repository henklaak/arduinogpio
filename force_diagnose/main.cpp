#include <chrono>
#include <iostream>
#include <cassert>
#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <iostream>
#include "mainwindow.h"


/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QApplication app( argc, argv );
    app.setOrganizationName( "DeLink" );
    app.setOrganizationDomain( "vluchtsimulatie.nl" );
    app.setApplicationName( "Force Diagnose" );
    app.setApplicationVersion( "0.0.0" );

    QFile styleFile( ":/style.qss" );
    styleFile.open( QFile::ReadOnly );

    QString style( styleFile.readAll() );
    app.setStyleSheet( style );

    try
    {
        MainWindow w;
        w.show();

        return app.exec();
    }
    catch( std::runtime_error &e )
    {
        QMessageBox::critical( nullptr, "Critical error", e.what() );
        return -1;
    }
}

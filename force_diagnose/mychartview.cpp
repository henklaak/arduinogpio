#include "mychartview.h"

#include <QLineSeries>

/**************************************************************************************************/
MyChartView::MyChartView( QWidget *parent )
    : QChartView( parent )
{
    setRenderHint( QPainter::Antialiasing );
    m_chart = new QChart();

    m_xaxis = new QValueAxis();
    m_xaxis->setRange( 0.0, 60.0 );
    m_chart->addAxis( m_xaxis, Qt::AlignBottom );

    m_yaxis = new QValueAxis();
    m_yaxis->setRange( -1.0, 1.0 );
    m_chart->addAxis( m_yaxis, Qt::AlignLeft );


    {
        m_pointlist[0].append( QPointF( 0, 0 ) );
        m_position = new QLineSeries( m_chart );
        m_position->setName( "Position" );
        m_position->setColor( QColor::fromRgb( 0, 192, 0 ) );
        m_chart->addSeries( m_position );
        m_position->attachAxis( m_xaxis );
        m_position->attachAxis( m_yaxis );
    }
    {
        m_pointlist[1].append( QPointF( 0, 0 ) );
        m_autopilot = new QLineSeries( m_chart );
        m_autopilot->setName( "Autopilot" );
        m_autopilot->setColor( QColor::fromRgb( 255, 0, 0 ) );
        m_chart->addSeries( m_autopilot );
        m_autopilot->attachAxis( m_xaxis );
        m_autopilot->attachAxis( m_yaxis );
    }
    {
        m_pointlist[2].append( QPointF( 0, 0 ) );
        m_trim = new QLineSeries( m_chart );
        m_trim->setName( "Trim" );
        m_trim->setColor( QColor::fromRgb( 128, 128, 255 ) );
        m_chart->addSeries( m_trim );
        m_trim->attachAxis( m_xaxis );
        m_trim->attachAxis( m_yaxis );
    }

    setChart( m_chart );
}

/**************************************************************************************************/
void MyChartView::addSample( double position,
                             double autopilot,
                             double trim )
{
    auto x = m_pointlist[0].last().x();
    m_pointlist[0].push_back( QPointF( x + 1, position ) );

    if( m_pointlist[0].size() > 100 )
    {
        m_pointlist[0].erase( m_pointlist[0].begin() );
    }

    m_position->replace( m_pointlist[0] );

    m_pointlist[1].push_back( QPointF( x + 1, autopilot ) );

    if( m_pointlist[1].size() > 100 )
    {
        m_pointlist[1].erase( m_pointlist[1].begin() );
    }

    m_autopilot->replace( m_pointlist[1] );

    m_pointlist[2].push_back( QPointF( x + 1, trim ) );

    if( m_pointlist[2].size() > 100 )
    {
        m_pointlist[2].erase( m_pointlist[2].begin() );
    }

    m_trim->replace( m_pointlist[2] );

    m_xaxis->setRange( x - 100, x );
}




#pragma once

#include <QObject>
class AdcDac;

enum PortId
{
    PORT_WATCHDOG = 0,
    PORT_FEEDBACK,
    PORT_EMO,
    PORT_INRUSH,
    PORT_MAX
};

enum DiffPortId
{
    DIFFPORT_AIL_POSITION = 0,
    DIFFPORT_AIL_AUTOPILOT,
    DIFFPORT_AIL_TRIM,
    DIFFPORT_AIL_AIRSPEED,

    DIFFPORT_ELE_POSITION,
    DIFFPORT_ELE_AUTOPILOT,
    DIFFPORT_ELE_TRIM,
    DIFFPORT_ELE_AIRSPEED,

    DIFFPORT_RUD_POSITION,
    DIFFPORT_RUD_AUTOPILOT,
    DIFFPORT_RUD_TRIM,
    DIFFPORT_RUD_AIRSPEED,
    DIFFPORT_RUD_ASYM_THRUST,
    DIFFPORT_RUD_FLIGHT,

    DIFFPORT_MAX
};

class ForceModel : public QObject
{
    Q_OBJECT
public:
    explicit ForceModel(QObject *parent = nullptr);

    void kickWatchdog();
    bool getWatchdogFeedback();
    bool getEmoState();
    bool getInrushState();

    double getAilPosition();
    void setAilAutopilot(double autopilot);
    double getAilAutopilot();
    double getAilAutopilotSetpoint();
    void setAilTrim(double trim);
    double getAilTrim();
    double getAilTrimSetpoint();
    void setAilAirspeed(double airspeed);
    double getAilAirspeed();
    double getAilAirspeedSetpoint();

    double getElePosition();
    void setEleAutopilot(double autopilot);
    double getEleAutopilot();
    double getEleAutopilotSetpoint();
    void setEleTrim(double trim);
    double getEleTrim();
    double getEleTrimSetpoint();
    void setEleAirspeed(double airspeed);
    double getEleAirspeed();
    double getEleAirspeedSetpoint();

    double getRudPosition();
    void setRudAutopilot(double autopilot);
    double getRudAutopilot();
    double getRudAutopilotSetpoint();
    void setRudTrim(double trim);
    double getRudTrim();
    double getRudTrimSetpoint();
    void setRudAirspeed(double airspeed);
    double getRudAirspeed();
    double getRudAirspeedSetpoint();
    void setRudAsymmetricThrust(double asym);
    double getRudAsymmetricThrust();
    double getRudAsymmetricThrustSetpoint();
    void setRudFlight(double flight);
    double getRudFlight();
    double getRudFlightSetpoint();

private:
    double getDiffInputVoltage( DiffPortId diffportid );
    double getDiffOutputVoltage( DiffPortId diffportid );
    void setDiffOutputVoltage( DiffPortId diffportid, double voltage );

    AdcDac *m_adcdac=nullptr;
    bool m_watchdog_state=false;
};

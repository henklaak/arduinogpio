#ifndef ELECHARTVIEW_H
#define ELECHARTVIEW_H

#include <QChartView>
#include <QLineSeries>
#include <QObject>
#include <QValueAxis>

class MyChartView : public QChartView
{
    Q_OBJECT
public:
    MyChartView( QWidget *parent = nullptr );

    void addSample( double position,
                    double autopilot,
                    double trim );

private:
    QChart *m_chart = nullptr;
    QValueAxis *m_xaxis = nullptr;
    QValueAxis *m_yaxis = nullptr;
    QLineSeries *m_position = nullptr;
    QLineSeries *m_autopilot = nullptr;
    QLineSeries *m_trim = nullptr;

    QList<QPointF> m_pointlist[3];

};

#endif // ELECHARTVIEW_H

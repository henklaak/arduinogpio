#include <iostream>
#include <filesystem>
#include <cassert>
#include <fstream>
#include <QGuiApplication>
#include <QScreen>

#include "config.h"

namespace fs = std::filesystem;

/**************************************************************************************************/
void patch_prf_file( const std::string &filename, const std::string &line,
                     const std::string &value )
{
    //std::cout << filename << ": " << line << " = " << value << std::endl;

    fs::path base( XPLANE_DIRECTORY );
    base = base / "Output" / "preferences";

    //std::cout << base << std::endl;

    assert( fs::exists( base ) );

    fs::path p = base / fs::path( filename );
    assert( fs::exists( p ) );

    fs::path pnew = base / fs::path( filename + std::string( ".new" ) );

    fs::path pold = base / fs::path( filename + std::string( ".old" ) );
    fs::remove( pold );

    fs::copy_file( p, pold );

    std::ifstream oldfile;
    std::ofstream newfile;
    oldfile.open( pold );
    newfile.open( pnew );

    // Read past first two lines
    std::string oldline;
    std::string delimiter = " ";
    bool replaced = false;
    while( std::getline( oldfile, oldline ) )
    {
        if( oldline.find( line + " ", 0 ) == 0 )
        {
            newfile << line << " " << value << std::endl;
            replaced = true;
        }
        else
        {
            newfile << oldline << std::endl;
        }
    }

    if( !replaced )
    {
        newfile << line << " " << value << std::endl;
    }

    oldfile.close();
    newfile.close();

    fs::remove( p );
    fs::copy_file( pnew, p );
    fs::remove( pnew );
}

/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QGuiApplication app( argc, argv );

    std::cout << "Using " << XPLANE_DIRECTORY << std::endl;

    enum ScreenType
    {
        SCREEN_UNKNOWN = -1,
        SCREEN_G1000_LEFT = 0,
        SCREEN_G1000_RIGHT,
        SCREEN_IOS,
        SCREEN_VISUAL,
        SCREEN_MAX
    };

    struct Screen
    {
        int x, y, w, h;
        int idx;
    } screens[SCREEN_MAX] = {};

    int devicesFound = app.screens().size();

    if( devicesFound != SCREEN_MAX )
    {
        std::cerr << "Found only " << devicesFound << " out of " << SCREEN_MAX << " screens" << std::endl;
    }

    for( int i = 0; i < devicesFound; ++i )
    {
        ScreenType sc = SCREEN_UNKNOWN;

        auto k = app.screens()[i]->manufacturer();
        auto l = app.screens()[i]->model();
        auto m = app.screens()[i]->name();
        auto n = app.screens()[i]->serialNumber();

        switch( i )
        {
        case 2:
            sc = SCREEN_G1000_LEFT;
            break;
        case 3:
            sc = SCREEN_G1000_RIGHT;
            break;
        case 1:
            sc = SCREEN_IOS;
            break;
        case 0:
            sc = SCREEN_VISUAL;
            break;
        }

        screens[sc].idx = i;
        screens[sc].x = app.screens()[i]->geometry().left();
        screens[sc].y = app.screens()[i]->geometry().top();
        screens[sc].w = app.screens()[i]->geometry().width();
        screens[sc].h = app.screens()[i]->geometry().height();
        std::cout << sc << " " <<
                  screens[sc].x << "," << screens[sc].y <<
                  " (" << screens[sc].w << " x " << screens[sc].h << ")" << std::endl;
    }

    Screen scr{};

    char buf[256] {};
    scr = screens[SCREEN_VISUAL];
    if( scr.w > 0 )
    {
        patch_prf_file( "X-Plane Window Positions.prf", "num_monitors", "2" );
        snprintf( buf, 256, "%d", scr.idx );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_monitor", buf );
        snprintf( buf, 256, "%d", scr.x );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_window_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.y );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_window_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_window_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_window_bounds/3", buf );
        snprintf( buf, 256, "%d", scr.x );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_arbitrary_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.y );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_arbitrary_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_arbitrary_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_arbitrary_bounds/3", buf );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_is_fullscreen",
                        "wmgr_mode_fullscreen" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_usage", "wmgr_usage_normal_visuals" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_refresh_rate", "60" );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_x_res_full", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_y_res_full", buf );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/m_bpp", "32" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/proj/diff_FOV", "0" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/proj/FOVx_renopt", "50" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/0/proj/window_2d_off", "0" );
    }

    scr = screens[SCREEN_IOS];
    if( scr.w > 0 )
    {
        snprintf( buf, 256, "%d", scr.idx );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_monitor", buf );
        snprintf( buf, 256, "%d", scr.x );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_window_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.y );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_window_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_window_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_window_bounds/3", buf );
        snprintf( buf, 256, "%d", scr.x );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_arbitrary_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.y );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_arbitrary_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_arbitrary_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_arbitrary_bounds/3", buf );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_is_fullscreen",
                        "wmgr_mode_fullscreen" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_usage", "wmgr_usage_ios" );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_refresh_rate", "60" );
        snprintf( buf, 256, "%d", scr.w );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_x_res_full", buf );
        snprintf( buf, 256, "%d", scr.h );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_y_res_full", buf );
        patch_prf_file( "X-Plane Window Positions.prf", "monitor/1/m_bpp", "32" );
    }

    scr = screens[SCREEN_G1000_LEFT];
    if( scr.w > 0 )
    {
        std::cout << "Repositioning G1000 Left" << std::endl;
        char buf[512];
        snprintf( buf, 512, "%d||%d||%d||%d", scr.x, scr.y + 5, scr.x + scr.w, scr.y + scr.h );

        patch_prf_file( "Miscellaneous.prf", "P pilot-1000 gps_popped_out_loc", buf );
        patch_prf_file( "Miscellaneous.prf", "P pilot-1000 gps_window_is_popped_out", "1" );
        patch_prf_file( "Miscellaneous.prf", "P pilot-1000 gps_window_visible", "1" );
    }
    else
    {
        std::cout << "Disabling G1000 left" << std::endl;
        patch_prf_file( "Miscellaneous.prf", "P pilot-1000 gps_window_is_popped_out", "0" );
        patch_prf_file( "Miscellaneous.prf", "P pilot-1000 gps_window_visible", "0" );
    }

    scr = screens[SCREEN_G1000_RIGHT];
    if( scr.w > 0 )
    {
        std::cout << "Repositioning G1000 Right" << std::endl;
        char buf[512];
        snprintf( buf, 512, "%d||%d||%d||%d", scr.x, scr.y + 5, scr.x + scr.w, scr.y + scr.h );

        patch_prf_file( "Miscellaneous.prf", "P center-1000 gps_popped_out_loc", buf );
        patch_prf_file( "Miscellaneous.prf", "P center-1000 gps_window_is_popped_out", "1" );
        patch_prf_file( "Miscellaneous.prf", "P center-1000 gps_window_visible", "1" );
    }
    else
    {
        std::cout << "Disabling G1000 center" << std::endl;
        patch_prf_file( "Miscellaneous.prf", "P center-1000 gps_window_is_popped_out", "0" );
        patch_prf_file( "Miscellaneous.prf", "P center-1000 gps_window_visible", "0" );
    }

    std::cout << "Correcting other X-Plane settings" << std::endl;

    patch_prf_file( "X-Plane Screen Res.prf", "_use_vsync1", "0" );

    patch_prf_file( "Miscellaneous.prf", "default_situation", "Output/situations/startme.sit" );
    patch_prf_file( "Miscellaneous.prf", "confirm_quit", "0" );

    patch_prf_file( "X-Plane.prf", "_lang", "0" );
    patch_prf_file( "X-Plane.prf", "_default_view", "1" );
    patch_prf_file( "X-Plane.prf", "_show_qfl_on_start", "0" );
    patch_prf_file( "X-Plane.prf", "_show_atc_taxi_route", "0" );
    patch_prf_file( "X-Plane.prf", "_warn_scene", "0" );
    patch_prf_file( "X-Plane.prf", "_warn_text", "0" );
    patch_prf_file( "X-Plane.prf", "_text_out", "0" );

    patch_prf_file( "X-Plane.prf", "_vol_master", "1.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_aircraft", "1.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_cockpit", "0.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_copilot", "1.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_enviro", "1.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_radios", "1.000000" );
    patch_prf_file( "X-Plane.prf", "_vol_grp_ui", "1.000000" );

    patch_prf_file( "X-Plane.prf", "_start_running", "0" );
    patch_prf_file( "X-Plane.prf", "renopt_gload", "0" );
    patch_prf_file( "X-Plane.prf", "renopt_sloped_runways", "1" );

    patch_prf_file( "X-Plane.prf", "renopt_scenery_shadows", "0" ); // 0,1
    patch_prf_file( "X-Plane.prf", "renopt_static_acf", "0" ); // 0,1
    patch_prf_file( "X-Plane.prf", "_tex_res", "4" ); // 1-5
    patch_prf_file( "X-Plane.prf", "_aniso_filter", "2" ); //1,2,4,8,16
    patch_prf_file( "X-Plane.prf", "renopt_draw_3d_04", "2" ); // 0-4
    patch_prf_file( "X-Plane.prf", "renopt_effects_04", "1" ); // 0-4

    patch_prf_file( "X-Plane.prf", "renopt_HDR_antial", "1" ); // 0-5
    patch_prf_file( "X-Plane.prf", "renopt_wat_05", "1" ); // 0-5

    patch_prf_file( "X-Plane.prf", "renopt_boats", "0" );
    patch_prf_file( "X-Plane.prf", "renopt_comp_texes", "1" );
    patch_prf_file( "X-Plane.prf", "renopt_deer_birds", "0" );

    patch_prf_file( "gfx.prf", "MODERN", "" );

    return 0;
}

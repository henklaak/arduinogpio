/*
 * GTX 680               Screens
 *
 * DP                    VGA  - G1000 PFD
 * HDMI                  VGA  - G1000 MFD
 * DVI-I                 VGA  - IOS
 * DVI-D                 HDMI - Visual
 *
 *
 * bcdedit.exe -set {globalsettings} bootuxdisabled on
 * Personalization
 *  background solid black
 *  lock screen no tips, no picture
 *
 */
#include <windows.h>
#include <iostream>
#include <filesystem>
#include <cassert>
#include <fstream>
#include <list>
#include <QImageWriter>
#include <QPixmap>
#include <QGuiApplication>
#include <QPainter>
#include "config.h"


enum Condition
{
    RESET = 0,
    ALWAYS = 1,
};

bool reset = false;

enum ScreenFunction
{
    SCREEN_FUNC_UNKNOWN = 0,
    SCREEN_FUNC_VIZ,
    SCREEN_FUNC_IOS,
    SCREEN_FUNC_PFD,
    SCREEN_FUNC_MFD,
    SCREEN_FUNC_MAX
};

std::string names[SCREEN_FUNC_MAX] = {"?","VIZ","IOS","PFD","MFD"};


struct Screen
{
    std::string device_name;
    std::string device_id;
    bool is_primary;
    ScreenFunction screen_function;
    QRect rc;
};

std::list<Screen> screens;

namespace fs = std::filesystem;

/**************************************************************************************************/
void assign_screens()
{
    BOOL success = false;

    // do inventory
    for (int deviceIndex=0; ; ++deviceIndex)
    {
        DISPLAY_DEVICEA ddCard{};
        ddCard.cb = sizeof( DISPLAY_DEVICEA );

        success = EnumDisplayDevicesA( NULL, deviceIndex, &ddCard, 0 );
        if (!success) break;

        if( ddCard.StateFlags & DISPLAY_DEVICE_ACTIVE )
        {
            DISPLAY_DEVICEA ddMonitor{};
            ddMonitor.cb = sizeof( DISPLAY_DEVICEA );
            success = EnumDisplayDevicesA( ddCard.DeviceName, 0, &ddMonitor, EDD_GET_DEVICE_INTERFACE_NAME );
            if (!success) break;

            Screen screen;
            screen.device_name = ddCard.DeviceName;
            screen.device_id = ddMonitor.DeviceID;
            screen.is_primary = ddMonitor.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE;
            screen.screen_function = SCREEN_FUNC_UNKNOWN;
            screens.push_back(screen);
        }
    }

    // assign functions
    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;

        if (screen.device_id.find("RTK1D1A") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_PFD;
        }
        if (screen.device_id.find("HIC0001") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_MFD;
        }
        if (screen.device_id.find("Default_Monitor") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_IOS;
        }
        if (screen.device_id.find("FAY") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_IOS;
        }
        if (screen.device_id.find("GSM76F6") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_VIZ;
        }
        if (screen.device_id.find("IVM5628") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_IOS;
        }
        if (screen.device_id.find("PHL08C5") != std::string::npos)
        {
            screen.screen_function = SCREEN_FUNC_VIZ;
        }
    }

    // set screen properties
    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;

        DEVMODEA dmScreen{};
        dmScreen.dmSize = sizeof( DEVMODEA );
        success = EnumDisplaySettingsA( screen.device_name.c_str(), ENUM_CURRENT_SETTINGS, &dmScreen );
        if (!success) continue;

        if (screen.screen_function == SCREEN_FUNC_PFD)
        {
            dmScreen.dmPosition.x = -2048;
            dmScreen.dmPosition.y = 0;
            dmScreen.dmPelsWidth = 1024;
            dmScreen.dmPelsHeight = 768;
            dmScreen.dmBitsPerPel = 32;
            dmScreen.dmDisplayFrequency = 60;
            dmScreen.dmFields = DM_POSITION |
                                DM_PELSWIDTH |
                                DM_PELSHEIGHT |
                                DM_BITSPERPEL |
                                DM_DISPLAYFREQUENCY;

            LONG result = ChangeDisplaySettingsExA (
                              screen.device_name.c_str(),
                              &dmScreen,
                              NULL,
                              CDS_UPDATEREGISTRY | CDS_GLOBAL | CDS_NORESET,
                              NULL);
            if (result != DISP_CHANGE_SUCCESSFUL) continue;
        }
        else if (screen.screen_function == SCREEN_FUNC_MFD)
        {
            dmScreen.dmPosition.x = -1024;
            dmScreen.dmPosition.y = 0;
            dmScreen.dmPelsWidth = 1024;
            dmScreen.dmPelsHeight = 768;
            dmScreen.dmBitsPerPel = 32;
            dmScreen.dmDisplayFrequency = 60;
            dmScreen.dmFields = DM_POSITION |
                                DM_PELSWIDTH |
                                DM_PELSHEIGHT |
                                DM_BITSPERPEL |
                                DM_DISPLAYFREQUENCY;

            LONG result = ChangeDisplaySettingsExA (
                              screen.device_name.c_str(),
                              &dmScreen,
                              NULL,
                              CDS_UPDATEREGISTRY | CDS_GLOBAL | CDS_NORESET,
                              NULL);
            if (result != DISP_CHANGE_SUCCESSFUL) continue;

        }
        else if (screen.screen_function == SCREEN_FUNC_IOS)
        {
            dmScreen.dmPosition.x = 0;
            dmScreen.dmPosition.y = 0;
            dmScreen.dmPelsWidth = 1024;
            dmScreen.dmPelsHeight = 768;
            dmScreen.dmBitsPerPel = 32;
            dmScreen.dmDisplayFrequency = 60;
            dmScreen.dmFields = DM_POSITION |
                                DM_PELSWIDTH |
                                DM_PELSHEIGHT |
                                DM_BITSPERPEL |
                                DM_DISPLAYFREQUENCY;

            LONG result = ChangeDisplaySettingsExA (
                              screen.device_name.c_str(),
                              &dmScreen,
                              NULL,
                              CDS_SET_PRIMARY | CDS_UPDATEREGISTRY | CDS_GLOBAL | CDS_NORESET,
                              NULL);
            if (result != DISP_CHANGE_SUCCESSFUL) continue;

        }
        else if (screen.screen_function == SCREEN_FUNC_VIZ)
        {
            dmScreen.dmPosition.x = 1024;
            dmScreen.dmPosition.y = 0;
            dmScreen.dmPelsWidth = 3440;
            dmScreen.dmPelsHeight = 1440;
            dmScreen.dmBitsPerPel = 32;
            dmScreen.dmDisplayFrequency = 60;
            dmScreen.dmFields = DM_POSITION |
                                DM_PELSWIDTH |
                                DM_PELSHEIGHT |
                                DM_BITSPERPEL |
                                DM_DISPLAYFREQUENCY;

            LONG result = ChangeDisplaySettingsExA (
                              screen.device_name.c_str(),
                              &dmScreen,
                              NULL,
                              CDS_UPDATEREGISTRY | CDS_GLOBAL | CDS_NORESET,
                              NULL);
            if (result != DISP_CHANGE_SUCCESSFUL) continue;
        }
        else
        {
            continue;
        }

    }
    ChangeDisplaySettingsEx(NULL, NULL, NULL, 0, NULL);

    // Good, screens are setup. The might not have obeyed the request position, so read that back
    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;

        DEVMODEA dmScreen{};
        dmScreen.dmSize = sizeof( DEVMODEA );
        success = EnumDisplaySettingsA( screen.device_name.c_str(), ENUM_CURRENT_SETTINGS, &dmScreen );
        if (!success) continue;

        screen.rc = QRect(dmScreen.dmPosition.x,
                          dmScreen.dmPosition.y,
                          dmScreen.dmPelsWidth,
                          dmScreen.dmPelsHeight);
    }

    //Now tell FSX about it!
}

/**************************************************************************************************/
void set_backgrounds()
{
    // determine extents
    int left=0, top=0, right=0, bottom=0;
    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;
        left = std::min(left, screen.rc.left());
        top = std::min(top, screen.rc.top());
        right= std::max(right, screen.rc.left()+screen.rc.width());
        bottom= std::max(bottom, screen.rc.top()+screen.rc.height());
    }

    QRect rc(left,top,right-left, bottom-top);
    QSize sz = rc.size();


    QPixmap pm(sz);
    QPainter pt(&pm);
    QFont f("Arial", 80);
    QPen pen(QColor("#404040"), 32);

    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;

        auto rc = screen.rc;
        rc.adjust(-left,-top,-left,-top);
        pt.setPen(pen);
        pt.setClipRect(rc);
        pt.drawRect(rc);
        pt.setPen(QPen(Qt::white));
        pt.setFont(f);
        pt.drawText(rc,Qt::AlignCenter, names[screen.screen_function].c_str());//.device_name.c_str());
    }

    const char *fn = "C:\\Localdata\\arduinogpio\\layout.png";
    pm.save(fn);
    SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, (void *)fn, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
}


/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/
void patch_prf_file( Condition cond,
                     const std::string &filename, const std::string &line,
                     const std::string &value )
{
    if (cond == RESET && !reset)
        return;

    //std::cout << filename << ": " << line << " = " << value << std::endl;

    fs::path base( XPLANE_DIRECTORY );
    base = base / "Output" / "preferences";

    //std::cout << base << std::endl;

    assert( fs::exists( base ) );

    fs::path p = base / fs::path( filename );
    assert( fs::exists( p ) );

    fs::path pnew = base / fs::path( filename + std::string( ".new" ) );

    fs::path pold = base / fs::path( filename + std::string( ".old" ) );
    fs::remove( pold );

    fs::copy_file( p, pold );

    std::ifstream oldfile;
    std::ofstream newfile;
    oldfile.open( pold );
    newfile.open( pnew );

    // Read past first two lines
    std::string oldline;
    std::string delimiter = " ";
    bool replaced = false;
    while( std::getline( oldfile, oldline ) )
    {
        if( oldline.find( line + " ", 0 ) == 0 )
        {
            newfile << line << " " << value << std::endl;
            replaced = true;
        }
        else
        {
            newfile << oldline << std::endl;
        }
    }

    if( !replaced )
    {
        newfile << line << " " << value << std::endl;
    }

    oldfile.close();
    newfile.close();

    fs::remove( p );
    fs::copy_file( pnew, p );
    fs::remove( pnew );
}

/**************************************************************************************************/
bool find_screen(ScreenFunction screen_func, Screen &scr)
{
    for (auto it =screens.begin(); it!= screens.end(); ++it)
    {
        Screen &screen = *it;
        if (screen.screen_function == screen_func)
        {
            scr = screen;
            return true;
        }
    }

    return false;
}

/**************************************************************************************************/
void patch_miscellaneous()
{
    const std::string prf = "Miscellaneous.prf";
    char buf[512] {};

    patch_prf_file( RESET, prf, "default_situation", "Output/situations/startme.sit" );
    patch_prf_file( ALWAYS, prf, "confirm_quit", "0" );


    Screen scr;
    if(find_screen(SCREEN_FUNC_PFD, scr))
    {
        std::cout << "Repositioning PFD" << std::endl;
        snprintf( buf, 512, "%d||%d||%d||%d",
                  scr.rc.x(),
                  scr.rc.y() + 5,
                  scr.rc.x() + scr.rc.width(),
                  scr.rc.y() + scr.rc.height());
        patch_prf_file( ALWAYS, prf, "P pilot-1000 gps_popped_out_loc", buf );
        patch_prf_file( ALWAYS, prf, "P pilot-1000 gps_window_is_popped_out", "1" );
        patch_prf_file( ALWAYS, prf, "P pilot-1000 gps_window_visible", "1" );
    }
    else
    {
        std::cout << "Disabling PFD" << std::endl;
        patch_prf_file( ALWAYS, prf, "P pilot-1000 gps_window_is_popped_out", "0" );
        patch_prf_file( ALWAYS, prf, "P pilot-1000 gps_window_visible", "0" );
    }

    if(find_screen(SCREEN_FUNC_MFD, scr))
    {
        std::cout << "Repositioning MFD" << std::endl;
        snprintf( buf, 512, "%d||%d||%d||%d",
                  scr.rc.x(),
                  scr.rc.y() + 5,
                  scr.rc.x() + scr.rc.width(),
                  scr.rc.y() + scr.rc.height());
        patch_prf_file( ALWAYS, prf, "P center-1000 gps_popped_out_loc", buf );
        patch_prf_file( ALWAYS, prf, "P center-1000 gps_window_is_popped_out", "1" );
        patch_prf_file( ALWAYS, prf, "P center-1000 gps_window_visible", "1" );
    }
    else
    {
        std::cout << "Disabling MFD" << std::endl;
        patch_prf_file( ALWAYS, prf, "P center-1000 gps_window_is_popped_out", "0" );
        patch_prf_file( ALWAYS, prf, "P center-1000 gps_window_visible", "0" );
    }
}

/**************************************************************************************************/
void patch_xplane()
{
    const std::string prf = "X-Plane.prf";

    patch_prf_file( ALWAYS, prf, "_default_view", "1" );
    patch_prf_file( ALWAYS, prf, "_disable_incoming_networking", "1");
    patch_prf_file( ALWAYS, prf, "_lang", "0" );
    patch_prf_file( ALWAYS, prf, "_show_qfl_on_start", "0" );
    patch_prf_file( ALWAYS, prf, "_show_atc_taxi_route", "0" );
    patch_prf_file( ALWAYS, prf, "_sound_on", "1");
    patch_prf_file( ALWAYS, prf, "_start_running", "0" );
    patch_prf_file( ALWAYS, prf, "_text_out", "0" );
    patch_prf_file( RESET, prf, "_vol_master", "1.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_aircraft", "1.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_cockpit", "0.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_copilot", "1.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_enviro", "1.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_radios", "1.000000" );
    patch_prf_file( RESET, prf, "_vol_grp_ui", "1.000000" );
    patch_prf_file( ALWAYS, prf, "_warn_scene", "0" );
    patch_prf_file( ALWAYS, prf, "_warn_text", "0" );
    patch_prf_file( RESET, prf, "renopt_gload", "0" );
    patch_prf_file( RESET, prf, "renopt_sloped_runways", "1" );
    patch_prf_file( RESET, prf, "renopt_scenery_shadows", "0" ); // 0,1
    patch_prf_file( RESET, prf, "renopt_static_acf", "0" ); // 0,1
    patch_prf_file( RESET, prf, "_tex_res", "4" ); // 1-5
    patch_prf_file( RESET, prf, "_aniso_filter", "2" ); //1,2,4,8,16
    patch_prf_file( RESET, prf, "renopt_draw_3d_04", "2" ); // 0-4
    patch_prf_file( RESET, prf, "renopt_effects_04", "1" ); // 0-4
    patch_prf_file( RESET, prf, "renopt_HDR_antial", "1" ); // 0-5
    patch_prf_file( RESET, prf, "renopt_wat_05", "1" ); // 0-5
    patch_prf_file( RESET, prf, "renopt_boats", "0" );
    patch_prf_file( RESET, prf, "renopt_comp_texes", "1" );
    patch_prf_file( RESET, prf, "renopt_deer_birds", "0" );

    // TODO disable in cockpit data?
}

/**************************************************************************************************/
void patch_positions()
{
    const std::string prf = "X-Plane Window Positions.prf";
    char buf[512] {};

    Screen scr;
    if(find_screen(SCREEN_FUNC_IOS, scr))
    {
        patch_prf_file( ALWAYS, prf, "num_monitors", "2" );
        snprintf( buf, 256, "%d", 0);
        patch_prf_file( ALWAYS, prf, "monitor/0/m_monitor", buf );
        snprintf( buf, 256, "%d", scr.rc.x() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_window_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.rc.y() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_window_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_window_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_window_bounds/3", buf );
        snprintf( buf, 256, "%d", scr.rc.x() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_arbitrary_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.rc.y());
        patch_prf_file( ALWAYS, prf, "monitor/0/m_arbitrary_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_arbitrary_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_arbitrary_bounds/3", buf );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_is_fullscreen", "wmgr_mode_fullscreen" );
        //patch_prf_file( ALWAYS, prf, "monitor/0/m_usage", "wmgr_usage_normal_visuals" );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_usage", "wmgr_usage_ios" );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_refresh_rate", "60" );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_x_res_full", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_y_res_full", buf );
        patch_prf_file( ALWAYS, prf, "monitor/0/m_bpp", "32" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/diff_FOV", "0" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/FOVx_renopt", "50" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/window_2d_off", "0" );
    }

    if(find_screen(SCREEN_FUNC_VIZ, scr))
    {
        snprintf( buf, 256, "%d", 3);
        patch_prf_file( ALWAYS, prf, "monitor/1/m_monitor", buf );
        snprintf( buf, 256, "%d", scr.rc.x() );
        patch_prf_file( ALWAYS, prf, "rc.monitor/1/m_window_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.rc.y() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_window_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_window_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_window_bounds/3", buf );
        snprintf( buf, 256, "%d", scr.rc.x() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_arbitrary_bounds/0", buf );
        snprintf( buf, 256, "%d", scr.rc.y() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_arbitrary_bounds/1", buf );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_arbitrary_bounds/2", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_arbitrary_bounds/3", buf );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_is_fullscreen", "wmgr_mode_fullscreen" );
        //patch_prf_file( ALWAYS, prf, "monitor/1/m_usage", "wmgr_usage_ios" );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_usage", "wmgr_usage_normal_visuals" );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_refresh_rate", "60" );
        snprintf( buf, 256, "%d", scr.rc.width() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_x_res_full", buf );
        snprintf( buf, 256, "%d", scr.rc.height() );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_y_res_full", buf );
        patch_prf_file( ALWAYS, prf, "monitor/1/m_bpp", "32" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/diff_FOV", "0" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/FOVx_renopt", "50" );
        patch_prf_file( ALWAYS, prf, "monitor/0/proj/window_2d_off", "0" );
    }
}

/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QGuiApplication app(argc, argv);

    if (argc == 2 and std::string(argv[1]) == "reset")
    {
        reset = true;
    }

    std::cout << (reset ? "Reset all settings to default" : "Adjust monitor positions only (use 'reset' option to reset all)") << std::endl;

    assign_screens();
    set_backgrounds();

    patch_miscellaneous();
    patch_xplane();
    patch_positions();

    patch_prf_file( ALWAYS, "gfx.prf", "MODERN", "" );

    patch_prf_file( ALWAYS, "X-Plane Screen Res.prf", "_use_vsync1", "0" );
    patch_prf_file( ALWAYS, "X-Plane Analytics.prf", "_warn_update", "0" );

    return 0;
}

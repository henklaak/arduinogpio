#include "data_cmd.h"
#include <vector>
#include <memory>
#include <fstream>
#include <filesystem>
#include <algorithm>

#include <XPLMDataAccess.h>
#include <XPLMUtilities.h>

#include "adcdac.h"
#include "serialcomms.h"
#include "timer.h"
#include "max_plugin.h"
#include "debugwindow.h"

struct Command
{
    std::string mnem;
};

struct DataRef
{
    std::string mnem;
    std::string type;
    bool writeable;
    std::string unit;
    std::string comment;
};

struct CommandMap
{
    uint8_t hwid;
    uint8_t cs;
    uint8_t port;
    float level_lo;
    float level_hi;
    bool repeat;
    std::string mnem;
    int arraypos;

    XPLMCommandRef ref;
    bool in_range_prev;
};

enum DataRefType
{
    DATATYPE_FLOAT = 0,
    DATATYPE_BOOL,
};

struct DataRefMap
{
    uint8_t hwid;
    uint8_t cs;
    uint8_t port;
    uint8_t io;
    std::vector<std::pair<float, float>> lut;

    std::string mnem;
    int arraypos;

    XPLMDataRef ref;
};

bool g_connected = false;
int g_heartbeat = 0;
float g_portvoltages[3][20] {};

std::shared_ptr<AdcDac> s_adcdac;

bool s_mapping_ok = false;
bool s_configured = false;

std::vector<CommandMap> s_commandMapping;
std::vector<DataRefMap> s_dataRefMapping;

namespace fs = std::filesystem;

/**************************************************************************************************/
float LUT( float value, const std::vector<std::pair<float, float>> &lut, bool clamp = true )
{
    if( lut.empty() )
    {
        return value;
    }

    if( lut.size() == 1 )
    {
        return lut.front().second;
    }

    auto min_x = lut.front().first;
    auto max_x = lut.back().first;

    if( clamp )
    {
        value = std::min( value, max_x );
        value = std::max( value, min_x );
    }

    uint8_t xidx = 0;
    for( xidx = 0; xidx < lut.size() - 2; xidx++ )
    {
        if( value < lut[xidx + 1].first )
        {
            break;
        }
    }

    double fx = ( value - lut[xidx].first ) / ( lut[xidx + 1].first - lut[xidx].first );
    double interpolated = ( 1 - fx ) * lut[xidx].second + fx * lut[xidx + 1].second;
    return interpolated;
}

/**************************************************************************************************/
std::vector<Command> ReadCommands()
{
    std::vector<Command> commands;

    char systemPathString[512];

    XPLMGetSystemPath( systemPathString );
    fs::path commandsPath( systemPathString );
    commandsPath = commandsPath / "Resources" / "plugins" / "Commands.txt";

    log( commandsPath.string() );
    bool commandsFound = fs::exists( commandsPath );
    log( commandsFound ? "Commands.txt found \n" : "Commands.txt NOT found" );

    if( commandsFound )
    {
        std::ifstream infile;
        infile.open( commandsPath );

        // Read past first two lines
        std::string line;
        std::string delimiter = " ";
        while( std::getline( infile, line ) )
        {
            std::vector<std::string> tokens;
            size_t last = 0;
            size_t next = 0;
            while( ( next = line.find( delimiter, last ) ) != std::string::npos )
            {
                tokens.push_back( line.substr( last, next - last ) );
                last = next + 1;
            }
            tokens.push_back( line.substr( last ) );

            Command newCommand;

            if( tokens.size() < 2 )
            {
                log( "MALFORMED@0: " + line );
                continue;
            }

            newCommand.mnem = tokens[0];
            commands.push_back( newCommand );
        }
    }

    log( "Found " + std::to_string( commands.size() ) + " Command entries" );

    return commands;
}

/**************************************************************************************************/
std::vector<DataRef> ReadDataRefs()
{
    std::vector<DataRef> dataRefs;

    char systemPathString[512];

    XPLMGetSystemPath( systemPathString );
    fs::path dataRefPath( systemPathString );
    dataRefPath = dataRefPath / "Resources" / "plugins" / "DataRefs.txt";

    log( dataRefPath.string() );
    bool dataRefFound = fs::exists( dataRefPath );
    log( dataRefFound ? "DataRefs.txt found" : "DataRefs.txt NOT found" );

    if( dataRefFound )
    {
        std::ifstream infile;
        infile.open( dataRefPath );

        // Read past first two lines
        std::string dummyLine;
        std::getline( infile, dummyLine );
        std::getline( infile, dummyLine );

        std::string line;
        std::string delimiter = "\t";
        while( std::getline( infile, line ) )
        {
            std::vector<std::string> tokens;
            size_t last = 0;
            size_t next = 0;
            while( ( next = line.find( delimiter, last ) ) != std::string::npos )
            {
                tokens.push_back( line.substr( last, next - last ) );
                last = next + 1;
            }
            tokens.push_back( line.substr( last ) );

            DataRef newDataRef;

            if( tokens.size() < 3 )
            {
                log( "MALFORMED@1: " + line );
                continue;
            }
            if( tokens[2] != "y" and tokens[2] != "n" )
            {
                log( "MALFORMED@2: " + line );
                continue;
            }

            newDataRef.mnem = tokens[0];
            newDataRef.type = tokens[1];
            newDataRef.writeable = ( tokens[2] == "y" );
            dataRefs.push_back( newDataRef );
        }
    }

    log( "Found " + std::to_string( dataRefs.size() ) + " DataRef entries" );

    return dataRefs;
}

/**************************************************************************************************/
std::vector<CommandMap> ReadCommandMapping( std::vector<Command>, bool &ok )
{
    std::vector<CommandMap> mapping;
    char systemPathString[512];

    XPLMGetSystemPath( systemPathString );
    fs::path commandsPath( systemPathString );
    commandsPath = commandsPath / "Resources" / "plugins" / "AdcDacPlugin" / "64" / "commandsmap.txt";

    log( commandsPath.string() );
    bool commandsFound = fs::exists( commandsPath );
    log( commandsFound ? "commandsmap.txt found" : "commandsmap.txt NOT found" );

    if( commandsFound )
    {
        std::ifstream infile;
        infile.open( commandsPath );

        std::string line;
        std::string delimiter = ",";
        while( std::getline( infile, line ) )
        {
            if( line.size() == 0 or line[0] == '#' )
            {
                continue;
            }

            std::vector<std::string> tokens;
            size_t last = 0;
            size_t next = 0;
            while( ( next = line.find( delimiter, last ) ) != std::string::npos )
            {
                tokens.push_back( line.substr( last, next - last ) );
                last = next + 1;
            }
            tokens.push_back( line.substr( last ) );

            if( tokens.size() < 7 || tokens.size() > 8 )
            {
                log( "MALFORMED@3: " + line );
                continue;
            }

            CommandMap commandMap;

            commandMap.hwid = stoi( tokens[0] );
            commandMap.cs = stoi( tokens[1] );
            commandMap.port = stoi( tokens[2] );
            commandMap.level_lo = stof( tokens[3] );
            commandMap.level_hi = stof( tokens[4] );
            commandMap.repeat = stoi( tokens[5] );
            commandMap.mnem = tokens[6];
            commandMap.mnem.erase( std::remove( commandMap.mnem.begin(), commandMap.mnem.end(), ' ' ),
                                   commandMap.mnem.end() );
            commandMap.ref = XPLMFindCommand( commandMap.mnem.c_str() );
            commandMap.arraypos = ( tokens.size() ) > 7 ? stoi( tokens[7] ) : -1;
            if( !commandMap.ref )
            {
                log( "mnem " + commandMap.mnem + " not found" );
                continue;
            }

            commandMap.in_range_prev = false;

            mapping.push_back( commandMap );
        }

        ok = true;
    }

    log( "Found " + std::to_string( mapping.size() ) + " Command maps" );
    for( auto it = mapping.begin(); it != mapping.end(); ++it )
    {
        auto &map = *it;
        char buf[512];
        snprintf( buf, 512, "HWID:%02d CS:%02d PORT:%02d LVL:%5.2f LVL:%5.2f REP:%d MNEM:%s IDX:%d",
                  map.hwid,
                  map.cs,
                  map.port,
                  map.level_lo,
                  map.level_hi,
                  map.repeat,
                  map.mnem.c_str(),
                  map.arraypos );
        if( map.hwid > 0 )
        {
            log( buf );
        }
    }

    return mapping;
}

/**************************************************************************************************/
std::vector<DataRefMap> ReadDataRefMapping( std::vector<DataRef>, bool &ok )
{
    std::vector<DataRefMap> mapping;
    char systemPathString[512];

    XPLMGetSystemPath( systemPathString );
    fs::path dataRefsPath( systemPathString );
    dataRefsPath = dataRefsPath / "Resources" / "plugins" / "AdcDacPlugin" / "64" / "datarefsmap.txt";

    log( dataRefsPath.string() );
    bool dataRefsFound = fs::exists( dataRefsPath );
    log( dataRefsFound ? "datarefsmap.txt found" : "datarefsmap.txt NOT found" );

    if( dataRefsFound )
    {
        std::ifstream infile;
        infile.open( dataRefsPath );

        std::string line;
        std::string delimiter = ",";
        while( std::getline( infile, line ) )
        {
            if( line.size() == 0 or line[0] == '#' )
            {
                continue;
            }

            std::vector<std::string> tokens;
            size_t last = 0;
            size_t next = 0;
            while( ( next = line.find( delimiter, last ) ) != std::string::npos )
            {
                tokens.push_back( line.substr( last, next - last ) );
                last = next + 1;
            }
            tokens.push_back( line.substr( last ) );

            if( tokens.size() < 5 || tokens.size() > 6 )
            {
                log( "MALFORMED@4: " + line );
                continue;
            }

            DataRefMap dataRefMap{};

            try
            {
                dataRefMap.hwid = stoi( tokens[0] );
                dataRefMap.cs = stoi( tokens[1] );
                dataRefMap.port = stoi( tokens[2] );
                dataRefMap.io = stoi( tokens[3] );
                dataRefMap.mnem = tokens[4];
                dataRefMap.mnem.erase( std::remove( dataRefMap.mnem.begin(), dataRefMap.mnem.end(), ' ' ),
                                       dataRefMap.mnem.end() );
                dataRefMap.arraypos = ( tokens.size() > 5 ) ? stoi( tokens[5] ) : -1;
                dataRefMap.ref = XPLMFindDataRef( dataRefMap.mnem.c_str() );
            }
            catch( ... )
            {
                log( "Problem on line '" + line + "'" );
                continue;
            }

            if( !dataRefMap.ref )
            {
                log( "mnem " + dataRefMap.mnem + " not found" );
                continue;
            }


            bool lut_ended = false;
            while( !lut_ended )
            {
                std::getline( infile, line );
                if( line.size() == 0 )
                {
                    lut_ended = true;
                    continue;
                }

                std::vector<std::string> lut_tokens;
                last = 0;
                while( ( next = line.find( delimiter, last ) ) != std::string::npos )
                {
                    lut_tokens.push_back( line.substr( last, next - last ) );
                    last = next + 1;
                }
                lut_tokens.push_back( line.substr( last ) );

                if( lut_tokens.size() != 2 )
                {
                    log( "MALFORMED@5: " + line + " got " + std::to_string( lut_tokens.size() ) +
                         " tokens instead of 2" );
                    continue;
                }

                float x = stof( lut_tokens[0] );
                float y = stof( lut_tokens[1] );

                dataRefMap.lut.push_back( std::pair<float, float>( x, y ) );
            }

            sort( dataRefMap.lut.begin(), dataRefMap.lut.end() );
            mapping.push_back( dataRefMap );

        }

        ok = true;
    }

    log( "Found " + std::to_string( mapping.size() ) + " DataRef maps" );
    for( auto it = mapping.begin(); it != mapping.end(); ++it )
    {
        auto &map = *it;
        char buf[512];
        snprintf( buf, 512, "HWID:%02d CS:%02d PORT:%02d IO:%d MNEM:%s IDX:%d",
                  map.hwid,
                  map.cs,
                  map.port,
                  map.io,
                  map.mnem.c_str(),
                  map.arraypos );
        if( map.hwid > 0 )
        {
            log( buf );
        }
    }

    return mapping;
}

/**************************************************************************************************/
void InitDataCmdRefs()
{
    // Needed to get rid of crosshair
    XPLMDataRef refjs = XPLMFindDataRef( "sim/operation/override/override_joystick" );
    XPLMSetDatai( refjs, 1 );

    // Called once at initialize
    std::vector<Command> commands = ReadCommands();
    bool commands_ok = false;
    s_commandMapping = ReadCommandMapping( commands, commands_ok );

    std::vector<DataRef> dataRefs = ReadDataRefs();
    bool datarefs_ok = false;
    s_dataRefMapping = ReadDataRefMapping( dataRefs, datarefs_ok );

    // Mapping valid if we got at least one of each
    s_mapping_ok = commands_ok & datarefs_ok;
}

/**************************************************************************************************/
void ExitDataCmdRefs()
{
    XPLMDataRef refjs = XPLMFindDataRef( "sim/operation/override/override_joystick" );
    XPLMSetDatai( refjs, 0 );
}

/**************************************************************************************************/
void EnableArduino()
{
    if( !s_mapping_ok )
    {
        return;
    }

    // Called at each plugin enable

    std::vector<std::string> deviceNames = findArduinoDeviceNames();
    log( "Found " + std::to_string( deviceNames.size() ) + " Arduino device(s)" );

    for( auto it = deviceNames.begin(); it != deviceNames.end(); ++it )
    {
        std::string &deviceName = *it;
        log( "Found " + deviceName );
    }

    if( !deviceNames.empty() )
    {
        log( "Reset Arduino" );
        s_adcdac.reset( new AdcDac( deviceNames[0] ) );
    }

    if( s_adcdac )
    {
        log( +"Waiting for Arduino running...." );
        while( !s_adcdac->isConnected() or s_adcdac->getIdSelector() < 0 )
        {
            Timer::sleep_ms( 100 );
        }

        std::string fwVersion = s_adcdac->getFwVersion();
        log( "FW Version: " + fwVersion );

        int8_t idSelector = s_adcdac->getIdSelector();
        log( "ID selector: " + std::to_string( idSelector ) );
    }

    EnsureArduinoConfigured();
}

/**************************************************************************************************/
void EnsureArduinoConfigured()
{
    if( s_adcdac and !s_configured )
    {
        for( auto it = s_commandMapping.begin(); it != s_commandMapping.end(); ++it )
        {
            // All commands are implictly inputs
            CommandMap &map = *it;
            if( map.hwid == 1 )
            {
                s_adcdac->setPortConfig( map.cs, map.port, 1 );
                log( "PortConfig " + std::to_string( map.cs ) + " " + std::to_string( map.port ) + " " +
                     std::to_string( 1 ) );
            }
        }
        for( auto it = s_dataRefMapping.begin(); it != s_dataRefMapping.end(); ++it )
        {
            DataRefMap &map = *it;
            if( map.hwid == 1 )
            {
                s_adcdac->setPortConfig( map.cs, map.port, map.io );
                log( "PortConfig " + std::to_string( map.cs ) + " " + std::to_string( map.port ) + " " +
                     std::to_string( map.io ) );
            }
        }

        s_configured = true;
    }
}

/**************************************************************************************************/
void DisableArduino()
{
    // Called at each plugin disable

    if( s_adcdac )
    {
        s_adcdac.reset();
    }
}

/**********************************************************************/
float UpdateArduino()
{
    if( !s_mapping_ok or !s_adcdac )
    {
        log( "Initialization NOT ok" );
        return 0; // Never call me again
    }

    g_connected = s_adcdac->isConnected();

    if( !g_connected )
    {
        s_configured = false;
        log( "Not connected" );
        return 1.0; // call me again in 1 sec
    }

    EnsureArduinoConfigured();

    if( IsDebugWindowVisible() )
    {
        g_heartbeat = s_adcdac->getHeartbeat();
        for( int cs = 0; cs < 3; ++cs )
        {
            for( int port = 0; port < 20; ++port )
            {
                g_portvoltages[cs][port] = s_adcdac->getAnalogInputValue( cs, port ) * 10.0 / 4096;
            }
        }
    }

    // Commands
    for( auto it = s_commandMapping.begin(); it != s_commandMapping.end(); ++it )
    {
        CommandMap &map = *it;
        if( map.hwid != 1 )
        {
            continue;
        }

        float value = s_adcdac->getAnalogInputValue( map.cs, map.port ) * 10.0 / 4096;
        bool in_range = ( value >= map.level_lo &&
                          value < map.level_hi );

        // Check if value is with command trigger range
        if( in_range )
        {
            if( map.repeat )
            {
                // Repeating command every cycle
                XPLMCommandOnce( map.ref );
            }
            else
            {
                // Non repeating, only once when value enters range
                if( !map.in_range_prev )
                {
                    XPLMCommandOnce( map.ref );
                }
            }
        }

        map.in_range_prev = in_range;
    }

    // DataRefs
    for( auto it = s_dataRefMapping.begin(); it != s_dataRefMapping.end(); ++it )
    {
        DataRefMap &map = *it;
        if( map.hwid != 1 )
        {
            continue;
        }

        XPLMDataTypeID types = XPLMGetDataRefTypes( map.ref );

        double value_raw = 0.0;
        float value = 0.0;

        switch( map.io )
        {
        case 0:
            if( types & xplmType_FloatArray && map.arraypos >= 0 )
            {
                XPLMGetDatavf( map.ref, &value, map.arraypos, 1 );
            }
            else if( types & xplmType_Double )
            {
                value = XPLMGetDatad( map.ref );
            }
            else if( types & xplmType_Float )
            {
                value = XPLMGetDataf( map.ref );
            }
            else if( types & xplmType_Int )
            {
                value = XPLMGetDatai( map.ref );
            }
            else
            {
                value = 0;
                XPLMDebugString( "Unhandled types" );
            }
            value_raw = LUT( value, map.lut );
            s_adcdac->setAnalogOutputValue( map.cs, map.port, value_raw * 4096 / 10.0 );
            break;

        case 1:
            value_raw = s_adcdac->getAnalogInputValue( map.cs, map.port ) * 10.0 / 4096;
            value = LUT( value_raw, map.lut );
            if( types & xplmType_Double )
            {
                XPLMSetDatad( map.ref, value );
            }
            else if( types & xplmType_Float )
            {
                XPLMSetDataf( map.ref, value );
            }
            else if( types & xplmType_Int )
            {
                XPLMSetDatai( map.ref, value + 0.5 );
            }
            else if( types & xplmType_FloatArray && map.arraypos >= 0 )
            {
                XPLMSetDatavf( map.ref, &value, map.arraypos, 1 );
            }
            else
            {
                XPLMDebugString( "Unhandled types" );
            }
            break;
        }
    }

    return -1; // -1 = call me again next frame
}

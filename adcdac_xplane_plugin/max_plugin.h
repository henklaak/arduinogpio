#pragma once
#include <string>

void log( const std::string &txt );

void errorCallback( const char *msg );
void AdcDacPluginMenuHandler( void *mRef, void *iRef );
float AdcDacPluginFlightLoopCallback(
    float inElapsedSinceLastCall,
    float inElapsedTimeSinceLastFlightLoop,
    int   inCounter,
    void *inRefcon );



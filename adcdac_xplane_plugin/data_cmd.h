#pragma once

#include <memory>

void InitDataCmdRefs();
void ExitDataCmdRefs();
void EnableArduino();
void EnsureArduinoConfigured();
void DisableArduino();
float UpdateArduino();

extern bool g_connected;
extern int g_heartbeat;
extern float g_portvoltages[3][20];




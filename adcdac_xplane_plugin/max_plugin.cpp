#include "max_plugin.h"
#if IBM
#include <windows.h>
#endif
#include <string>
#include <string.h>
#include <stdio.h>

#include <XPLMDisplay.h>
#include <XPLMMenus.h>
#include <XPLMProcessing.h>
#include <XPLMPlugin.h>
#include <XPUIGraphics.h>
//#include <XPLM/XPLMUtilities.h>
//#include <XPLM/XPLMGraphics.h>


#include "data_cmd.h"
#include "debugwindow.h"

XPLMWindowID s_debug_window;

XPLMMenuID s_max_menu_id;

/**********************************************************************/
void log( const std::string &txt )
{
    XPLMDebugString( ( "com.delink.adcdac: " + txt + "\n" ).c_str() );
}

/**********************************************************************/
PLUGIN_API int XPluginStart( char *outName,
                             char *outSig,
                             char *outDesc )
{
    log( "Starting..." );

    strncpy( outName, "AdcDacPlugin", 255 );
    strncpy( outSig, "com.delink.adcdac", 255 );
    strncpy( outDesc, "AdcDacPlugin", 255 );

    XPLMSetErrorCallback( errorCallback );

    int item;
    item = XPLMAppendMenuItem( XPLMFindPluginsMenu(), "AdcDacPlugin", nullptr, 1 );
    s_max_menu_id = XPLMCreateMenu( "AdcDacPlugin", XPLMFindPluginsMenu(), item,
                                    AdcDacPluginMenuHandler, nullptr );
    XPLMAppendMenuItem( s_max_menu_id, "Reload", ( void * )"Reload", 1 );
    XPLMAppendMenuItem( s_max_menu_id, "Show debug info", ( void * )"ShowDebugInfo", 1 );

    CreateDebugWindow();

    log( "Started" );

    return 1; // SUCCESS
}

/**********************************************************************/
PLUGIN_API void XPluginStop()
{
    log( "Stopping..." );

    DestroyDebugWindow();
    XPLMDestroyMenu( s_max_menu_id );

    log( "Stopped" );
}

/**********************************************************************/
PLUGIN_API int XPluginEnable()
{
    char buf[1024];
    char outFilePath[256];

    log( "Enabling..." );

    InitDataCmdRefs();
    EnableArduino();

    XPLMCreateFlightLoop_t fl;
    fl.structSize = sizeof( XPLMCreateFlightLoop_t );
    fl.callbackFunc = AdcDacPluginFlightLoopCallback;
    fl.phase = xplm_FlightLoop_Phase_AfterFlightModel;
    fl.refcon = nullptr;

    XPLMFlightLoopID loopid = XPLMCreateFlightLoop( &fl );

    XPLMScheduleFlightLoop( loopid, -1, 0 ); //-1 call every frame

    log( "Enabled" );

    //XPLMSpeakString("Thank you for pressing the self destruct button. This would be a good time to disembark.");

    return 1; // SUCCESS
}

/**********************************************************************/
PLUGIN_API void XPluginDisable()
{
    log( "Disabling..." );

    XPLMUnregisterFlightLoopCallback( AdcDacPluginFlightLoopCallback, nullptr );

    ExitDataCmdRefs();

    EnsureDebugWindowVisible( false );
    DisableArduino();

    log( "Disabled" );
}

/**********************************************************************/
PLUGIN_API void XPluginReceiveMessage( XPLMPluginID inFromWho,
                                       int inMessage,
                                       void */*inParam*/ )
{
    char buf[1024];

    switch( inMessage )
    {
    case XPLM_MSG_PLANE_CRASHED:
    case XPLM_MSG_PLANE_LOADED:
    case XPLM_MSG_AIRPORT_LOADED:
    case XPLM_MSG_SCENERY_LOADED:
    case XPLM_MSG_AIRPLANE_COUNT_CHANGED:
    case XPLM_MSG_PLANE_UNLOADED:
    case XPLM_MSG_WILL_WRITE_PREFS:
    case XPLM_MSG_LIVERY_LOADED:
    case XPLM_MSG_ENTERED_VR:
    case XPLM_MSG_EXITING_VR:
    case XPLM_MSG_RELEASE_PLANES:
        break;
    default:
    {
        snprintf( buf, 1024, "inFromWho: %d\n", inFromWho );
        log( buf );
        snprintf( buf, 1024, "inMessage: %d\n", inMessage );
        log( buf );
    }
    }
}

/**********************************************************************/
void errorCallback( const char *msg )
{
    char buf[1024];
    snprintf( buf, 1024, "ErrorCallback: %s\n", msg );
    log( buf );
}

/**********************************************************************/
void AdcDacPluginMenuHandler( void */*mRef*/, void *iRef )
{
    if( !strncmp( ( char * )iRef, "Reload", 6 ) )
    {
        XPLMReloadPlugins();
    }
    else if( !strncmp( ( char * )iRef, "ShowDebugInfo", 13 ) )
    {
        EnsureDebugWindowVisible( true );
    }
}

/**********************************************************************/
float AdcDacPluginFlightLoopCallback( float /*inElapsedSinceLastCall*/,
                                      float /*inElapsedTimeSinceLastFlightLoop*/,
                                      int   inCounter,
                                      void */*inRefcon*/ )
{
    bool show_debug = !g_connected;

    if( show_debug )
    {
        EnsureDebugWindowVisible( true );
    }
    else
    {
        EnsureDebugWindowVisible( false );
    }

    UpdateDebugWindowData();

    return UpdateArduino();
}

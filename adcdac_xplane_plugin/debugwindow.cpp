#include "debugwindow.h"
#include <XPLMDisplay.h>
#include <XPLMGraphics.h>
#include <GL/gl.h>

#include "max_plugin.h"
#include "data_cmd.h"
#include "adcdac.h"

XPLMWindowID s_mainwindow;

static unsigned char col = 0;

/*****************************************************************************/
void drawWindowFunc( XPLMWindowID inWindowID, void *inRefcon )
{
    int left, top, right, bottom;
    XPLMGetWindowGeometry( inWindowID, &left, &top, &right, &bottom );

    XPLMDrawTranslucentDarkBox( left, top, right, bottom );

    char buf[256];

    if( g_connected )
    {
        float rgb[3] = {0.5, 1, 0.5};
        snprintf( buf, 256, "ADC/DAC board connected" );
        XPLMDrawString( rgb, left + 10, top - 20, buf, 0, xplmFont_Proportional );
    }
    else
    {
        float rgb[3] = {1.0, 0.5, 0.5};
        snprintf( buf, 256, "ADC/DAC board disconnected" );
        XPLMDrawString( rgb, left + 10, top - 20, buf, 0, xplmFont_Proportional );
    }

    snprintf( buf, 256, "%d", g_heartbeat );

    float rgb[3] = {1.0, 1.0, 1.0};
    for( int cs = 0; cs < 3; ++cs )
    {
        for( int port = 0; port < 20; ++port )
        {
            int column = int( port / 10 );
            int row = port % 10;
            int block = int( row / 5 );
            int left2 = ( left + 10 ) + column * 65 + cs * 150;
            int top2 = ( top - 40 ) - 20 * row - 10 * block;
            snprintf( buf, 256, "%6.3lf", g_portvoltages[cs][port] );
            XPLMDrawString( rgb, left2, top2, buf, 0, xplmFont_Proportional );
        }
    }


    col += 8;
    XPLMSetGraphicsState( 0, 0, 0, 0, 0, 0, 0 );

    glBegin( GL_LINE_STRIP );            // Each set of 4 vertices form a quad
    glColor4f( col / 255., 0.0f, 1.0f, col / 255. ); // Red
    glVertex2f( left, top );
    glVertex2f( right, top );
    glVertex2f( right, bottom );
    glVertex2f( left, bottom );
    glVertex2f( left, bottom );
    glVertex2f( left, top );
    glEnd();
}

/*****************************************************************************/
XPLMCursorStatus handleCursorFunc(
    XPLMWindowID inWindowID, int x, int y, void *inRefcon )
{
    return xplm_CursorDefault;
}

/*****************************************************************************/
void handleKeyFunc( XPLMWindowID         inWindowID,
                    char                 inKey,
                    XPLMKeyFlags         inFlags,
                    char                 inVirtualKey,
                    void *               inRefcon,
                    int                  losingFocus )
{

}

/*****************************************************************************/
int handleMouseClickFunc( XPLMWindowID         inWindowID,
                          int                  x,
                          int                  y,
                          XPLMMouseStatus      inMouse,
                          void *               inRefcon )
{
    return 0;
}

/*****************************************************************************/
int handleMouseWheelFunc( XPLMWindowID         inWindowID,
                          int                  x,
                          int                  y,
                          int                  wheel,
                          int                  clicks,
                          void *               inRefcon )
{
    return 0;
}

/*****************************************************************************/
void CreateDebugWindow()
{
    log( "Create Debug Window" );

    int left, top, right, bottom;
    XPLMGetScreenBoundsGlobal( &left, &top, &right, &bottom );
    int width = right - left;
    int height = top - bottom;

    int x1 = width * 0.2;
    int y1 = height * 0.8;
    int x2 = x1 + 450;
    int y2 = y1 - 250;

    XPLMCreateWindow_t params{};
    params.structSize = sizeof( XPLMCreateWindow_t );
    params.left = x1;
    params.top = y1;
    params.right = x2;
    params.bottom = y2;
    params.visible = 0;
    params.decorateAsFloatingWindow = xplm_WindowDecorationRoundRectangle;
    params.layer =  xplm_WindowLayerFloatingWindows;
    params.drawWindowFunc = drawWindowFunc;
    params.handleCursorFunc = handleCursorFunc;
    params.handleKeyFunc = handleKeyFunc;
    params.handleMouseClickFunc = handleMouseClickFunc;
    params.handleMouseWheelFunc = handleMouseWheelFunc;
    params.refcon = nullptr;

    s_mainwindow = XPLMCreateWindowEx( &params );
    XPLMSetWindowTitle( s_mainwindow, "Hardware Diagnostics" );
}

/*****************************************************************************/
void DestroyDebugWindow()
{
    XPLMDestroyWindow( s_mainwindow );
}

/*****************************************************************************/
void EnsureDebugWindowVisible( bool visible )
{
    if( visible && !IsDebugWindowVisible() )
    {
        XPLMSetWindowIsVisible( s_mainwindow, 1 );
    }
    else if( !visible && IsDebugWindowVisible() )
    {
        XPLMSetWindowIsVisible( s_mainwindow, 0 );
    }
}

/*****************************************************************************/
void UpdateDebugWindowData()
{

}

/*****************************************************************************/
bool IsDebugWindowVisible()
{
    return XPLMGetWindowIsVisible( s_mainwindow );
}

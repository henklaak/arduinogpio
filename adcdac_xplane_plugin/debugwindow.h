#pragma once

void CreateDebugWindow();
void DestroyDebugWindow();
void EnsureDebugWindowVisible( bool visible );
bool IsDebugWindowVisible();
void UpdateDebugWindowData();



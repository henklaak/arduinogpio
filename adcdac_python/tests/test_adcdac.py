import pytest

# Public imports
from adcdac import AdcDac
from adcdac import __version__
from adcdac import find_adcdac_device_names
# Private imports
from adcdac.api import _clamp, _pack12bits, _unpack12bits

# COMPORT =
VERSION = __version__
ID_SELECTOR = 1


def test_clamp():
    assert _clamp(20, 10, 30) == 20
    assert _clamp(0, 10, 30) == 10
    assert _clamp(0, -30, -10) == -10


def test_pack12bits_even():
    packed = _pack12bits([0x123])
    assert (len(packed) == 2)
    assert packed[0] == 0x23
    assert packed[1] == 0x01

    packed = _pack12bits([0x123, 0x456])
    assert (len(packed) == 3)
    assert packed[0] == 0x23
    assert packed[1] == 0x61
    assert packed[2] == 0x45

    packed = _pack12bits([0x123, 0x456, 0x789])
    assert (len(packed) == 5)
    assert packed[0] == 0x23
    assert packed[1] == 0x61
    assert packed[2] == 0x45
    assert packed[3] == 0x89
    assert packed[4] == 0x07


def test_pack12bits_odd():
    packed = _pack12bits([0x123], odd=True)
    assert (len(packed) == 2)
    assert packed[0] == 0x30
    assert packed[1] == 0x12

    packed = _pack12bits([0x123, 0x456], odd=True)
    assert (len(packed) == 4)
    assert packed[0] == 0x30
    assert packed[1] == 0x12
    assert packed[2] == 0x56
    assert packed[3] == 0x04

    packed = _pack12bits([0x123, 0x456, 0x789], odd=True)
    assert (len(packed) == 5)
    assert packed[0] == 0x30
    assert packed[1] == 0x12
    assert packed[2] == 0x56
    assert packed[3] == 0x94
    assert packed[4] == 0x78


def test_unpack12bits_even():
    packed = _pack12bits([0x123])
    unpacked = _unpack12bits(packed)
    assert len(unpacked) == 1
    assert unpacked[0] == 0x123

    packed = _pack12bits([0x123, 0x456])
    unpacked = _unpack12bits(packed)
    assert len(unpacked) == 2
    assert unpacked[0] == 0x123
    assert unpacked[1] == 0x456

    packed = _pack12bits([0x123, 0x456, 0x789])
    unpacked = _unpack12bits(packed)
    assert len(unpacked) == 3
    assert unpacked[0] == 0x123
    assert unpacked[1] == 0x456
    assert unpacked[2] == 0x789


def test_unpack12bits_odd():
    packed = _pack12bits([0x123], odd=True)
    unpacked = _unpack12bits(packed, odd=True)
    assert len(unpacked) == 1
    assert unpacked[0] == 0x123

    packed = _pack12bits([0x123, 0x456], odd=True)
    unpacked = _unpack12bits(packed, odd=True)
    assert len(unpacked) == 2
    assert unpacked[0] == 0x123
    assert unpacked[1] == 0x456

    packed = _pack12bits([0x123, 0x456, 0x789], odd=True)
    unpacked = _unpack12bits(packed, odd=True)
    assert len(unpacked) == 3
    assert unpacked[0] == 0x123
    assert unpacked[1] == 0x456
    assert unpacked[2] == 0x789


@pytest.fixture(scope='session')
def comport():
    comports = find_adcdac_device_names()
    assert len(comports) > 0
    return comports[0]


@pytest.fixture(scope='session')
def io():
    comports = find_adcdac_device_names()
    assert len(comports) > 0
    with AdcDac(comports[0]) as io:
        yield io


def test_device_name(io, comport):
    assert io.device_name == comport


def test_repr_str(comport):
    io = AdcDac(comport)
    assert repr(io) == f"AdcDac(device_name='{comport}')"
    assert str(io) == f"AdcDac, device_name='{comport}', connected='False'"


def test_connection(comport):
    io = AdcDac(comport)
    assert not io.is_connected()

    io.connect()
    assert io.is_connected()

    io.disconnect()
    assert not io.is_connected()


def test_context_connection(comport):
    with AdcDac(comport) as io:
        assert io.is_connected()

    assert not io.is_connected()


def test_get_fw_version(io):
    assert io.get_fw_version() == VERSION


def test_get_id_selector(io):
    assert io.get_id_selector() == ID_SELECTOR


def test_get_heartbeat(io):
    hb1 = io.get_heartbeat()
    hb2 = io.get_heartbeat()
    diff_hb = hb2 - hb1
    assert diff_hb > 0
    assert diff_hb < 1000  # ~1 sec


def test_reset(io):
    io.reset()
    hb1 = io.get_heartbeat()
    assert hb1 > 0
    assert hb1 < 1000  # ~1 sec


def test_get_supplies(io):
    supplies = io.get_supplies()
    assert 11 < supplies['V0'] < 13
    assert 11 < supplies['V1'] < 13
    assert 11 < supplies['V2'] < 13
    assert 0 < supplies['I0'] < 50
    assert 0 < supplies['I1'] < 50
    assert 0 < supplies['I2'] < 50
    assert 9 < supplies['10V'] < 11
    assert 11 < supplies['12V'] < 13


def test_get_temperatures(io):
    temperatures = io.get_temperatures()
    assert 15 < temperatures[0] < 35
    assert 15 < temperatures[1] < 35
    assert 15 < temperatures[2] < 35


def test_read_register(io):
    for cs in range(3):
        value = io.read_register(cs, 0)
        assert value == 0x0424


def test_write_register(io):
    for cs in range(3):
        io.write_register(cs, 0x0d, 0x1234)
        value = io.read_register(cs, 0x0d)
        assert value == 0x1234
        io.write_register(cs, 0x0d, 0x0)
        value = io.read_register(cs, 0x0d)
        assert value == 0x0000


def test_set_port_mode(io):
    for cs in range(3):
        for port in range(20):
            io.set_port_mode(cs, port, io.PORT_MODE_OUTPUT)
            value = io.read_register(cs, 0x20 + port)
            assert value == 0x6100

            io.set_port_mode(cs, port, io.PORT_MODE_INPUT)
            value = io.read_register(cs, 0x20 + port)
            assert value == 0x7180

            io.set_port_mode(cs, port, io.PORT_MODE_NOT_CONNECTED)
            value = io.read_register(cs, 0x20 + port)
            assert value == 0x0000


def test_read_adc(io):
    for cs in range(3):
        for port in range(20):
            io.set_port_mode(cs, port, io.PORT_MODE_INPUT)

    for cs in range(3):
        values = [0] * 20
        for port in range(20):
            values = io.read_adc(cs)
            assert len(values) == 20

        for port in range(0, 20):
            if cs == 0 and port == 0:
                assert (4.0 < values[port] < 6.0)
            else:
                assert (0.0 <= values[port] < 1.0)

        for port in range(20):
            value = io.read_adc(cs, port)
            assert value == values[port]

        for port in range(20):
            value = io.read_register(cs, 0x40 + port) / 4095 * 10.0
            assert abs(value - values[port]) < 1.0


def test_read_dac(io):
    for cs in range(3):
        values = io.read_dac(cs)
        for value in values:
            assert (value == 0)

    for cs in range(3):
        for port in range(20):
            value = io.read_dac(cs, port)
            assert (value == 0)


def test_write_dac(io):
    io.reset()

    all_set_points = [{0: 1.1, 1: 1.0, 2: 1.0, 3: 1.0, 4: 1.0,
                       5: 1.0, 6: 1.0, 7: 1.0, 8: 1.0, 9: 1.0,
                       10: 1.0, 11: 1.0, 12: 1.0, 13: 1.0, 14: 1.0,
                       15: 1.0, 16: 1.0, 17: 1.0, 18: 1.0, 19: 1.0,
                       }, {3: 4.123}, {7: 3.333, 18: 3.333}]

    for cs, set_points in enumerate(all_set_points):
        io.write_dac(cs, set_points)

        values = io.read_dac(cs)
        for port, value in set_points.items():
            assert pytest.approx(values[port], rel=0.005) == value


@pytest.mark.skip(reason="Development only")
def test_update_firmware(comport):
    io = AdcDac(comport)
    assert io.update_firmware()

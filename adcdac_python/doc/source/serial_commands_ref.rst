Serial command reference
------------------------

.. todo::
    Write docs

Get Version
~~~~~~~~~~~

Return the FW version as an ASCII string. The returned string length is variable and passed as part of the reply.

+-------+-----------+-----------------+
| **Command**                         |
+-------+-----------+-----------------+
| Byte  | Content   + Comment         |
+=======+===========+=================+
| 0     | 100       | CMD_GET_VERSION |
+-------+-----------+-----------------+

+-------+-----------+-----------------+
| **Reply**                           |
+-------+-----------+-----------------+
| Byte  | Content   + Comment         |
+=======+===========+=================+
| 0     | 100       | CMD_GET_VERSION |
+-------+-----------+-----------------+
| 1     | N         | String length   |
+-------+-----------+-----------------+
| 2     | 'A'       | First character |
+-------+-----------+-----------------+
| ...   | ...       |                 |
+-------+-----------+-----------------+
| N + 1 | 'Z'       | Last character  |
+-------+-----------+-----------------+

Note:
In practical implementations, the first 2 bytes of the Reply structure will have to be read first
and depending on the content, N additional bytes will have to be read.

Get Heartbeat
~~~~~~~~~~~~~
The processor keeps a 32-bit iteration counter that is incremented at each call of **loop()**.
It wraps around after several months, depending on average loop interval.

+-------+-----------+-------------------+
| **Command**                           |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 101       | CMD_GET_HEARTBEAT |
+-------+-----------+-------------------+

+-------+-----------+-------------------+
| **Reply**                             |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 101       | CMD_GET_HEARTBEAT |
+-------+-----------+-------------------+
| 1-4   | UINT32    | Heartbeat         |
+-------+-----------+-------------------+

Get State
~~~~~~~~~
Each MAX maintains a state word, which holds current and past warnings.
The Clear Warnings command will clear this state word.

+-------+-----------+-------------------+
| **Command**                           |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 102       | CMD_STATE         |
+-------+-----------+-------------------+

+-------+-----------+-------------------+
| **Reply**                             |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 102       | CMD_STATE         |
+-------+-----------+-------------------+
| 1-2   | UINT16    | State MAX #0      |
+-------+-----------+-------------------+
| 3-4   | UINT16    | State MAX #1      |
+-------+-----------+-------------------+
| 5-6   | UINT16    | State MAX #2      |
+-------+-----------+-------------------+

+-------+--------------------+------------+
| **State word**                          |
+-------+--------------------+------------+
| Bit   | Content            + Mask       +
+=======+====================+============+
| 5     | Overcurrent        | 0x0020     |
+-------+--------------------+------------+
| 7     | Temperature low    | 0x0080     |
+-------+--------------------+------------+
| 8     | Temperature high   | 0x0100     |
+-------+--------------------+------------+
| 15    | Supply voltage low | 0x8000     |
+-------+--------------------+------------+

Get OC State
~~~~~~~~~~~~
Gets the current and past overcurrent state for each individual MAX port.
The Clear Warnings command will clear this state word.

+-------+-----------+-------------------+
| **Command**                           |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 103       | CMD_OC_STATE      |
+-------+-----------+-------------------+

+-------+-----------+-------------------+
| **Reply**                             |
+-------+-----------+-------------------+
| Byte  | Content   + Comment           |
+=======+===========+===================+
| 0     | 103       | CMD_OC_STATE      |
+-------+-----------+-------------------+
| 1-4   | UINT32    | OC State MAX #0   |
+-------+-----------+-------------------+
| 5-8   | UINT32    | OC State MAX #1   |
+-------+-----------+-------------------+
| 9-12  | UINT32    | OC State MAX #2   |
+-------+-----------+-------------------+

+-------+--------------------+-------------+
| **OC word**                              |
+-------+--------------------+-------------+
| Bit   | Content            + Mask        +
+=======+====================+=============+
| 0-19  | Overcurrent port   | 0x000fffff  |
+-------+--------------------+-------------+

Clear Warnings
~~~~~~~~~~~~~~

Clear the State and OC State warnings.

+-------+-----------+--------------------+
| **Command**                            |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 104       | CMD_CLEAR_WARNINGS |
+-------+-----------+--------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 104       | CMD_CLEAR_WARNINGS |
+-------+-----------+--------------------+

ID Selector
~~~~~~~~~~~

Gets the position of the on board ID selector rotary switch, which can be 0-15.
By convention, 0 (factory default) means 'not set up yet, do not use'.
Although not recommended, client software is free to deviate from this convention

+-------+-----------+--------------------+
| **Command**                            |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 105       | CMD_ID_SELECTOR    |
+-------+-----------+--------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 105       | CMD_ID_SELECTOR    |
+-------+-----------+--------------------+
| 1     | ID        | ID selector        |
+-------+-----------+--------------------+


Supplies
~~~~~~~~

Get the diagnostic values for the MAX electrical supplies.
Values are raw binary sample values and need a conversion factor to get physical values.
These factors are based on a set of on board resistors. They are precise, but convoluted.

+-------+-----------+--------------------+
| **Command**                            |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 106       | CMD_SUPPLIES       |
+-------+-----------+--------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 106       | CMD_SUPPLIES       |
+-------+-----------+--------------------+
| 1-2   | UINT16    | Voltage MAX #0     |
+-------+-----------+--------------------+
| 3-4   | UINT16    | Voltage MAX #1     |
+-------+-----------+--------------------+
| 5-6   | UINT16    | Voltage MAX #2     |
+-------+-----------+--------------------+
| 7-8   | UINT16    | Current MAX #0     |
+-------+-----------+--------------------+
| 9-10  | UINT16    | Current MAX #1     |
+-------+-----------+--------------------+
| 11-12 | UINT16    | Current MAX #2     |
+-------+-----------+--------------------+
| 13-14 | UINT16    | Supply voltage 10V |
+-------+-----------+--------------------+
| 15-16 | UINT16    | Supply voltage 12V |
+-------+-----------+--------------------+

+-------------+--------------+------------------+------------+
| **Conversion factor**                         |            |
+-------------+--------------+------------------+------------+
| Raw value   | Factor       + Physical value   + Full scale +
+=============+==============+==================+============+
| Voltage MAX | 0.00366300   | Voltage [V]      | 15.000     |
+-------------+--------------+------------------+------------+
| Current MAX | 0.244200     | Current [mA]     |  1.000     |
+-------------+--------------+------------------+------------+
| Voltage 10V | 0.00359848   | Voltage [V]      | 14.736     |
+-------------+--------------+------------------+------------+
| Voltage 12V | 0.00358855   | Voltage [V]      | 14.695     |
+-------------+--------------+------------------+------------+

Temperatures
~~~~~~~~~~~~

+-------+-----------+--------------------+
| **Command**                            |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 107       | CMD_TEMPERATURES   |
+-------+-----------+--------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 107       | CMD_TEMPERATURES   |
+-------+-----------+--------------------+
| 1-2   | UINT16    | Temperature MAX #0 |
+-------+-----------+--------------------+
| 3-4   | UINT16    | Temperature MAX #1 |
+-------+-----------+--------------------+
| 5-6   | UINT16    | Temperature MAX #2 |
+-------+-----------+--------------------+

Reset
~~~~~

Perform a warm reboot on the MAX devices. Internal state will be reset to power on values.

+-------+-----------+--------------------+
| **Command**                            |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 108       | CMD_RESET          |
+-------+-----------+--------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 108       | CMD_RESET          |
+-------+-----------+--------------------+


Set Port Mode
~~~~~~~~~~~~~

Set port modes on a single port of a MAX device.

+-------+----------------+------------------------------+
| **Command**                                           |
+-------+----------------+------------------------------+
| Byte  | Content        + Comment                      |
+=======+================+==============================+
| 0     | 109            | CMD_SET_PORTMODE             |
+-------+----------------+------------------------------+
| 1     | device         | Selected MAX device: 0, 1, 2 |
+-------+----------------+------------------------------+
| 2     | bits 5-0: port | Selected port: 0-19          |
+-------+----------------+------------------------------+
|       | bits 7-6: mode | Selected mode:               |
|       |                | 0 - Output,                  |
|       |                | 1 - Input,                   |
|       |                | 2 - Not Connected            |
+-------+----------------+------------------------------+

+-------+-----------+--------------------+
| **Reply**                              |
+-------+-----------+--------------------+
| Byte  | Content   + Comment            |
+=======+===========+====================+
| 0     | 109       | CMD_SET_PORTMODE   |
+-------+-----------+--------------------+

ADC Data
~~~~~~~~

Get the ADC data. This command will only return the values of ports that have changed since
the last call. A bitmask early in the reply will indicate which ports are included in he remainder.

+-------+----------------+---------------+
| **Command**                            |
+-------+----------------+---------------+
| Byte  | Content        + Comment       |
+=======+================+===============+
| 0     | 110            | CMD_ADC_DATA  |
+-------+----------------+---------------+

+-------+----------------+------------------------------+
| **Reply**                                             |
+-------+----------------+------------------------------+
| Byte  | Content        + Comment                      |
+=======+================+==============================+
| 0     | 110            | CMD_ADC_DATA                 |
+-------+----------------+------------------------------+
| 1     | Bits 7-0       | Port mask  7- 0              |
+-------+----------------+------------------------------+
| 2     | Bits 7-0       | Port mask 15- 8              |
+-------+----------------+------------------------------+
| 3     | Bits 3-0       | Port mask 19-16              |
+-------+----------------+------------------------------+
|       | Bits 7-4       | Beginning of packed ADC data |
+-------+----------------+------------------------------+
| ...   |                | ...                          |
+-------+----------------+------------------------------+
| N     |                | End of packed ADC data       |
+-------+----------------+------------------------------+

**Packed data**
ADC data consists of 12-bit values, whose nibbles are packed LSN to MSN as a byte sequence.
The Port Mask will indicate which ports are included in the reply in order of port number.

Example:

If the Port mask has bits set for ports 1,2 and 5, the reply will contain 3 12-bit values,
whose 9 nibbles will be packed into 4 and a half bytes. In the Reply structure above, the first
nibble will have been placed into the upper half of mask byte 3. The remaining 8 nibbles will
have been packed into 4 additional bytes, so N will be 7 (3 for the mask and 4 for the data).

In practical implementations, the first 4 bytes will have to be read first and depending on the
content, N-3 additional bytes will have to be read.

A similar packing pattern is used for DAC data (see below).

DAC Data
~~~~~~~~
    - #define CMD_DAC_DATA       (111)

.. todo::
    Write docs

Write Reg
~~~~~~~~~
    - #define CMD_WRITE_REG      (112)

.. todo::
    Write docs


Read Reg
~~~~~~~~
    - #define CMD_READ_REG       (113)

.. todo::
    Write docs

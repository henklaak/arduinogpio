﻿adcdac.AdcDac
=============

.. currentmodule:: adcdac

.. autoclass:: AdcDac

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~AdcDac.PORT_MODE_INPUT
      ~AdcDac.PORT_MODE_NOT_CONNECTED
      ~AdcDac.PORT_MODE_OUTPUT
      ~AdcDac.STATUS_BIT_DAC_OVER_CURRENT
      ~AdcDac.STATUS_BIT_SUPPLY_VOLTAGE_LOW
      ~AdcDac.STATUS_BIT_TEMPERATURE_HIGH
      ~AdcDac.STATUS_BIT_TEMPERATURE_LOW
      ~AdcDac.STATUS_MASK_ERROR
      ~AdcDac.device_name
   
   

   

   
   .. rubric:: Methods

   .. autosummary::
   
      ~AdcDac.__init__
      ~AdcDac.connect
      ~AdcDac.disconnect
      ~AdcDac.get_fw_version
      ~AdcDac.get_heartbeat
      ~AdcDac.get_id_selector
      ~AdcDac.get_supplies
      ~AdcDac.get_temperatures
      ~AdcDac.is_connected
      ~AdcDac.read_adc
      ~AdcDac.read_dac
      ~AdcDac.read_error_states
      ~AdcDac.read_oc_states
      ~AdcDac.read_register
      ~AdcDac.reset
      ~AdcDac.set_port_mode
      ~AdcDac.update_firmware
      ~AdcDac.write_dac
      ~AdcDac.write_register

   
   .. automethod:: __init__
   .. automethod:: connect
   .. automethod:: disconnect
   .. automethod:: get_fw_version
   .. automethod:: get_heartbeat
   .. automethod:: get_id_selector
   .. automethod:: get_supplies
   .. automethod:: get_temperatures
   .. automethod:: is_connected
   .. automethod:: read_adc
   .. automethod:: read_dac
   .. automethod:: read_error_states
   .. automethod:: read_oc_states
   .. automethod:: read_register
   .. automethod:: reset
   .. automethod:: set_port_mode
   .. automethod:: update_firmware
   .. automethod:: write_dac
   .. automethod:: write_register

   
   






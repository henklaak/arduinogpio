API reference
=============

.. currentmodule:: adcdac

.. autosummary::
   :nosignatures:
   :toctree: generated

   find_adcdac_device_names

.. autosummary::
   :nosignatures:
   :toctree: generated
   :template: class.rst

   AdcDac

Firmware documentation
======================

Context
-------
The processor on the adcdac board is an *Arm® Cortex®-M0 32-bit SAMD21*

This same processor is used by the *Arduino® Nano 33 IoT* device, which makes it possible
to use common Arduino® tooling, such as compiler/linker, libraries, programmers and debuggers
to develop the adcdac board firmware.

Note: Except for the processor, the adcdac board shares none of the features of the Arduino Nano.
It has a different power supply, no bluetooth, WiFi or any of the motion sensors of the Arduino Nano.

Structure
---------
On the firmware top level, **setup()** which will be called immediately after boot.

After that, **loop()** will be called periodically.

The implementation is shown here:

.. image:: FW_init.png
   :scale: 100%

Setup
~~~~~

IO setup
........

In *IO setup*, the processor is configured for 12-bits ADC with an external reference
and its IO pins are configured for communicate with the onboard devices.

.. image:: FW_io_setup.png
   :scale: 100%

MAXs setup
..........
In *MAXs setup*, the processor's SPI port is configured to communicate with the three MAX
devices. Using SPI, the MAX devices are configured for adcdac operations.

.. image:: FW_max_setup.png
   :scale: 100%

In **Reset MAXs**, the MAX reset bit will be set, which will trigger a MAX warm reboot.
The reset bit will be read back until it is reset, with which the MAX signals that reboot is complete.

In **Configure interrupt masks**, the conditions will be selected that trigger a hardware interrupt from
the MAX to the processor. This interrupt line is not used by the processor, but it **is** connected to an
onboard red LED, which will provide a visiual indication that the MAX has an interruptable condition.

The possible conditions are:

    - overcurrent in any of the ports
    - over or under temperature
    - supply voltage drop out

In **Configure temperature limits**, the temperature limits for the MAX device are are set to 10-70 ℃.

When the MAX temperature goes outside of these limits, an interrupt will be signaled as stated above. To prevent

When the MAX temperature exceeds the upper limit, all ports will be put in *Not Connected* mode, which means
they will be high impedance. This is done to prevent device damage due to overheating.

In **Configure periodic sampling**, the sample rate and sampling averaging count are configured.

In **Configure all ports**, all ports are set to *Not Connected*, which puts them in a high impedance state.

Serial setup
............
In *Serial setup*, the processor opens an UsbSerial port and **waits** until a client connects to it.
The baudrate is set at 115200, but the underlying UsbSerial driver will ignore the setting and communicate at
the maximum attainable USB2 speed.

Note: The adcdac board has a known issue with USB3 under Microsoft® Windows®. When encountering problems, try
to connecting using a USB2 port or using a USB2 hub.

Loop
~~~~
The **loop()** function will be executed periodically at a best effort rate. In practice, the minimum interval
will be around 1 millisecond.

IO sync
.......
In *IO sync*, the processor will read its 8 ADC channels, which are used to measure supply voltage and supply
currents to the MAX devices. It will also read the *id_selector* position using 4 Digital Inputs.

.. image:: FW_io_sync.png
   :scale: 100%

MAXs sync
.........
In *IO sync*, the core functionality of the adcdac board is carried out.

.. todo::
    Write docs

Serial sync
...........

.. todo::
    Write docs


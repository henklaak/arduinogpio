""" Class that controls a single adcdac board. """

import os
import re
import struct
import subprocess
import sys
from copy import copy
from math import floor
from time import sleep
from typing import List, Dict, Tuple, Union
from warnings import warn

import serial
import serial.tools.list_ports as lp

__version__ = "2022.08.09.0"

HERE = os.path.abspath(os.path.dirname(__file__))


def find_adcdac_device_names() -> List[str]:
    """ Find connected adcdac devices.

    Returns:
        List of device names for connected AdcDac devices. Devices are USBSerial ports.

    Examples:
        >>> len(find_adcdac_device_names())  # Count devices
        1

        >>> find_adcdac_device_names()[0]  # Find first device
        'COM4'


    """
    # Our bootloader identifies as Arduino Nano 33 IoT
    valid_vid = 0x2341  # Vendor ID: Arduino
    valid_pids = (0x0057, 0x8057)  # Product IDs: Nano 33 IoT flavours

    devices = []
    for cp in lp.comports():
        if cp.vid != valid_vid:
            continue
        if cp.pid not in valid_pids:
            continue
        devices.append(cp.device)
    return devices


def _clamp(num: float, min_value: float, max_value: float) -> float:
    """ Clamp value between and including minimum and maximum value.

    Args:
        num: Arbitrary input value
        min_value: Allowed minimum value
        max_value: Allowed maximum value

    Returns:
        Clamped value;

    Examples:
        >>> _clamp(20.0, 10.0, 30.0)  # Clamp 10 between 10 and 30
        20.0
        >>> _clamp(31.0, 10.0, 30.0)  # Clamp 31 between 10 and 30
        30.0
        >>> _clamp(2.0, 10.0, 30.0)  # Clamp 2 between 10 and 30
        10.0

    """
    return max(min(num, max_value), min_value)


def _pack12bits(input_data: List[int], odd=False) -> bytes:
    """
    Even:                     Odd:
    x123   ->   23            x123   ->   3x
    x456        61            x456        12
    x789        45            x789        56
                89                        94
                x7                        78
    """

    nibbles = bytearray()
    if odd:
        nibbles.append(0)
    for a in input_data:
        nibbles.append((a >> 0) & 0xf)
        nibbles.append((a >> 4) & 0xf)
        nibbles.append((a >> 8) & 0xf)

    # Make sure the list is even
    if len(nibbles) % 2:
        nibbles.append(0)

    output = bytearray()
    while nibbles:
        lo = nibbles.pop(0)
        hi = nibbles.pop(0)
        output.append((hi << 4) + lo)

    return output


def _unpack12bits(input_data: bytes, odd=False) -> List[int]:
    nibbles = bytearray()
    for a in input_data:
        nibbles.append((a >> 0) & 0xf)
        nibbles.append((a >> 4) & 0xf)

    if odd:
        nibbles.pop(0)

    # Make sure the list is multiple of three nibbles
    while len(nibbles) % 3:
        nibbles.pop()

    output = []
    while nibbles:
        lo = nibbles.pop(0)
        mi = nibbles.pop(0)
        hi = nibbles.pop(0)
        output.append(lo + (mi << 4) + (hi << 8))

    return output


class AdcDac:
    """
    Class that controls a single adcdac board.
    """
    # Port modes
    PORT_MODE_OUTPUT = 0
    """ Port mode as DAC output """

    PORT_MODE_INPUT = 1
    """ Port mode as ADC input """

    PORT_MODE_NOT_CONNECTED = 2
    """ Port mode as high impedance """

    # Error bits and mask
    STATUS_BIT_DAC_OVER_CURRENT = 0x0020
    """ DAC port over current error """

    STATUS_BIT_TEMPERATURE_LOW = 0x0080
    """ Temperature low error """

    STATUS_BIT_TEMPERATURE_HIGH = 0x0100
    """ Temperature high error """

    STATUS_BIT_SUPPLY_VOLTAGE_LOW = 0x8000
    """ Supply voltage low error """

    STATUS_MASK_ERROR = (STATUS_BIT_DAC_OVER_CURRENT |
                         STATUS_BIT_TEMPERATURE_LOW |
                         STATUS_BIT_TEMPERATURE_HIGH |
                         STATUS_BIT_SUPPLY_VOLTAGE_LOW)
    """ Errors mask """

    # Commands
    _CMD_GET_VERSION = 100
    _CMD_GET_HEARTBEAT = 101
    _CMD_STATE = 102
    _CMD_OC_STATE = 103
    _CMD_CLEAR_WARNINGS = 104
    _CMD_ID_SELECTOR = 105
    _CMD_SUPPLIES = 106
    _CMD_TEMPERATURES = 107
    _CMD_RESET = 108
    _CMD_SET_PORT_MODE = 109
    _CMD_ADC_DATA = 110
    _CMD_DAC_DATA = 111
    _CMD_WRITE_REG = 112
    _CMD_READ_REG = 113

    # Analog voltage reference value
    _VOLTAGE_REF = 2.5

    def __init__(self, device_name: str = None):
        """ Create and initialize an instance of AdcDac.

        Note:
            The device will not be connected. Use 'connect' and 'disconnect' or use as a context manager.

        Example:
            >>> dev_name = find_adcdac_device_names()[0]
            >>> AdcDac(dev_name)
            AdcDac(device_name='COM4')

        """
        self._device_name = device_name
        self._firmware_ok = False
        self._serial = None
        self._values = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    def __repr__(self) -> str:
        """ Get a representation of the object """
        return f"AdcDac(device_name='{self._device_name}')"

    def __str__(self) -> str:
        """ Describe the object """
        return f"AdcDac, device_name='{self._device_name}', connected='{self.is_connected()}'"

    def __enter__(self):
        """ Context manager enter """
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ Context manager exit """
        self.disconnect()

    def is_connected(self) -> bool:
        """ Returns True if connected, False otherwise.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.is_connected()
            False
        """
        return (self._serial is not None) and self._serial.is_open

    def connect(self, device_name: str = None):
        """ Connect the named device

        Args:
            device_name: Device name to be used (if different from initialization)

        Examples:
            >>> dev_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(dev_name)
            >>> io.connect()
            >>> io.is_connected()
            True
        """
        if self.is_connected():
            self.disconnect()

        if device_name is not None:
            self._device_name = device_name

        self._serial = serial.Serial(port=self._device_name,
                                     baudrate=115200,
                                     bytesize=8,
                                     parity=serial.PARITY_NONE,
                                     stopbits=serial.STOPBITS_ONE,
                                     timeout=1)

        fw_version = self.get_fw_version()
        sw_version = __version__
        self._firmware_ok = (fw_version == sw_version)

        if not self._firmware_ok:
            warn(f"ADC/DAC board firmware version {fw_version:s} does not match software version {sw_version:s}.\n"
                 "Call AdcDac.update_firmware() to update all connected boards.")

    def disconnect(self):
        """ Disconnect the device

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.is_connected()
            True
            >>> io.disconnect()
            >>> io.is_connected()
            False
        """
        self._serial.close()

    @property
    def device_name(self) -> str:
        """ Get device name.

        Returns:
            Device name

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.device_name
            'COM4'
        """
        return self._device_name

    def reset(self):
        """ Reset the device.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.reset()
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_RESET,))
        self._serial.write(cmd)

        ans = self._serial.read(1)
        assert len(ans) == 1
        assert ans[0] == cmd[0]

    def get_id_selector(self) -> int:
        """ Get the id selector value.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.get_id_selector()
            1
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_ID_SELECTOR,))
        self._serial.write(cmd)

        ans = self._serial.read(2)
        assert len(ans) == 2
        assert ans[0] == cmd[0]
        return ans[1]

    def get_supplies(self) -> Dict[str, float]:
        """ Get the supplies values

        Returns:
            Dictionary with 8 elements, with entries V0,V1,V2,I0,I1,I2,10V,12V.
            Voltages in V, Currents in mA.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> sups = io.get_supplies()
            >>> sups['V0'] > 11.0
            True
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_SUPPLIES,))
        self._serial.write(cmd)

        ans = self._serial.read(17)
        assert len(ans) == 17
        assert ans[0] == cmd[0]

        supplies = dict()
        supplies['V0'] = struct.unpack('h', ans[1:3])[0]
        supplies['V1'] = struct.unpack('h', ans[3:5])[0]
        supplies['V2'] = struct.unpack('h', ans[5:7])[0]
        supplies['I0'] = struct.unpack('h', ans[7:9])[0]
        supplies['I1'] = struct.unpack('h', ans[9:11])[0]
        supplies['I2'] = struct.unpack('h', ans[11:13])[0]
        supplies['10V'] = struct.unpack('h', ans[13:15])[0]
        supplies['12V'] = struct.unpack('h', ans[15:17])[0]

        # scale
        r_ports = 1 / (1 / 20.5 + 1 / 20.5)
        scale = (2.05 + r_ports) / 2.05
        scale *= self._VOLTAGE_REF / 4095.0
        supplies['V0'] *= scale
        supplies['V1'] *= scale
        supplies['V2'] *= scale

        scale = 1000.0 / 4095.0
        supplies['I0'] *= scale
        supplies['I1'] *= scale
        supplies['I2'] *= scale

        r_10v = 1 / (1 / 30.1 + 1 / 30.1 + 1 / 30.1)
        scale = (2.05 + r_10v) / 2.05
        scale *= self._VOLTAGE_REF / 4095.0
        supplies['10V'] *= scale

        r_12v = 10.0
        scale = (2.05 + r_12v) / 2.05
        scale *= self._VOLTAGE_REF / 4095.0
        supplies['12V'] *= scale

        return supplies

    def get_fw_version(self) -> str:
        """ Get the firmware version

        Returns:
            Firmware verrsion as 4 dotted numbers, e.g. 1.2.3.4

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.get_fw_version()
            '2022.05.31.0'
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_GET_VERSION,))
        self._serial.write(cmd)

        ans = self._serial.read(2)
        assert len(ans) == 2
        assert ans[0] == cmd[0]

        length = ans[1]
        ans = self._serial.read(length)
        assert len(ans) == length
        return ans.decode()

    def get_heartbeat(self) -> int:
        """ Get the heartbeat value: a 32-bit wrapping counter that ticks at every Arduino cycle.

        Returns:
            Heartbeat value.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.get_heartbeat() >= 0
            True
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_GET_HEARTBEAT,))
        self._serial.write(cmd)

        ans = self._serial.read(5)
        assert len(ans) == 5
        assert ans[0] == cmd[0]

        value = struct.unpack('I', ans[1:5])[0]
        return value

    def get_temperatures(self) -> Tuple[float, float, float]:
        """ Get the internal temperatures for each ADC/DAC chip.

        Returns:
            Tuple with three temperatures in deg C.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> temperatures = io.get_temperatures()
            >>> 15 < temperatures[2] < 35.0
            True
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_TEMPERATURES,))
        self._serial.write(cmd)

        ans = self._serial.read(7)
        assert len(ans) == 7
        assert ans[0] == cmd[0]

        temp1 = struct.unpack('h', ans[1:3])[0] / 8
        temp2 = struct.unpack('h', ans[3:5])[0] / 8
        temp3 = struct.unpack('h', ans[5:7])[0] / 8
        return temp1, temp2, temp3

    def read_register(self, cs: int, reg: int) -> int:
        """ Read an internal register of an ADC/DAC chip

        Args:
            cs: Chip number 0,1,2
            reg: Register 0-127

        Returns:
            Register contents as 16-bit value.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> hex(io.read_register(2,0))  # read ID register
            '0x424'
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        assert (0 <= reg <= 127)

        cmd = bytes((self._CMD_READ_REG, cs, reg))
        self._serial.write(cmd)

        ans = self._serial.read(3)
        assert len(ans) == 3
        assert ans[0] == cmd[0]

        value = struct.unpack('h', ans[1:3])[0]
        return value

    def set_port_mode(self, cs: int, port: int, port_mode: int):
        """ Set the port mode of a port of an ADC/DAC chip

        Args:
            cs: Chip number 0,1,2
            port: Port number 0-19
            port_mode: Any of the PORT_MODE_* constants, e.g. 0,1,2 for output,input,nc

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.set_port_mode(0,0, io.PORT_MODE_NOT_CONNECTED)  # Stop conversion on chip 0, port 0
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        assert (0 <= port <= 19)
        assert (0 <= port_mode <= 2)

        port_mode = (port & 0x3f) + ((port_mode & 3) << 6)
        cmd = bytes((self._CMD_SET_PORT_MODE, cs, port_mode))
        self._serial.write(cmd)

        ans = self._serial.read(1)
        assert len(ans) == 1
        assert ans[0] == cmd[0]

    def write_register(self, cs: int, reg: int, value: int):
        """ Write an internal register of an ADC/DAC chip

        Args:
            cs: Chip number 0,1,2
            reg: Register 0-127
            value: New contents as 16-bit value.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.write_register(2,0x0d, 123)
            >>> io.read_register(2,0x0d)
            123
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        assert (0 <= reg <= 127)
        assert (0 <= value <= 0xffff)

        cmd = bytes((self._CMD_WRITE_REG, cs, reg, value & 0xff, (value >> 8) & 0xff))
        self._serial.write(cmd)

        ans = self._serial.read(1)
        assert len(ans) == 1
        assert ans[0] == cmd[0]

    def read_dac(self, cs: int, port: int = None) -> Union[List[float], float]:
        """ Read dac (output) value for a port on a chip

        Args:
            cs: Chip number 0,1,2
            port: Port number 0-19

        Returns:
            Single voltage (when port number provided by caller) or list of 20 voltages.
        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.read_dac(1)
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            >>> io.read_dac(1, 2)
            0.0
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        if port is None:
            values = [0.0] * 20
            for port in range(20):
                values[port] = self.read_register(cs, 0x60 + port) * 10.0 / 4095
            return values
        else:
            assert (0 <= port <= 20)
            dac_data = self.read_register(cs, 0x60 + port)
            return dac_data * 10.0 / 4095

    def write_dac(self, cs: int, set_points: Dict[int, float]):
        """ Write dac (output) value for a port on a chip

        Args:
            cs: Chip number 0,1,2
            set_points: Dictionary with set points per port

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.write_dac(0,{2: 10.0})
            >>> io.read_dac(0, 2)
            10.0
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        for port, value in set_points.items():
            assert (0 <= port <= 19)
            assert (0.0 <= value <= 10.0)

        # pack the set points
        mask = int(0)
        for port in set_points.keys():
            mask |= int(1 << port)

        nr_channels = bin(mask)[-20:].count('1')
        nr_bytes = floor(nr_channels * 1.5)

        cmd = bytearray(5 + nr_bytes)

        cmd[0] = self._CMD_DAC_DATA
        cmd[1] = cs
        cmd[2] = (mask >> 0) & 0xff
        cmd[3] = (mask >> 8) & 0xff
        cmd[4] = (mask >> 16) & 0x0f

        odd = True
        idx = 4
        for channel in range(20):
            if mask & (1 << channel):
                set_point = int(set_points[channel] * 4095 / 10.0 + 0.5)
                if odd:
                    cmd[idx] |= ((set_point & 0xf) << 4)
                    cmd[idx + 1] |= (set_point >> 4)
                    idx += 2
                    odd = False
                else:
                    cmd[idx] |= (set_point & 0xff)
                    cmd[idx + 1] |= (set_point >> 8)
                    idx += 1
                    odd = True

        self._serial.write(cmd)

        ans = self._serial.read(1)
        assert len(ans) == 1
        assert ans[0] == cmd[0]

    def read_adc(self, cs: int, port: int = None) -> Union[List[float], float]:
        """ Read adc (input) value for a port on a chip

        Args:
            cs: Chip number 0,1,2
            port: Port number 0-19

        Returns:
            Single voltage (when port number provided by caller) or list of 20 voltages.
        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> len(io.read_adc(1))
            20
            >>> 0.0 <= io.read_adc(1, 2) < 1.0
            True
        """
        assert self.is_connected(), "AdcDac is not connected"

        assert (0 <= cs <= 2)
        adc_data = self._read_all_adc(cs)
        if port is None:
            return adc_data
        else:
            assert (0 <= port <= 20)
            return adc_data[port]

    def _read_all_adc(self, cs: int) -> List[float]:
        """ Read all adc channels """
        assert (0 <= cs <= 2)
        cmd = bytes((self._CMD_ADC_DATA, cs))
        self._serial.write(cmd)

        ans = bytearray(self._serial.read(5))
        assert len(ans) == 5
        assert ans[0] == cmd[0]
        assert ans[1] == cmd[1]

        ans2 = copy(ans)
        ans2.append(0)
        mask = struct.unpack('I', ans2[2:6])[0]

        nr_channels = bin(mask)[-20:].count('1')
        nr_words = floor(nr_channels * 1.5)
        ans = ans + self._serial.read(nr_words)
        assert len(ans) == 5 + nr_words

        odd = True
        idx = 4
        for channel in range(20):
            if mask & (1 << channel):
                if odd:
                    nibble0 = (ans[idx] >> 4) & 0x0f
                    nibble1 = (ans[idx + 1] >> 0) & 0x0f
                    nibble2 = (ans[idx + 1] >> 4) & 0x0f
                    value = nibble0 + (nibble1 << 4) + (nibble2 << 8)
                    self._values[cs][channel] = value * 10.0 / 4095
                    odd = False
                    idx += 2
                else:
                    nibble0 = (ans[idx] >> 0) & 0x0f
                    nibble1 = (ans[idx] >> 4) & 0x0f
                    nibble2 = (ans[idx + 1] >> 0) & 0x0f
                    value = nibble0 + (nibble1 << 4) + (nibble2 << 8)
                    self._values[cs][channel] = value * 10.0 / 4095
                    odd = True
                    idx += 1
        return self._values[cs]

    def read_error_states(self) -> Tuple[int, int, int]:
        """ Read the states of all three ADC/DAC chips

        Returns:
            Tuple with 3 state words. States can be interpreted with the STATUS_* masks.

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.read_error_states()
            (0, 0, 0)
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_STATE,))
        self._serial.write(cmd)
        ans = bytearray(self._serial.read(7))
        assert len(ans) == 7
        assert ans[0] == cmd[0]

        states0 = struct.unpack('h', ans[1:3])[0] & self.STATUS_MASK_ERROR
        states1 = struct.unpack('h', ans[3:5])[0] & self.STATUS_MASK_ERROR
        states2 = struct.unpack('h', ans[5:7])[0] & self.STATUS_MASK_ERROR

        return states0, states1, states2

    def read_oc_states(self) -> Tuple[int, int, int]:
        """ Read the overcurrent status of all ports on all three ADC/DAC chip

        Returns:
            Tuple with 3 status words. Bit numbers are port numbers. LSB = 0

        Examples:
            >>> device_name = find_adcdac_device_names()[0]
            >>> io = AdcDac(device_name)
            >>> io.connect()
            >>> io.read_oc_states()
            (0, 0, 0)
        """
        assert self.is_connected(), "AdcDac is not connected"

        cmd = bytes((self._CMD_OC_STATE,))
        self._serial.write(cmd)
        ans = bytearray(self._serial.read(13))
        assert len(ans) == 13
        assert ans[0] == cmd[0]

        states0 = int(struct.unpack('I', ans[1:5])[0])
        states1 = int(struct.unpack('I', ans[5:9])[0])
        states2 = int(struct.unpack('I', ans[9:13])[0])

        return states0, states1, states2

    def update_firmware(self):
        """
        Update adcdac board firmware to current version.

        Returns:
            True if successful

        """
        assert not self.is_connected(), "AdcDac is connected. Disconnect first"

        if sys.platform == 'win32':
            bossac_exe = os.path.sep.join((HERE, "bossac.exe"))
        elif sys.platform == 'linux':
            bossac_exe = os.path.sep.join((HERE, "bossac"))
        else:
            bossac_exe = None

        assert os.path.isfile(bossac_exe), f"'{bossac_exe}' could not be found in adcdac directory."

        fw_image = os.path.sep.join((HERE, "arduinogpio.ino.bin"))
        assert os.path.isfile(fw_image), f"'{fw_image}' could not be found in adcdac directory."

        assert os.access(bossac_exe, os.R_OK | os.X_OK), f"File '{bossac_exe}' not executable."

        # Setting the serial port to 1200 baud signals to the device to do a 'clear firmware and reset'
        # After reset, the adcdac identifies as a new device, and it will most likely get a different port assigned.
        temp_serial = serial.Serial(port=self._device_name, baudrate=1200)
        assert temp_serial.is_open, f"Could not open serial port {self._device_name:s}"
        sleep(1)
        temp_serial.close()
        sleep(1)

        # Find new device name
        cmdline = [bossac_exe, '-i']
        output = subprocess.run(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_lines = output.stdout.decode('utf-8').splitlines()
        stderr_lines = output.stderr.decode('utf-8').splitlines()
        assert output.returncode == 0, f"Process failed '{' '.join(cmdline)}' with exit code {output.returncode}\n" \
                                       f"{' '.join(stdout_lines)}\n" \
                                       f"{' '.join(stderr_lines)}"

        if sys.platform == "win32":
            regex = r'(?:COM[\d]+)'
        elif sys.platform == "linux":
            regex = r'(?:ttyACM[\d]+)'
        else:
            regex = None

        result = re.search(regex, stdout_lines[0])
        assert result, "bossac did not find serial port. Please check connections, blocking processes and retry."
        new_device_name = result.group(0)

        # Flash new firmware
        cmdline = [bossac_exe, '--port=' + new_device_name, "--force_usb_port=true", "-e", "-w", "-v", fw_image, "-R"]
        output = subprocess.run(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_lines = output.stdout.decode('utf-8').splitlines()
        stderr_lines = output.stderr.decode('utf-8').splitlines()
        assert output.returncode == 0, f"Process failed '{' '.join(cmdline)}' with exit code {output.returncode}\n" \
                                       f"{' '.join(stdout_lines)}\n" \
                                       f"{' '.join(stderr_lines)}"

        return True

import sys

from adcdac import find_adcdac_device_names, AdcDac

if __name__ == '__main__':
    device_names = find_adcdac_device_names()
    if len(device_names) == 0:
        print("No AdcDac devices found")
        sys.exit(1)

    device_name = device_names[0]
    print(f"Using device {device_name}")

    with AdcDac(device_name) as io:
        if not io.is_connected():
            print("Could not connect")
            sys.exit(1)

        print(" Configuration ".center(60, '-'))

        io.reset()
        fw_version = io.get_fw_version()
        print(f"Device has version = {fw_version}")

        id_selector = io.get_id_selector()
        print(f"Device has id_selector = {id_selector}")

        print(" Diagnostics ".center(60, '-'))

        supplies = io.get_supplies()
        print(f"Device has 10V,12V  = {supplies['10V']:5.1f} {supplies['12V']:5.1f} [V]")
        print(f"Device has Voltages = {supplies['V0']:5.1f} {supplies['V1']:5.1f} {supplies['V2']:5.1f} [V]")
        print(f"Device has Currents = {supplies['I0']:5.1f} {supplies['I1']:5.1f} {supplies['I2']:5.1f} [mA]")

        temperatures = io.get_temperatures()
        print(f"Device has Temps    = {temperatures[0]:5.1f} {temperatures[1]:5.1f} {temperatures[2]:5.1f} [deg C]")

        io.set_port_mode(0, 0, io.PORT_MODE_INPUT)
        io.set_port_mode(0, 1, io.PORT_MODE_OUTPUT)
        io.set_port_mode(0, 2, io.PORT_MODE_OUTPUT)

        module0_setpoints = {1: 2.0,
                             2: 3.0}
        io.write_dac(0, module0_setpoints)

        voltage0 = io.read_adc(0, 0)
        voltage1 = io.read_adc(0, 1)
        voltage2 = io.read_adc(0, 2)

        target0 = io.read_dac(0, 0)
        target1 = io.read_dac(0, 1)
        target2 = io.read_dac(0, 2)

        print(" Measurements ".center(60, '-'))
        print(f"Device 0, port 0 has  = {voltage0:.3f} [V]")
        print(f"Device 0, port 1 has  = {voltage1:.3f} [V]")
        print(f"Device 0, port 2 has  = {voltage2:.3f} [V]")
        print(f"Device 0, port 0 sets = {target0:.3f} [V]")
        print(f"Device 0, port 1 sets = {target1:.3f} [V]")
        print(f"Device 0, port 2 sets = {target2:.3f} [V]")

        print(" Error states ".center(60, '-'))
        state = io.read_error_states()
        print(state)

        print(" Over current port mask ".center(60, '-'))
        states = io.read_oc_states()
        print(states)

    sys.exit(0)

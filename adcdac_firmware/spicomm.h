#ifndef SPICOMM_H
#define SPICOMM_H

#include <stdint.h>

void spi_setup();
void spi_write(uint8_t cs, uint8_t reg, const uint16_t *values, uint8_t count);
void spi_read(uint8_t cs, uint8_t reg, uint16_t *values, uint8_t count);

#endif // SPICOMM_H

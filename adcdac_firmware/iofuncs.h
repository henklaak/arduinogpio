#ifndef SUPPLY_H
#define SUPPLY_H
#include <stdint.h>
#include "firmware_globals.h"

void io_setup();
void io_sync();
void io_diag_led(bool state);
void io_diag_blink();

extern uint16_t supplies[NR_SUPPLIES];
extern uint8_t id_selector;

#endif // SUPPLY_H

#ifndef LOCALS_H
#define LOCALS_H

// IO pin definitions

// Digital
#define OUT_CS_0      (9)
#define OUT_CS_1      (6)
#define OUT_CS_2      (4)
#define OUT_DIAG_LED  (5)
#define IN_HWID_0     (3)
#define IN_HWID_1     (2)
#define IN_HWID_2     (8)
#define IN_HWID_3     (7)

// Analog
#define IN_SUPPLY_0  (A1)
#define IN_SUPPLY_1  (A7)
#define IN_SUPPLY_2  (A4)
#define IN_CURRENT_0 (A3)
#define IN_CURRENT_1 (A6)
#define IN_CURRENT_2 (A5)
#define IN_10V       (A0)
#define IN_12V       (A2)

// MAX registers
#define MAX_REG_INTR             (0x01)
#define MAX_REG_ADC_STATUS       (0x02)
#define MAX_REG_OC_STATUS        (0x04)
#define MAX_REG_TEMPERATURE      (0x08)
#define MAX_REG_DEV_CTRL         (0x10)
#define MAX_REG_INTR_MASK        (0x11)
#define MAX_REG_TEMP_CONFIG      (0x18)
#define MAX_REG_TEMP_THRESH_HILO (0x19)
#define MAX_REG_PORT_CONFIG      (0x20)
#define MAX_REG_ADC_DATA         (0x40)
#define MAX_REG_DAC_DATA         (0x60)


#endif

#ifndef SERCOMM_H
#define SERCOMM_H

void ser_setup();
void ser_sync();

extern uint32_t cycle_counter;

#endif

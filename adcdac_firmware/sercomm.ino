#include "sercomm.h"

uint32_t cycle_counter = 0;
uint32_t no_comm_time = 0;

/********************************************************************/
void ser_setup()
{
  Serial.begin(BAUDRATE);
  io_diag_led(true);
  while (!Serial) ;
  io_diag_led(false);
}

/********************************************************************/
void ser_sync()
{
  // Commands
  // --------
  //   Reset
  //   Read/write single register
  //   Read/Write port configs
  //   Read ADCs
  //   Read/write DACs
  //   Read temperatures
  //
  //   Read supplies
  //   Read id_selector
  //   Read SW version

  static uint8_t idx = 0;
  static uint8_t idx_dac = 5;
  static uint8_t buffer[MAXMSGLENGTH];
  static uint8_t cmd;

  if (Serial.available())
  {
    no_comm_time = 0;
    io_diag_led(false);

    buffer[idx] = Serial.read();

    if (idx == 0)
    {
      cmd = buffer[idx];
    }

    switch (cmd)
    {
      case CMD_GET_VERSION: // 100
        buffer[1] = strlen(FWVERSION_STR);
        for (int i=0; i<buffer[1]; ++i)
        {
          buffer[2+i] = FWVERSION_STR[i];
        }
        Serial.write(buffer, 2+buffer[1]);
        break;

      case CMD_GET_HEARTBEAT: // 101
        buffer[1] =  (cycle_counter >>  0) & 0xff;
        buffer[2] =  (cycle_counter >>  8) & 0xff;
        buffer[3] =  (cycle_counter >> 16) & 0xff;
        buffer[4] =  (cycle_counter >> 24) & 0xff;
        Serial.write(buffer, 5);
        break;

      case CMD_STATE: // 102
        for (int i = 0; i < NR_MAX_MODULES; ++i)
        {
          buffer[1 + 2 * i + 0] = (max_state[i] >> 0) & 0xff;
          buffer[1 + 2 * i + 1] = (max_state[i] >> 8) & 0xff;
        }
        Serial.write(buffer, 1 + 2 * NR_MAX_MODULES);
        idx = 0;
        break;

      case CMD_OC_STATE: // 103
        for (int i = 0; i < NR_MAX_MODULES; ++i)
        {
          buffer[1 + 4 * i + 0] = (oc_state[i] >>  0) & 0xff;
          buffer[1 + 4 * i + 1] = (oc_state[i] >>  8) & 0xff;
          buffer[1 + 4 * i + 2] = (oc_state[i] >> 16) & 0xff;
          buffer[1 + 4 * i + 3] = (oc_state[i] >> 24) & 0xff;
        }
        Serial.write(buffer, 1 + 4 * NR_MAX_MODULES);
        idx = 0;
        break;

      case CMD_CLEAR_WARNINGS: // 104
        max_state[0] = 0;
        max_state[1] = 0;
        max_state[2] = 0;
        oc_state[0] = 0;
        oc_state[1] = 0;
        oc_state[2] = 0;

        Serial.write(buffer, 1);
        idx = 0;
        break;

      case CMD_ID_SELECTOR: // 105
        buffer[1] = id_selector;
        Serial.write(buffer, 2);
        idx = 0;
        break;

      case CMD_SUPPLIES: // 106
        for (int i = 0; i < NR_SUPPLIES; ++i)
        {
          buffer[1 + 2 * i + 0] = (supplies[i] >> 0) & 0xff;
          buffer[1 + 2 * i + 1] = (supplies[i] >> 8) & 0xff;
        }
        Serial.write(buffer, 1 + 2 * NR_SUPPLIES);
        idx = 0;
        break;

      case CMD_TEMPERATURES: // 107
        for (int i = 0; i < NR_MAX_MODULES; ++i)
        {
          buffer[1 + 2 * i + 0] = (temperature[i] >> 0) & 0xff;
          buffer[1 + 2 * i + 1] = (temperature[i] >> 8) & 0xff;
        }
        Serial.write(buffer, 1 + 2 * NR_MAX_MODULES);
        idx = 0;
        break;

      case CMD_RESET: // 108
        max_setup();
        Serial.write(buffer, 1);
        idx = 0;
        cycle_counter = 0;
        break;

      case CMD_SET_PORTMODE: // 109
        if (idx == 2)
        {
          uint8_t cs = buffer[1];
          uint8_t port = (buffer[2] >> 0) & 0x3f;
          uint8_t mode = (buffer[2] >> 6) & 0x03;
          uint16_t value = 0;

          switch (mode)
          {
            case 0:
              value = 0x6100;
              break;
            case 1:
              value = 0x7180;
              break;
            default:
              value = 0x0000;
          }
          spi_write(cs, MAX_REG_PORT_CONFIG + port, &value, 1);

          Serial.write(buffer, 1);
          idx = 0;
        }
        else
        {
          idx++;
        }
        break;

      case CMD_ADC_DATA: // 110
        if (idx == 1)
        {
          uint8_t cs = buffer[1];
          buffer[2] = (adc_data_dirty[cs] >> 0) & 0xff;
          buffer[3] = (adc_data_dirty[cs] >> 8) & 0xff;
          buffer[4] = (adc_data_dirty[cs] >> 16) & 0x0f;

          uint8_t idx2 = 4;
          bool odd = true;

          for (int i = 0; i < NR_PORTS; ++i)
          {
            if ((adc_data_dirty[cs] >> i) & 1)
            {
              if (odd)
              {
                buffer[idx2 + 0] |= ((adc_data[cs][i] & 0xf) << 4);
                buffer[idx2 + 1] = (adc_data[cs][i] >> 4) & 0xff;
                idx2 += 2;
                odd = false;
              }
              else
              {
                buffer[idx2 + 0] = (adc_data[cs][i] & 0xff);
                buffer[idx2 + 1] = (adc_data[cs][i] >> 8) & 0x0f;
                idx2 += 1;
                odd = true;
              }
            }
          }
          Serial.write(buffer, idx2 + (odd ? 1 : 0));
          adc_data_dirty[cs] = 0;
          idx = 0;
        }
        else
        {
          idx++;
        }
        break;

      case CMD_DAC_DATA: // 111
        if (idx == 4)
        {
          uint32_t mask = 0x00000000;
          mask |= ((uint32_t)(buffer[2]) <<  0);
          mask |= ((uint32_t)(buffer[3]) <<  8);
          mask |= ((uint32_t)(buffer[4]) <<  16);

          uint8_t nr_channels = 0;
          for (int i=0; i<NR_PORTS; ++i)
          {
            if ((mask >> i) & 1)
            {
              nr_channels++;
            }
          }

          idx_dac = 4 + floor(nr_channels * 1.5);
        }
        if (idx == idx_dac)
        {
          uint8_t cs = buffer[1];

          dac_data_dirty[cs] = 0;
          dac_data_dirty[cs] |= ((uint32_t)(buffer[2]) <<  0);
          dac_data_dirty[cs] |= ((uint32_t)(buffer[3]) <<  8);
          dac_data_dirty[cs] |= ((uint32_t)(buffer[4]) <<  16);

          uint8_t idx2 = 4;
          bool odd = true;
          for (int i = 0; i < NR_PORTS; ++i)
          {
            if ((dac_data_dirty[cs] >> i) & 1)
            {
              if (odd)
              {
                dac_data[cs][i] = ((buffer[idx2] & 0xf0) >> 4) + ((uint32_t)(buffer[idx2+1]) << 4);
                idx2 += 2;
                odd = false;
              }
              else
              {
                dac_data[cs][i] = buffer[idx2] + ((uint32_t)(buffer[idx2+1] & 0x0f) << 8);
                idx2 += 1;
                odd = true;
              }
            }
          }
          Serial.write(buffer, 1);
          idx = 0;
        }
        else
        {
          idx++;
        }
        break;

      case CMD_WRITE_REG: // 112
        if (idx == 4)
        {
          uint8_t cs = buffer[1];
          uint8_t reg = buffer[2];
          uint16_t lsb = buffer[3];
          uint16_t msb = buffer[4];
          uint16_t value = (msb << 8) | lsb;

          spi_write(cs, reg, &value, 1);
          Serial.write(buffer, 1);
          idx = 0;
        }
        else
        {
          idx++;
        }
        break;

      case CMD_READ_REG: // 113
        if (idx == 2)
        {
          uint8_t cs = buffer[1];
          uint8_t reg = buffer[2];
          uint16_t value;
          spi_read(cs, reg, &value, 1);
          buffer[1] = (value >> 0) & 0xff;
          buffer[2] = (value >> 8) & 0xff;
          Serial.write(buffer, 3);
          idx = 0;
        }
        else
        {
          idx++;
        }
        break;

      default: // invalid, ignore
        idx = 0;
        break;
    }
  }
  else
  {
      no_comm_time++;
      if (no_comm_time == 1000)
      {
        io_diag_led(true);
      }
  }
}

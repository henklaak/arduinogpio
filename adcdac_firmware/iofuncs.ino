#include "locals.h"
#include "iofuncs.h"
#include "maxfuncs.h"
#include "sercomm.h"

const int POWER_INDICES[NR_SUPPLIES] = {
  IN_SUPPLY_0,
  IN_SUPPLY_1,
  IN_SUPPLY_2,
  IN_CURRENT_0,
  IN_CURRENT_1,
  IN_CURRENT_2,
  IN_10V,
  IN_12V
};

uint16_t supplies[NR_SUPPLIES] = {};
uint8_t id_selector = 0;

/********************************************************************/
void io_setup()
{
  // use external reference voltage
  analogReference(AR_EXTERNAL);
  analogReadResolution(12);

  pinMode(OUT_DIAG_LED, OUTPUT);

  pinMode(IN_SUPPLY_0, INPUT);
  pinMode(IN_SUPPLY_1, INPUT);
  pinMode(IN_SUPPLY_2, INPUT);

  pinMode(IN_CURRENT_0, INPUT);
  pinMode(IN_CURRENT_1, INPUT);
  pinMode(IN_CURRENT_2, INPUT);

  pinMode(IN_10V, INPUT);
  pinMode(IN_12V, INPUT);

  pinMode(IN_HWID_0, INPUT);
  pinMode(IN_HWID_1, INPUT);
  pinMode(IN_HWID_2, INPUT);
  pinMode(IN_HWID_3, INPUT);
}

/********************************************************************/
void io_diag_led(bool state)
{
  digitalWrite(OUT_DIAG_LED, state ? HIGH : LOW);
}

/********************************************************************/
void io_diag_blink()
{
  io_diag_led(true);
  delay(50);
  io_diag_led(false);
}


/********************************************************************/
void io_sync()
{
  static uint8_t idx = 0;

  if (idx < NR_SUPPLIES)
  {
    supplies[idx] = analogRead(POWER_INDICES[idx]);
  }
  else
  {
    // periodically update id selector
    id_selector  = (digitalRead(IN_HWID_0) << 0) +
                   (digitalRead(IN_HWID_1) << 1) +
                   (digitalRead(IN_HWID_2) << 2) +
                   (digitalRead(IN_HWID_3) << 3);
  }

  idx = (idx + 1) % (NR_SUPPLIES + 1);
}

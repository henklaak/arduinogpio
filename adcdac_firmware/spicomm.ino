#include <SPI.h>
#include "spicomm.h"
#include "locals.h"

static SPISettings settingsA(15000000, MSBFIRST, SPI_MODE0);

/******************************************************************/
void spi_setup()
{
  // Set CS pins to output
  pinMode(OUT_CS_0, OUTPUT);
  pinMode(OUT_CS_1, OUTPUT);
  pinMode(OUT_CS_2, OUTPUT);

  // Ensure CSs pins are initially high
  digitalWrite(OUT_CS_0, HIGH);
  digitalWrite(OUT_CS_1, HIGH);
  digitalWrite(OUT_CS_2, HIGH);

  SPI.begin();
}

/******************************************************************/
static void chip_select(uint8_t cs, PinStatus level)
{
  digitalWrite(OUT_CS_0, (cs == 0) ? level : HIGH);
  digitalWrite(OUT_CS_1, (cs == 1) ? level : HIGH);
  digitalWrite(OUT_CS_2, (cs == 2) ? level : HIGH);
}

/******************************************************************/
void spi_write(uint8_t cs, uint8_t reg, const uint16_t *values, uint8_t count)
{
  SPI.beginTransaction(settingsA);
  chip_select(cs, LOW);
  
  SPI.transfer((reg << 1) + 0);
  for (int i = 0; i < count; ++i)
  {
    SPI.transfer((values[i] >> 8) & 0xff);
    SPI.transfer((values[i] >> 0) & 0xff);
  }

  chip_select(cs, HIGH);
  SPI.endTransaction();
}

/******************************************************************/
void spi_read(uint8_t cs, uint8_t reg, uint16_t *values, uint8_t count)
{
  SPI.beginTransaction(settingsA);
  chip_select(cs, LOW);

  SPI.transfer((reg << 1) + 1);
  for (int i = 0; i < count; ++i)
  {
    uint16_t msb = SPI.transfer(0x00);
    uint16_t lsb = SPI.transfer(0x00);
    values[i] = (msb << 8) | lsb;
  }

  chip_select(cs, HIGH);
  SPI.endTransaction();
}

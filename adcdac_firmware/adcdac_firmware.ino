#include "iofuncs.h"
#include "maxfuncs.h"
#include "sercomm.h"

/*******************************************************************************/
void setup()
{
  io_setup();
  max_setup();
  ser_setup();
}

/*******************************************************************************/
void loop()
{
  cycle_counter++;
  
  io_sync();
  max_sync();
  ser_sync();
}

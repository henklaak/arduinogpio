#ifndef MAX_H
#define MAX_H
#include <stdint.h>

void max_setup();
void max_sync();

extern uint16_t adc_data[NR_MAX_MODULES][NR_PORTS];
extern uint32_t adc_data_dirty[NR_MAX_MODULES];

extern uint16_t dac_data[NR_MAX_MODULES][NR_PORTS];
extern uint32_t dac_data_dirty[NR_MAX_MODULES];

extern uint32_t oc_state[NR_MAX_MODULES];
extern uint16_t temperature[NR_MAX_MODULES];
extern uint16_t max_state[NR_MAX_MODULES];

#endif

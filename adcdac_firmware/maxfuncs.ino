#include "firmware_globals.h"
#include "spicomm.h"
#include "sercomm.h"

uint16_t adc_data[NR_MAX_MODULES][NR_PORTS] = {};
uint32_t adc_data_dirty[NR_MAX_MODULES] = {};

uint16_t dac_data[NR_MAX_MODULES][NR_PORTS] = {};
uint32_t dac_data_dirty[NR_MAX_MODULES] = {};

uint32_t oc_state[NR_MAX_MODULES] = {};
uint16_t temperature[NR_MAX_MODULES] = {};
uint16_t max_state[NR_MAX_MODULES] = {};

/*************************************************************************/
void max_setup()
{
  spi_setup();

  uint16_t values[NR_PORTS] = {};

  for (uint8_t cs = 0; cs < NR_MAX_MODULES; ++cs)
  {
    // Reset max chip
    values[0] = 0x8000; //reset bit
    spi_write(cs, MAX_REG_DEV_CTRL, values, 1);

  }

  for (uint8_t cs = 0; cs < NR_MAX_MODULES; ++cs)
  {
    // Wait until reset is done. When powered off, this will also fall through
    uint16_t cleared = 0xffff;
    while (cleared & 0x8000 )
    {
      spi_read(cs, MAX_REG_DEV_CTRL, &cleared, 1);
    }
  }

  delay(100);

  uint8_t csorder[3] = {2, 1, 0};
  for (uint8_t csi = 0; csi < NR_MAX_MODULES; ++csi)
  {
    uint8_t cs = csorder[csi];

    // Set Interrupt Mask (for test LED only)
    values[0] = 0x7e5f; // LED on for VMON, TMPINT HI/LO, DACOI
    spi_write(cs, MAX_REG_INTR_MASK, values, 1);

    // Config tmpint monitoring, 30C is about nominal
    values[0] = 0x0003;
    spi_write( cs, MAX_REG_TEMP_CONFIG, values, 1);

    values[0] = 8 * 70;
    values[1] = 8 * 10;
    spi_write( cs, MAX_REG_TEMP_THRESH_HILO, values, 2);

    //set device config
    values[0] = 0x01c3;
    spi_write( cs, MAX_REG_DEV_CTRL, values, 1 );

    // Config all ports as NC
    for (uint8_t p = 0; p < NR_PORTS; ++p)
    {
      values[p] = 0x0000;
      adc_data_dirty[cs] = 0xfffff;
    }
    spi_write( cs, MAX_REG_PORT_CONFIG, values, NR_PORTS );

    max_state[cs] = 0;
    oc_state[cs] = 0;

    io_diag_blink();
  }
}

/*************************************************************************/
void max_sync()
{
  uint16_t values[NR_PORTS] = {};

  // Handle all MAX modules interrupts
  for (int cs = 0; cs < NR_MAX_MODULES; ++cs)
  {

    // write 20 dacs if dirty and mark as clean
    for (uint8_t i = 0; i < NR_PORTS; ++i)
    {
      uint32_t mask = uint32_t(1) << i;
      bool dirty = dac_data_dirty[cs] & mask;
      if (dirty)
      {
        spi_write(cs, MAX_REG_DAC_DATA +i, &(dac_data[cs][i]),1);

        // clear bit
        dac_data_dirty[cs] &= ~mask; 
      }
    }

    // Handle all MAX modules interrupts
    uint16_t intrs = 0;
    spi_read(cs, MAX_REG_INTR, &intrs, 1);

    if (intrs & INTR_ADCDR)
    {
      {
        uint16_t adc_status[2] = {};
        spi_read(cs, MAX_REG_ADC_STATUS, adc_status, 2);

        // read 20 adcs if changed and mark as dirty
        for (int i = 0; i < 16; ++i)
        {
          if ((adc_status[0] >> i) & 1)
          {
            uint16_t sample;
            spi_read(cs, MAX_REG_ADC_DATA + i, &sample, 1);
            if (adc_data[cs][i] != sample)
            {
              adc_data[cs][i] = sample;
              adc_data_dirty[cs] |= (1 << i);
            }
          }
        }
        for (int i = 0; i < 4; ++i)
        {
          if ((adc_status[1] >> i) & 1)
          {
            uint16_t sample;
            spi_read(cs, MAX_REG_ADC_DATA + 16 + i, &sample, 1);
            if (adc_data[cs][16 + i] != sample)
            {
              adc_data[cs][16 + i] = sample;
              adc_data_dirty[cs] |= (1 << (16 + i));
            }
          }
        }
      }
    }

    if (intrs & INTR_DACOI)
    {
      uint32_t newbits;
      spi_read(cs, MAX_REG_OC_STATUS, (uint16_t *)(&newbits), 2);
      oc_state[cs] |= newbits;
    }

    if (intrs & INTR_TMPINT)
    {
      spi_read(cs, MAX_REG_TEMPERATURE, temperature + cs, 1);
    }

    if (intrs & INTR_TMPINTHI)
    {
      // OVERTEMPERATURE! Set all port configs to NC
      for (uint8_t p = 0; p < NR_PORTS; p++)
      {
        values[0] = 0x0000;
      }
      spi_write( cs, MAX_REG_PORT_CONFIG, values, NR_PORTS );
    }

    // status
    {
      max_state[cs] |= (intrs & (INTR_VMON | INTR_TMPINTHI | INTR_TMPINTLO | INTR_DACOI));
    }
  }
}

#ifndef GLOBALS_H
#define GLOBALS_H

#include "version.h"

#define BAUDRATE      (115200)
#define MAXMSGLENGTH      (64)

#define NR_SUPPLIES        (8)

#define NR_MAX_MODULES     (3)
#define NR_PORTS          (20)

#define CMD_GET_VERSION    (100)
#define CMD_GET_HEARTBEAT  (101)
#define CMD_STATE          (102)
#define CMD_OC_STATE       (103)
#define CMD_CLEAR_WARNINGS (104)
#define CMD_ID_SELECTOR    (105)
#define CMD_SUPPLIES       (106)
#define CMD_TEMPERATURES   (107)
#define CMD_RESET          (108)
#define CMD_SET_PORTMODE   (109)
#define CMD_ADC_DATA       (110)
#define CMD_DAC_DATA       (111)
#define CMD_WRITE_REG      (112)
#define CMD_READ_REG       (113)

// MAX status bits
#define INTR_ADCDR             (0x0002)
#define INTR_DACOI             (0x0020)
#define INTR_TMPINT            (0x0040)
#define INTR_TMPINTLO          (0x0080)
#define INTR_TMPINTHI          (0x0100)
#define INTR_VMON              (0x8000)

#define PORTMODE_AO (0)
#define PORTMODE_AI (1)
#define PORTMODE_NC (2)

#endif

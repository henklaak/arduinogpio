#include "temperaturewidget.h"
#include <QPainter>
#include <QStaticText>

TemperatureWidget::TemperatureWidget( QWidget *parent )
    : QWidget{parent}
    , m_head( 0 )
    , m_tail( 0 )
{
    setAttribute( Qt::WA_TranslucentBackground );

    for( int n = 0; n < 370; ++n )
    {
        for( int i = 0; i < NR_MAX_MODULES; ++i )
        {
            m_samples[n][i] = 0.0;
        }
    }

    m_red = QPen( QColor::fromRgb( 192, 64, 64 ) );
    m_red.setCosmetic( true );
    m_red.setWidth( 1 );

    m_green = QPen( QColor::fromRgb( 32, 240, 0 ) );
    m_green.setCosmetic( true );
    m_green.setWidth( 1 );

    m_blue = QPen( QColor::fromRgb( 128, 128, 128 ) );
    m_blue.setCosmetic( true );
    m_blue.setWidth( 1 );

    m_labelfont = QFont( "Arial" );
    m_labelfont.setPixelSize( 10 );
}

void TemperatureWidget::addTempSamples( float tmp0,
                                        float tmp1,
                                        float tmp2 )
{
    m_samples[m_head][0] = tmp0;
    m_samples[m_head][1] = tmp1;
    m_samples[m_head][2] = tmp2;
    m_head = ( m_head + 1 ) % 370;
    repaint();
}

void TemperatureWidget::paintEvent( QPaintEvent *event )
{
    QPainter painter( this );

    painter.setRenderHint( QPainter::TextAntialiasing );
    painter.setRenderHint( QPainter::Antialiasing );
#define A(i,n) (height() - height()/60.0 *(m_samples[i][n] - 15))
#define B(i,k) (height() - height()/60.0 *(k - 15))


    painter.setPen( m_red );
    painter.drawLine( 14, B( 14, 20 ), 384, B( 384, 20 ) );
    painter.setPen( m_blue );
    painter.drawLine( 14, B( 14, 30 ), 384, B( 384, 30 ) );
    painter.drawLine( 14, B( 14, 40 ), 384, B( 384, 40 ) );
    painter.drawLine( 14, B( 14, 50 ), 384, B( 384, 50 ) );
    painter.drawLine( 14, B( 14, 60 ), 384, B( 384, 60 ) );
    painter.setPen( m_red );
    painter.drawLine( 14, B( 14, 70 ), 384, B( 38, 70 ) );

    painter.setPen( m_blue );

    int k_head = 14 + m_head;
    painter.drawLine( k_head, 0, k_head, 100 );
    painter.setPen( m_green );

    for( int i = 1; i < 370; ++i )
    {
        if( i == m_head )
        {
            continue;
        }

        int k = 14 + i;
        painter.drawLine( k - 1, A( i - 1, 0 ), k, A( i, 0 ) );
        painter.drawLine( k - 1, A( i - 1, 1 ), k, A( i, 1 ) );
        painter.drawLine( k - 1, A( i - 1, 2 ), k, A( i, 2 ) );
    }

    painter.setPen( m_blue );
    painter.setFont( m_labelfont );

    for( int i = 20; i <= 80; i += 10 )
    {
        QStaticText lbl( QString( "%1" ).arg( i ) );
        painter.drawStaticText( QPoint( 0, B( 0, i ) - 5 ), lbl );
        painter.drawStaticText( QPoint( 385, B( 385, i ) - 5 ), lbl );
    }
}

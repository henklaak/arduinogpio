#pragma once

#include <QGraphicsView>
#include <QObject>

class BoardLayout : public QGraphicsView
{
    Q_OBJECT
public:
    BoardLayout( QWidget *parent = nullptr );

    void setHighlight( int cs,
                       int pin,
                       bool state );
    void clearHighlights();

protected:
    void resizeEvent( QResizeEvent * ) override;

private:
    void setupScene();
    QGraphicsItem *addBoard( QGraphicsItem *parent = nullptr );
    QGraphicsItem *addMax( QGraphicsItem *parent,
                           QPointF origin,
                           int max_nr,
                           bool flipped );
    QGraphicsItem *addMolex( QGraphicsItem *parent,
                             QPointF origin,
                             int max_nr,
                             int molex_nr,
                             bool flipped );
    QGraphicsItem *addPin( QGraphicsItem *parent,
                           QPointF origin,
                           int max_nr,
                           int pin_nr,
                           bool flipped );

    QGraphicsScene *m_scene = nullptr;
    bool m_highlights[3][20] {};
};



#include "maxwidget.h"
#include <iostream>
#include <filesystem>
#include <QThreadPool>
#include <QFuture>
#include <QApplication>
#include <QDir>
#include <QSpacerItem>
#include <filesystem>
#include "ui_maxwidget.h"
#include "adcdac.h"
#include "portwidget.h"
#include "fwupdater.h"

/**************************************************************************************************/
MaxWidget::MaxWidget( const QString &deviceName, QWidget *parent )
    : m_deviceName( deviceName )
    , QWidget( parent )
    , ui( new Ui::MaxWidget )
{
    ui->setupUi( this );

    m_adcdac.reset( new AdcDac( m_deviceName.toStdString() ) );
    m_adcdac->fwReset();

    QGridLayout *layout = new QGridLayout( ui->widgetMaxs );

    for( size_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        for( size_t port = 0; port < NR_PORTS; ++port )
        {
            int col = port + int( port / 4 );
            if( port > 0 && ( 0 == ( port % 4 ) ) )
            {
                layout->addItem( new QSpacerItem( 16, 16 ), cs, col - 1 );
            }
            PortWidget *portctrl = new PortWidget( ui->widgetMaxs, cs, port );
            layout->addWidget( portctrl, cs, col );

            QObject::connect( portctrl, &PortWidget::portConfigChanged,
                              this, &MaxWidget::onPortConfigChanged );

            m_portctrls[cs][port] = portctrl;
        }
    }

    QObject::connect( ui->pbFwUpdate, &QPushButton::clicked,
                      this, &MaxWidget::onStartFwUpdate );

    initPorts();
}

/**************************************************************************************************/
MaxWidget::~MaxWidget()
{
    delete ui;
}

/**************************************************************************************************/
void MaxWidget::initPorts()
{
    for( size_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        for( size_t port = 0; port < NR_PORTS; ++port )
        {
            m_portctrls[cs][port]->init();
        }
    }
}

/**************************************************************************************************/
QString MaxWidget::getDeviceName() const
{
    return m_deviceName;
}

/**************************************************************************************************/
void MaxWidget::onPortConfigChanged( uint8_t cs,
                                     uint8_t port,
                                     uint8_t portConfig )
{
    m_adcdac->setPortConfig( cs, port, portConfig );
}

/**************************************************************************************************/
void MaxWidget::updateGenerators()
{
    for( size_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        for( size_t port = 0; port < NR_PORTS; ++port )
        {
            PortWidget *portCtrl = m_portctrls[cs][port];

            if (portCtrl->isOutput())
            {
                uint16_t target = portCtrl->getTarget();
                m_adcdac->setAnalogOutputValue( cs, port, target );
            }
        }
    }
}

/**************************************************************************************************/
void MaxWidget::updateGui()
{
    if( !m_adcdac )
    {
        return;
    }

    ui->pbFwUpdate->setVisible( !( m_adcdac->isFwVersionOk() ) );

    bool conn = m_adcdac->isConnected();
    ui->lblDeviceName->setText( conn ? "Online" : "Offline" );
    ui->lblDeviceName->setStyleSheet( conn ? "color: #0c0;" : "color: #f00;" );

    ui->lblFwVersion->setText( QString::fromStdString( m_adcdac->getFwVersion() ) );
    ui->lblFwVersion->setStyleSheet( m_adcdac->isFwVersionOk() ? "color: #0c0;" : "color: #f00;" );

    int8_t idSelector = m_adcdac->getIdSelector();
    uint32_t heartbeat = m_adcdac->getHeartbeat();
    ui->lblIdSelector->setText( ( idSelector >= 0 ? QString::number( idSelector ) : "Unknown" ) + " / #"
                                + QString::number( heartbeat ) );
    ui->lblIdSelector->setStyleSheet( idSelector >= 0 ? "color: #0c0;" : "color: #f00;" );

    double value;
    bool valid;

    // Timing
    value = m_adcdac->getPeriod() * 1000;
    valid = ( value > 0 );
    ui->lblPeriod->setText( QString::number( value, 'f', 1 ) + " ms" );
    ui->lblPeriod->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    // Identifcation
    emit idSelectorChanged( m_adcdac->getIdSelector() );

    // Supplies
    value = m_adcdac->getSupplyVoltage( 0 );
    valid = value > 11 && value < 13;
    ui->lblV0->setText( QString::number( value, 'f', 2 ) + " V" );
    ui->lblV0->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->getSupplyVoltage( 1 );
    valid = value > 11 && value < 13;
    ui->lblV1->setText( QString::number( value, 'f', 2 ) + " V" );
    ui->lblV1->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->getSupplyVoltage( 2 );
    valid = value > 11 && value < 13;
    ui->lblV2->setText( QString::number( value, 'f', 2 ) + " V" );
    ui->lblV2->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->getSupplyCurrent( 0 );
    valid = value > 0;
    ui->lblI0->setText( QString::number( value, 'f', 1 ) + " mA" );
    ui->lblI0->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->getSupplyCurrent( 1 );
    valid = value > 0;
    ui->lblI1->setText( QString::number( value, 'f', 1 ) + " mA" );
    ui->lblI1->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->getSupplyCurrent( 2 );
    valid = value > 0;
    ui->lblI2->setText( QString::number( value, 'f', 1 ) + " mA" );
    ui->lblI2->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->get10VReferenceVoltage();
    valid = value > 9 && value < 11;
    ui->lbl10V->setText( QString::number( value, 'f', 2 ) + " V" );
    ui->lbl10V->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    value = m_adcdac->get12VSupplyVoltage();
    valid = value > 11 && value < 13;
    ui->lbl12V->setText( QString::number( value, 'f', 2 ) + " V" );
    ui->lbl12V->setStyleSheet( valid ? "color: #0c0;" : "color: #f00;" );

    // Temperatures
    float tmp0 = m_adcdac->getInternalTemperature( 0 );
    ui->lblTempCs0->setText( QString::number( tmp0, 'f', 1 ) + " C" );
    ui->lblTempCs0->setStyleSheet( ( !tmp0
                                     || m_adcdac->getOverTemperatureState( 0 ) ) ? "color: #f00;" :
                                   ( m_adcdac->getUnderTemperatureState( 0 ) ? "color: #08f;" :
                                     "color: #0c0;" ) );

    float tmp1 = m_adcdac->getInternalTemperature( 1 );
    ui->lblTempCs1->setText( QString::number( tmp1, 'f', 1 ) + " C" );
    ui->lblTempCs1->setStyleSheet( ( !tmp1
                                     || m_adcdac->getOverTemperatureState( 1 ) ) ? "color: #f00;" :
                                   ( m_adcdac->getUnderTemperatureState( 1 ) ? "color: #08f;" :
                                     "color: #0c0;" ) );


    float tmp2 = m_adcdac->getInternalTemperature( 2 );
    ui->lblTempCs2->setText( QString::number( tmp2, 'f', 1 ) + " C" );
    ui->lblTempCs2->setStyleSheet( ( !tmp2
                                     || m_adcdac->getOverTemperatureState( 2 ) ) ? "color: #f00;" :
                                   ( m_adcdac->getUnderTemperatureState( 2 ) ? "color: #08f;" :
                                     "color: #0c0;" ) );

    //Analog values
    for( size_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        for( size_t port = 0; port < NR_PORTS; ++port )
        {
            uint16_t value = m_adcdac->getAnalogInputValue( cs, port );
            m_portctrls[cs][port]->updateValue( value );

            bool oc = m_adcdac->getPortOverCurrentState( cs, port );
            m_portctrls[cs][port]->updateOc( oc );
        }
    }

    // Module overcurrents
    ui->lblOCCs0->setText( m_adcdac->getOverCurrentState( 0 ) ? "OC" : "OK" );
    ui->lblOCCs0->setStyleSheet( m_adcdac->getOverCurrentState( 0 ) ? "color: #f00;" :
                                 "color: #0c0;" );

    ui->lblOCCs1->setText( m_adcdac->getOverCurrentState( 1 ) ? "OC" : "OK" );
    ui->lblOCCs1->setStyleSheet( m_adcdac->getOverCurrentState( 1 ) ? "color: #f00;" :
                                 "color: #0c0;" );

    ui->lblOCCs2->setText( m_adcdac->getOverCurrentState( 2 ) ? "OC" : "OK" );
    ui->lblOCCs2->setStyleSheet( m_adcdac->getOverCurrentState( 2 ) ? "color: #f00;" :
                                 "color: #0c0;" );

    // Temperature graph
    ui->widGraph->addTempSamples( tmp0, tmp1, tmp2 );
}


/**************************************************************************************************/
void MaxWidget::clearErrors()
{
    m_adcdac->clearWarnings();
}

/**************************************************************************************************/
void MaxWidget::reconfigure()
{
    m_adcdac->fwReset();

    for( size_t cs = 0; cs < NR_MAX_MODULES; ++cs )
    {
        for( size_t port = 0; port < NR_PORTS; ++port )
        {
            m_portctrls[cs][port]->reinit();
        }
    }
}


/**************************************************************************************************/
void MaxWidget::onStartFwUpdate()
{
    // Disconnect
    m_adcdac.reset();

    FwUpdater fw( m_deviceName );

    fw.exec();
}

#include <chrono>
#include <iostream>
#include <cassert>
#include <QApplication>
#include <QFile>
#include <iostream>
#include "mainwindow.h"


/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QApplication app( argc, argv );
    app.setOrganizationName( "DeLink" );
    app.setOrganizationDomain( "vluchtsimulatie.nl" );
    app.setApplicationName( "ADCDAC" );
    app.setApplicationVersion( SWVERSION_STR );

    QFile styleFile( ":/style.qss" );
    styleFile.open( QFile::ReadOnly );

    QString style( styleFile.readAll() );
    app.setStyleSheet( style );

    MainWindow w;
    w.show();

    return app.exec();
}

#pragma once

#include <QWidget>
#include <QQueue>
#include <QPen>
#include <firmware_globals.h>

class TemperatureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TemperatureWidget( QWidget *parent = nullptr );

    void addTempSamples( float tmp0,
                         float tmp1,
                         float tmp2 );

    void paintEvent( QPaintEvent *event ) override;

private:
    float m_samples[370][NR_MAX_MODULES];

    int m_head;
    int m_tail;

    QPen m_red, m_green, m_blue;
    QFont m_labelfont;
};


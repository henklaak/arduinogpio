#pragma once

#include <QMainWindow>
#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QCheckBox>
#include <QProgressBar>

#include <firmware_globals.h>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget *parent = nullptr );
    ~MainWindow();

public slots:
    void updateGui();
    void updateGenerators();
    void discoverDevices();
    void onIdSelectorChanged( int idSelector );

private:
    Ui::MainWindow *ui;

    QTimer *m_timerUpdateGui = nullptr;
    QTimer *m_timerUpdateGenerators = nullptr;
    QTimer *m_timerDiscoverDevices = nullptr;
};


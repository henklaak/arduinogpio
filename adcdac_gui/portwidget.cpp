#include "portwidget.h"
#include <QPainter>
#include <QKeyEvent>
#include <QInputDialog>
#include <QSettings>
#include <firmware_globals.h>
#include <math.h>
#ifdef WIN32
#include <windows.h>
#else
#include <sys/time.h>

#include <QDir>
#include <QSettings>
#endif

uint8_t initialportmodes[NR_MAX_MODULES][NR_PORTS] =
{
    {
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
    },
    {
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
    },
    {
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
        PORTMODE_NC, PORTMODE_NC, PORTMODE_NC, PORTMODE_NC,
    },
};

static time_t T0 = 0;

/******************************************************************************/
long double GetTimeValue( void )
{
#if WIN32
    _LARGE_INTEGER countnumber;
    _LARGE_INTEGER countfrequency;

    QueryPerformanceCounter( &countnumber );
    QueryPerformanceFrequency( &countfrequency );

    double timesecs = ( double )countnumber.QuadPart / ( double )
                      countfrequency.QuadPart;

    if( T0 == 0 )
    {
        time_t now;
        time( &now );
        T0 = now - ( long )timesecs;
    }

    timesecs += T0;

    return timesecs;
#else
    struct timeval tv;
    gettimeofday( &tv, NULL );
    return 1 * tv.tv_sec + tv.tv_usec * 0.000001;
#endif
}

/**************************************************************************************************/
PortWidget::PortWidget( QWidget *parent,
                        uint8_t cs,
                        uint8_t port )
    : QWidget( parent )
    , m_cs( cs )
    , m_port( port )
    , m_target( 0 )
    , m_amplitude( 256 )
    , m_value( 0 )
    , m_oc( false )
{
    m_portconfig = initialportmodes[m_cs][m_port];
    QSizePolicy sp( QSizePolicy::QSizePolicy::Fixed,
                    QSizePolicy::QSizePolicy::Fixed );
    setSizePolicy( sp );

    setFocusPolicy( Qt::StrongFocus );

    QSettings settings( "pinnames.ini", QSettings::IniFormat );

    int pin_index = m_port + 20 * m_cs;
    QString key = QString( "names/pin_%1_%2" )
                  .arg( int( m_cs ), 2, 10, QChar( '0' ) )
                  .arg( int( m_port ), 2, 10, QChar( '0' ) );
    m_label = settings.value( key, QVariant( m_port ) ).toString();
    settings.setValue( key, m_label );

    m_greyLt = QColor( 192, 192, 192 );
    m_greyDk = QColor( 72, 72, 72 );
    m_greenLt = QColor( 0, 192, 0 );
    m_greenDk = QColor( 0, 72, 0 );
    m_redLt = QColor( 192, 0, 0 );
    m_redDk = QColor( 72, 0, 0 );

    m_penGreyLtThin = QPen( m_greyLt );
    m_penGreyLtThin.setCosmetic( true );

    m_penGreyDkThin = QPen( m_greyDk );
    m_penGreyDkThin.setCosmetic( true );

    m_penGreenLtThin = QPen( m_greenLt );
    m_penGreenLtThin.setCosmetic( true );

    m_greenOverlay = QBrush( QColor( 0, 255, 0, 64 ) );
}

/**************************************************************************************************/
PortWidget::~PortWidget()
{
}

/**************************************************************************************************/
void PortWidget::init()
{
    updateWidgetsFromState();
    emit portConfigChanged( m_cs, m_port, m_portconfig );
}

/**************************************************************************************************/
void PortWidget::reinit()
{
    m_portconfig = initialportmodes[m_cs][m_port];
    m_target = 0;
    updateWidgetsFromState();
    emit portConfigChanged( m_cs, m_port, m_portconfig );
}

/**************************************************************************************************/
bool PortWidget::isOutput() const
{
    return ( m_portconfig == PORTMODE_AO );
}

/**************************************************************************************************/
uint16_t PortWidget::getTarget() const
{
    uint16_t target = 0;
    long double t = GetTimeValue();
    long double arg = 6.283185 * t / m_generatorPeriod;
    double sn = sin( arg );
    double cs = cos( arg );
    double phs = atan2( sn, cs );

    switch( m_generatorMode )
    {
    case GENERATOR_DC:
        target = m_target;
        break;

    case GENERATOR_SINE:
    {
        target = m_target + m_amplitude * sin( phs );
    }
    break;

    case GENERATOR_TRIANGLE:
    {
        target = m_target - m_amplitude + 2 * m_amplitude * abs( phs ) / 3.14159;
    }
    break;

    case GENERATOR_BLOCK:
    {
        double sign = ( phs >= 0 ) ? 1.0 : -1.0;
        target = m_target + m_amplitude * sign;
    }
    break;

    case GENERATOR_PULSE:
    {
        double sign = phs < -2.513 ? 1 : 0; // -0.8 pi = 10% duty cycle
        target = m_target + m_amplitude * sign;
    }
    break;

    }

    return target;
}

/**************************************************************************************************/
void PortWidget::updateValue( uint16_t value )
{
    m_value = value;
    updateWidgetsFromState();
}

/**************************************************************************************************/
void PortWidget::updateOc( bool oc )
{
    m_oc = oc;
    updateWidgetsFromState();
}

/**************************************************************************************************/
void PortWidget::updateWidgetsFromState()
{
}

/**************************************************************************************************/
QSize PortWidget::sizeHint() const
{
    return QSize( 2 * 24, 5 * 24 );
}

/**************************************************************************************************/
void PortWidget::paintEvent( QPaintEvent * )
{
    QPainter painter( this );


    switch( m_portconfig )
    {
    case PORTMODE_AO:
        painter.fillRect( 0, 0, width() - 1, height() - 1, m_oc ? m_redDk : m_greenDk );
        break;

    case PORTMODE_AI:
        painter.fillRect( 0, 0, width() - 1, height() - 1, m_greyDk );
        break;

    case PORTMODE_NC:
        break;
    }


    painter.setPen( m_penGreyLtThin );
    painter.drawLine( 0, 24, 2 * 24, 24 );
    painter.drawText( QRect( 0, 0, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      m_label );

    ///////////

    double w = 0.0;

    switch( m_portconfig )
    {
    case PORTMODE_AI:
        w = m_value / 4096.0;
        painter.fillRect( 0, 24 + 4 * 24 * ( 1 - w ), 2 * 24, 4 * 24 * w, m_greenOverlay );
        break;

    case PORTMODE_AO:
        w = m_value / 4096.0;
        painter.fillRect( 0, 24 + 4 * 24 * ( 1 - w ), 2 * 24, 4 * 24 * w, m_greenOverlay );

        w = m_target / 4096.0;
        painter.setPen( m_penGreenLtThin );
        painter.drawLine( 0, 24 + 4 * 24 * ( 1 - w ), 2 * 24, 24 + 4 * 24 * ( 1 - w ) );
        break;
    }

    QString txt;

    ///////////
    /// actual

    switch( m_portconfig )
    {
    case PORTMODE_NC:
        txt = "";
        break;

    case PORTMODE_AI:
    case PORTMODE_AO:
        switch( m_format )
        {
        case 0:
            txt = QString::number( m_value / 4096.0 * 10.0, 'f', 3 );
            break;

        case 1:
            txt = QString::number( m_value );
            break;

        case 2:
            txt = "0x" + QString::number( m_value, 16 );
            break;
        }

        break;
    }

    painter.setPen( m_penGreyLtThin );
    painter.drawText( QRect( 0, 1 * 24, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      txt );

    ///////////
    /// pattern
    switch( m_portconfig )
    {
    case PORTMODE_NC:
    case PORTMODE_AI:
        txt = "";
        break;

    case PORTMODE_AO:
        switch( m_generatorMode )
        {
        case GENERATOR_DC:
            txt = "dc";
            break;

        case GENERATOR_SINE:
            txt = "sine";
            break;

        case GENERATOR_TRIANGLE:
            txt = "triangle";
            break;

        case GENERATOR_BLOCK:
            txt = "block";
            break;

        case GENERATOR_PULSE:
            txt = "pulse";
            break;
        }

        break;
    }

    painter.setPen( m_penGreenLtThin );
    painter.drawText( QRect( 0, 1.75 * 24, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      txt );


    ///////////
    /// target

    switch( m_portconfig )
    {
    case PORTMODE_NC:
    case PORTMODE_AI:
        txt = "";
        break;

    case PORTMODE_AO:
        switch( m_format )
        {
        case 0:
            txt = QString::number( m_target / 4096.0 * 10.0, 'f', 3 );
            break;

        case 1:
            txt = QString::number( m_target );
            break;

        case 2:
            txt = "0x" + QString::number( m_target, 16 );
            break;
        }

        break;
    }

    painter.setPen( m_penGreenLtThin );
    painter.drawText( QRect( 0, 2.5 * 24, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      txt );


    ///////////
    /// amplitude

    switch( m_portconfig )
    {
    case PORTMODE_NC:
    case PORTMODE_AI:
        txt = "";
        break;

    case PORTMODE_AO:
        switch( m_generatorMode )
        {
        case GENERATOR_DC:
            txt = "";
            break;

        default:
            switch( m_format )
            {
            case 0:
                txt = QString::number( m_amplitude / 4096.0 * 10.0, 'f', 3 );
                break;

            case 1:
                txt = QString::number( m_amplitude );
                break;

            case 2:
                txt = "0x" + QString::number( m_amplitude, 16 );
                break;
            }

            break;
        }

        break;
    }

    painter.setPen( m_penGreenLtThin );
    painter.drawText( QRect( 0, 3.25 * 24, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      txt );


    ///////////
    /// period

    switch( m_portconfig )
    {
    case PORTMODE_NC:
    case PORTMODE_AI:
        txt = "";
        break;

    case PORTMODE_AO:
        switch( m_generatorMode )
        {
        case GENERATOR_DC:
            txt = "";
            break;

        default:
            txt = QString::number( m_generatorPeriod, 'f', 2 ) + " s";
            break;
        }

        break;
    }

    painter.setPen( m_penGreenLtThin );
    painter.drawText( QRect( 0, 4 * 24, 2 * 24, 24 ),
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      txt );


    ///////////

    if( hasFocus() )
    {
        painter.setPen( m_penGreyLtThin );
        painter.drawRect( 0, 0, width() - 1, height() - 1 );
    }

}

/**************************************************************************************************/
void PortWidget::keyPressEvent( QKeyEvent *event )
{
    switch( event->modifiers() )
    {
    case Qt::NoModifier:
    {

        switch( event->key() )
        {
        case Qt::Key_I:
            switch( m_portconfig )
            {
            case PORTMODE_AO:
            case PORTMODE_NC:
                m_portconfig = PORTMODE_AI;
                break;

            case PORTMODE_AI:
                m_portconfig = PORTMODE_NC;
                break;
            }

            emit portConfigChanged( m_cs, m_port, m_portconfig );
            break;

        case Qt::Key_O:
            switch( m_portconfig )
            {
            case PORTMODE_AI:
            case PORTMODE_NC:
                m_target = 0;
                m_portconfig = PORTMODE_AO;
                break;

            case PORTMODE_AO:
                m_portconfig = PORTMODE_NC;
                break;
            }

            emit portConfigChanged( m_cs, m_port, m_portconfig );
            break;

        case Qt::Key_Space:
            m_format = ( m_format + 1 ) % 3;
            update();
            break;

        case Qt::Key_0:
            if( m_portconfig == PORTMODE_AO )
            {
                m_target = 0;
            }

            break;

        case Qt::Key_3:
            if( m_portconfig == PORTMODE_AO )
            {
                m_target = 3.3 / 10.0 * 4096.0;
            }

            break;

        case Qt::Key_5:
            if( m_portconfig == PORTMODE_AO )
            {
                m_target = 5.0 / 10.0 * 4096.0;
            }

            break;

        case Qt::Key_Right:
            focusNextChild();
            break;

        case Qt::Key_Left:
            focusPreviousChild();
            break;

        case Qt::Key_Up:
            m_target = std::min( m_target + 1, 4095 );
            break;

        case Qt::Key_Down:
            m_target = std::max( m_target, ( uint16_t )1 ) - 1;
            break;

        case Qt::Key_PageUp:
            m_target = std::min( m_target + 64, 4095 );
            break;

        case Qt::Key_PageDown:
            m_target = std::max( m_target, ( uint16_t )64 ) - 64;
            break;

        case Qt::Key_Home:
            m_target = 0;
            break;

        case Qt::Key_End:
            m_target = 4095;
            break;

        case Qt::Key_D:
            m_target = 2048;
            m_amplitude = 256;
            m_generatorMode = GENERATOR_DC;
            break;

        case Qt::Key_S:
            m_target = 2048;
            m_amplitude = 256;
            m_generatorMode = GENERATOR_SINE;
            break;

        case Qt::Key_T:
            m_target = 2048;
            m_amplitude = 256;
            m_generatorMode = GENERATOR_TRIANGLE;
            break;

        case Qt::Key_B:
            m_target = 2048;
            m_amplitude = 256;
            m_generatorMode = GENERATOR_BLOCK;
            break;

        case Qt::Key_P:
            m_target = 2048;
            m_amplitude = 256;
            m_generatorMode = GENERATOR_PULSE;
            break;

        case Qt::Key_BracketLeft:
            m_generatorPeriod = std::max( 0.01, m_generatorPeriod - 0.01 );
            break;

        case Qt::Key_BracketRight:
            m_generatorPeriod = m_generatorPeriod + 0.01;
            break;
        }
    }
    break; // nomodifier

    case Qt::ShiftModifier:
    {
        switch( event->key() )
        {
        case Qt::Key_Up:
            m_amplitude = std::min( m_amplitude + 1, 4095 );
            break;

        case Qt::Key_Down:
            m_amplitude = std::max( m_amplitude, ( uint16_t )1 ) - 1;
            break;

        case Qt::Key_PageUp:
            m_amplitude = std::min( m_amplitude + 64, 4095 );
            break;

        case Qt::Key_PageDown:
            m_amplitude = std::max( m_amplitude, ( uint16_t )64 ) - 64;
            break;

        case Qt::Key_Home:
            m_amplitude = 0;
            break;

        case Qt::Key_End:
            m_amplitude = 4095;
            break;
        }
    }
    break;
    }

    QWidget::keyPressEvent( event );
}

/**************************************************************************************************/
void PortWidget::mouseDoubleClickEvent( QMouseEvent * )
{
    // Get current label
    QSettings settings( "pinnames.ini", QSettings::IniFormat );
    QString key = QString( "names/pin_%1_%2" )
                  .arg( int( m_cs ), 2, 10, QChar( '0' ) )
                  .arg( int( m_port ), 2, 10, QChar( '0' ) );
    m_label = settings.value( key, QVariant( m_port ) ).toString();

    QString channel_id = QString( "Channel %1:%2" ).arg( m_cs ).arg( m_port );

    bool ok;
    QString text = QInputDialog::getText( this, "Rename channel",
                                          channel_id, QLineEdit::Normal,
                                          m_label, &ok );

    if( ok )
    {
        m_label = text;
        settings.setValue( key, m_label );
    }
}

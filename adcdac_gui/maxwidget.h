#pragma once

#include <memory>
#include <QWidget>
#include <string>

#include <firmware_globals.h>

class AdcDac;
class PortWidget;

namespace Ui
{
class MaxWidget;
}

class MaxWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MaxWidget( const QString &deviceName, QWidget *parent = nullptr );
    ~MaxWidget();
    void initPorts();

    QString getDeviceName() const;
signals:
    void idSelectorChanged( int idSelector );

public slots:
    void updateGui();
    void updateGenerators();
    void clearErrors();
    void reconfigure();

private slots:
    void onPortConfigChanged( uint8_t cs,
                              uint8_t port,
                              uint8_t portConfig );
    void onStartFwUpdate();

private:
    Ui::MaxWidget *ui = nullptr;
    PortWidget *m_portctrls[NR_MAX_MODULES][NR_PORTS];

    QString m_deviceName;

    std::shared_ptr<AdcDac> m_adcdac;
};


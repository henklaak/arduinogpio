#pragma once

#include <QWidget>
#include <QPen>

enum GeneratorMode
{
    GENERATOR_DC = 0,
    GENERATOR_SINE,
    GENERATOR_TRIANGLE,
    GENERATOR_BLOCK,
    GENERATOR_PULSE,
};


class PortWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PortWidget( QWidget *parent,
                         uint8_t cs,
                         uint8_t port );
    ~PortWidget();

    void init();
    void reinit();
    void updateValue( uint16_t value );
    void updateOc( bool oc );

    bool isOutput() const;
    uint16_t getTarget() const;

signals:
    void portConfigChanged( uint8_t cs,
                            uint8_t port,
                            uint8_t portmode );
protected:
    void paintEvent( QPaintEvent * ) override;
    QSize sizeHint() const override;
    void keyPressEvent( QKeyEvent *event ) override;
    void mouseDoubleClickEvent( QMouseEvent * ) override;

private:
    void updateWidgetsFromState();

    QString m_label;
    uint8_t m_cs;
    uint8_t m_port;
    uint16_t m_target;
    uint16_t m_amplitude;
    uint16_t m_value;
    bool m_oc;
    uint8_t  m_portconfig;

    GeneratorMode m_generatorMode = GENERATOR_DC;
    double m_generatorPeriod = 1.0;

    QColor m_greenLt, m_greenDk, m_redLt, m_redDk, m_greyLt, m_greyDk;
    QPen m_penGreyLtThin, m_penGreyDkThin, m_penGreenLtThin;
    QBrush m_greenOverlay;

    uint8_t m_format = 0;
};


#include "boardlayout.h"
#include <cassert>
#include <QGraphicsSimpleTextItem>

BoardLayout::BoardLayout( QWidget *parent )
    : QGraphicsView( parent )
{
    setRenderHints( QPainter::Antialiasing );
    setupScene();
}

void BoardLayout::resizeEvent( QResizeEvent *event )
{
    QRectF rect = m_scene->itemsBoundingRect();
    fitInView( rect, Qt::KeepAspectRatio );
    QGraphicsView::resizeEvent( event );
}

void BoardLayout::setHighlight( int cs,
                                int pin,
                                bool state )
{
    clearHighlights();
    assert( cs >= 0 && cs <= 2 );
    assert( pin >= 0 && pin <= 19 );
    m_highlights[cs][pin] = state;
    setupScene();
}

void BoardLayout::clearHighlights()
{
    for( int m = 0; m < 3; ++m )
        for( int p = 0; p < 20; ++p )
        {
            m_highlights[m][p] = false;
        }
    setupScene();
}

void BoardLayout::setupScene()
{
    if( m_scene )
    {
        delete m_scene;
    }
    m_scene = new QGraphicsScene( this );
    QGraphicsItem *board = addBoard();

    board->setRotation( 0 );

    m_scene->addItem( board );

    setScene( m_scene );
}

QGraphicsItem *BoardLayout::addBoard( QGraphicsItem *parent )
{
    auto board = new QGraphicsRectItem( QRectF( 0, 0, 198, 104 ), parent );
    board->setPos( QPointF( 20, 20 ) );
    board->setPen( QPen( Qt::NoPen ) );
    board->setBrush( QBrush( QColor( "#014f07" ), Qt::SolidPattern ) );

    auto ps = new QGraphicsRectItem( QRectF( 0, 0, 25.4, 25.4 ), board );
    ps->setPos( QPointF( 35, 3 ) );
    ps->setPen( QPen( Qt::NoPen ) );
    ps->setBrush( QBrush( QColor( "#000000" ), Qt::SolidPattern ) );

    auto sticker = new QGraphicsRectItem( QRectF( 0, 0, 23.4, 10.0 ), ps );
    sticker->setPos( QPointF( 1, 9 ) );
    sticker->setPen( QPen( Qt::NoPen ) );
    sticker->setBrush( QBrush( QColor( "#ff0000" ), Qt::SolidPattern ) );

    auto pwr = new QGraphicsRectItem( QRectF( 0, 0, 12, 16 ), board );
    pwr->setPos( QPointF( 2, 30 ) );
    pwr->setPen( QPen( Qt::NoPen ) );
    pwr->setBrush( QBrush( QColor( "#408040" ), Qt::SolidPattern ) );

    auto usb = new QGraphicsRectItem( QRectF( 0, 0, 15, 12 ), board );
    usb->setPos( QPointF( 2, 55 ) );
    usb->setPen( QPen( Qt::NoPen ) );
    usb->setBrush( QBrush( QColor( "#a0a0a0" ), Qt::SolidPattern ) );

    addMax( board, QPointF( 13, 75 ), 0, false );
    addMax( board, QPointF( 115, 75 ), 1, false );
    addMax( board, QPointF( 198 - 13, 104 - 75 ), 2, true );

    return board;
}

QGraphicsItem *BoardLayout::addMax( QGraphicsItem *parent,
                                    QPointF origin,
                                    int max_nr,
                                    bool flipped )
{
    QPen pen( Qt::NoPen );
    pen.setWidthF( 1.0f );
    pen.setColor( QColor( "#ffffff" ) );
    pen.setCosmetic( true );

    auto max = new QGraphicsRectItem( QRectF( 0, 0, 70.8, 21 ), parent );
    max->setPos( origin );
    max->setPen( pen );
    max->setBrush( QBrush( Qt::NoBrush ) );

    char buf[32];
    snprintf( buf, 32, "%2d", max_nr );
    auto label = new QGraphicsSimpleTextItem( buf, max );

    QFont font;
    font.setFixedPitch( false );
    font.setPixelSize( 12.0 );
    label->setFont( font );
    label->setBrush( QBrush( QColor( "#f0f0f0" ) ) );
    label->setRotation( !flipped ? -90.0 : 90.0 );
    label->setPos( !flipped ? QPointF( 26, 7 ) : QPointF( 44, -6 ) );

    addMolex( max, QPointF( 0, 0 ), max_nr, 0, flipped );
    addMolex( max, QPointF( 0, 16 ), max_nr, 1, flipped );
    addMolex( max, QPointF( 25.4, 16 ), max_nr, 2, flipped );
    addMolex( max, QPointF( 50.8, 16 ), max_nr, 3, flipped );
    addMolex( max, QPointF( 50.8, 0 ), max_nr,  4, flipped );

    if( flipped )
    {
        max->setRotation( 180 );
    }

    return max;
}

QGraphicsItem *BoardLayout::addMolex( QGraphicsItem *parent,
                                      QPointF origin,
                                      int max_nr,
                                      int molex_nr,
                                      bool flipped )
{
    auto molex = new QGraphicsRectItem( QRectF( 0, 0, 20, 5 ), parent );
    molex->setPos( origin );
    molex->setPen( Qt::NoPen );
    molex->setBrush( QBrush( QColor( 0xf0, 0xf0, 0xf0 ), Qt::SolidPattern ) );

    QPen pen( Qt::SolidLine );
    pen.setColor( QColor( "#a0a0a0" ) );
    pen.setWidthF( 1.0 );
    pen.setCosmetic( true );

    auto stub1 = new QGraphicsRectItem( QRectF( 0, 0, 5, 1 ), molex );
    stub1->setPos( 1, 0 );
    stub1->setPen( pen );
    stub1->setBrush( QColor( 0xff, 0xff, 0xff ) );

    auto stub2 = new QGraphicsRectItem( QRectF( 0, 0, 5, 1 ), molex );
    stub2->setPos( 14, 0 );
    stub2->setPen( pen );
    stub2->setBrush( QColor( 0xff, 0xff, 0xff ) );

    for( int i = 0; i <= 7; ++i )
    {
        int pin_nr = -1;
        if( i >= 1 && i <= 4 )
        {
            pin_nr = ( i - 1 ) + 4 * molex_nr;
        }
        addPin( molex, QPointF( 1.25 + 2.5 * i, 2.5 ), max_nr, pin_nr, flipped );
    }

    return molex;
}

QGraphicsItem *BoardLayout::addPin( QGraphicsItem *parent,
                                    QPointF origin,
                                    int max_nr,
                                    int pin_nr,
                                    bool flipped )
{
    bool iopin = ( pin_nr >= 0 );

    auto pin = new QGraphicsRectItem( QRectF( -0.6, -0.6, 1.2, 1.2 ), parent );
    pin->setPos( origin );
    pin->setPen( Qt::NoPen );
    pin->setBrush( QBrush( iopin ? QColor( Qt::black ) : QColor( "#a0a0a0" ), Qt::SolidPattern ) );

    if( pin_nr >= 0 )
    {
        char buf[32];
        snprintf( buf, 32, "%2d", pin_nr );
        auto label = new QGraphicsSimpleTextItem( buf, pin );

        QFont font;
        font.setFixedPitch( false );
        font.setPixelSize( 3.0 );
        label->setFont( font );
        label->setBrush( QBrush( QColor( "#f0f0f0" ) ) );
        label->setRotation( flipped ? 90.0 : -90.0 );
        label->setPos( flipped ? QPointF( 2, -7 ) : QPointF( -2, 7 ) );
    }
    return pin;
}

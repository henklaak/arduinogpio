#include "mainwindow.h"
#include <QTimer>
#include <algorithm>
#include <set>
#include <filesystem>
#include <iostream>

#include "ui_mainwindow.h"
#include "portwidget.h"
#include "timer.h"
#include "maxwidget.h"
#ifdef __linux__
#include "serialcommslinux.h"
#endif
#ifdef _WIN32
#include "serialcommswin.h"
#endif

/**************************************************************************************************/
MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    setWindowTitle( qApp->applicationName() + " " + qApp->applicationVersion() );

    QObject::connect( ui->actionDiscoverDevices, &QAction::triggered,
                      this, &MainWindow::discoverDevices );
    QObject::connect( ui->actionQuit, &QAction::triggered,
                      this, &MainWindow::close );

    m_timerUpdateGui = new QTimer( this );
    QObject::connect( m_timerUpdateGui, &QTimer::timeout,
                      this, &MainWindow::updateGui );
    m_timerUpdateGui->start( 1000.0 * 4/60.0 );

    m_timerUpdateGenerators = new QTimer( this );
    QObject::connect( m_timerUpdateGui, &QTimer::timeout,
                      this, &MainWindow::updateGenerators );
    m_timerUpdateGenerators->start( 10 );

    m_timerDiscoverDevices = new QTimer( this );
    m_timerDiscoverDevices->setSingleShot( true );
    QObject::connect( m_timerDiscoverDevices, &QTimer::timeout,
                      this, &MainWindow::discoverDevices );
    m_timerDiscoverDevices->start( 0 );
}

/**************************************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
}

/**************************************************************************************************/
void MainWindow::updateGui()
{
    if( ui->tabMax->currentWidget() )
    {
        MaxWidget *maxwidget = qobject_cast<MaxWidget *>( ui->tabMax->currentWidget() );
        if( maxwidget )
        {
            qobject_cast<MaxWidget *>( ui->tabMax->currentWidget() )->updateGui();
        }
    }
}

/**************************************************************************************************/
void MainWindow::updateGenerators()
{
    if( ui->tabMax->currentWidget() )
    {
        MaxWidget *maxwidget = qobject_cast<MaxWidget *>( ui->tabMax->currentWidget() );
        if( maxwidget )
        {
            qobject_cast<MaxWidget *>( ui->tabMax->currentWidget() )->updateGenerators();
        }
    }
}

/**************************************************************************************************/
void MainWindow::discoverDevices()
{
    // Get device names in GUI:
    std::set<std::string> usedDevices;

    for( int i = 0; i < ui->tabMax->count(); ++i )
    {
        QWidget *tabWidget = ui->tabMax->widget( i );
        MaxWidget* maxWidget = qobject_cast<MaxWidget *>( tabWidget );
        if( maxWidget )
        {
            usedDevices.insert( maxWidget->getDeviceName().toStdString() );
        }
    }

    std::vector<std::string> availableDevices = findArduinoDeviceNames();

    std::vector<std::string> newDevices;
    std::set_difference( availableDevices.begin(), availableDevices.end(),
                         usedDevices.begin(), usedDevices.end(),
                         std::back_inserter( newDevices ) );

    std::vector<std::string> oldDevices;
    std::set_difference( usedDevices.begin(), usedDevices.end(),
                         availableDevices.begin(), availableDevices.end(),
                         std::back_inserter( oldDevices ) );

    // Remove GUI for old devices
    int i = 0;
    while( i < ui->tabMax->count() )
    {
        QWidget *tabWidget = ui->tabMax->widget( i );
        MaxWidget* maxWidget = qobject_cast<MaxWidget *>( tabWidget );
        if( maxWidget )
        {
            std::string devname = maxWidget->getDeviceName().toStdString();
            if( std::find( oldDevices.begin(), oldDevices.end(), devname ) != oldDevices.end() )
            {
                ui->tabMax->removeTab( i );
                delete maxWidget; //gotcha: removeTab doesn not free widget.
            }
            else
            {
                i++;
            }
        }
        else
        {
            i++;
        }
    }

    // Add GUI for new devices
    for( auto it = newDevices.begin(); it < newDevices.end(); ++it )
    {
        MaxWidget *new_widget = new MaxWidget( QString::fromStdString( *it ) );
        std::string devname = new_widget->getDeviceName().toStdString();
        std::filesystem::path a( devname );

        QObject::connect( new_widget, &MaxWidget::idSelectorChanged,
                          this, &MainWindow::onIdSelectorChanged );
        QObject::connect( ui->actionClearErrors, &QAction::triggered,
                          new_widget, &MaxWidget::clearErrors );
        QObject::connect( ui->actionReset, &QAction::triggered,
                          new_widget, &MaxWidget::reconfigure );

        int pagenr = ui->tabMax->addTab( new_widget, QString::fromStdString( a.filename().string() ) + " : ?" );
        ui->tabMax->setCurrentIndex(pagenr);
    }

    //Reschedule discovery
    m_timerDiscoverDevices->start( 1000 );
}

/**************************************************************************************************/
void MainWindow::onIdSelectorChanged( int idSelector )
{
    MaxWidget * widget = qobject_cast<MaxWidget *>( sender() );
    int idx = ui->tabMax->indexOf( widget );

    std::string devname = widget->getDeviceName().toStdString();
    std::filesystem::path a( devname );

    ui->tabMax->setTabText( idx,  QString::fromStdString( a.filename().string() ) + " : " +
                            ( ( idSelector >= 0 ) ? QString::number( idSelector ) : "?" ) );
}

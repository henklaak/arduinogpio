#include "fwupdater.h"
#include <filesystem>
#include <iostream>
#include <QDir>
#include <QMessageBox>
#include <QTimer>
#include "ui_fwupdater.h"
#include "serialcomms.h"
#include "timer.h"
#ifdef _WIN32
#include <windows.h>
#endif

/******************************************************************************/
FwUpdater::FwUpdater( const QString &deviceName, QWidget *parent )
    : QDialog( parent )
    , ui( new Ui::FwUpdater )
    , m_initialDeviceName( deviceName )
{
    ui->setupUi( this );
    std::filesystem::path a( deviceName.toStdString() );
    ui->lblInitialDeviceName->setText( QString::fromStdString( a.filename().string() ) );
    ui->lblUploadDeviceName->setText( "-" );

    ui->lblState->setText( "Idle" );
    QObject::connect( this, &FwUpdater::stateChanged,
                      this, &FwUpdater::onStateChanged, Qt::QueuedConnection );

    updateFw();
}

/******************************************************************************/
FwUpdater::~FwUpdater()
{
    delete ui;
}

/******************************************************************************/
void FwUpdater::setState( FwUpdater::FwUpdateState state )
{
    if( m_state != state )
    {
        m_state = state;
        emit stateChanged();
    }
}

/******************************************************************************/
void FwUpdater::updateFw()
{
    if( m_state == STATE_IDLE )
    {
        m_state = STATE_CLEAR;
        emit stateChanged();
    }
}

/******************************************************************************/
void FwUpdater::onStateChanged()
{
    switch( m_state )
    {
    case STATE_IDLE:
        ui->lblState->setText( "Idle" );
        break;

    case STATE_CLEAR:
        ui->lblState->setText( "Clear" );
        clearDevice();
        break;

    case STATE_FIND_DEVICE:
        ui->lblState->setText( "Find device" );
        findDevice();
        break;

    case STATE_UPLOAD:
        ui->lblState->setText( "Upload" );
        uploadDevice();
        break;

    case STATE_RESTART:
        qApp->quit();
        QProcess::startDetached( qApp->arguments()[0], qApp->arguments() );
        break;

    default:
        ui->lblState->setText( "?" );
        setState( STATE_IDLE );
        break;
    }
}

/******************************************************************************/
/******************************************************************************/
void FwUpdater::clearDevice()
{
#ifdef _WIN32
    HANDLE hComm = CreateFileA( m_initialDeviceName.toStdString().c_str(),
                                GENERIC_READ | GENERIC_WRITE,
                                0,
                                NULL,
                                OPEN_EXISTING,
                                0,
                                NULL );

    if( hComm != INVALID_HANDLE_VALUE )
    {
        DCB dcb;
        GetCommState( hComm, &dcb );
        dcb.BaudRate = 1200;
        dcb.ByteSize = 8;
        dcb.StopBits = ONESTOPBIT;
        dcb.Parity = NOPARITY;
        SetCommState( hComm, &dcb );

        CloseHandle( hComm );
        QTimer::singleShot( 2000, this, &FwUpdater::onClearDeviceFinished );
    }
    else
    {
        QMessageBox::critical( this, "Error", "Device " + m_initialDeviceName + " could not be opened." );
        close();
    }

#else
    QMessageBox::critical( this, "Error", "Not implemented for linux" );
#endif
}

/******************************************************************************/
void FwUpdater::onClearDeviceFinished()
{
    setState( STATE_FIND_DEVICE );
}

/******************************************************************************/
/******************************************************************************/
void FwUpdater::findDevice()
{
    m_uploadDeviceName = "";
    ui->lblUploadDeviceName->setText( "-" );
    //ui->txtOutput->clear();

    m_stdout.clear();
    m_stderr.clear();
    m_process.reset( new QProcess( nullptr ) );
    QObject::connect( m_process.data(), &QProcess::finished,
                      this, &FwUpdater::onFindDeviceFinished );
    QObject::connect( m_process.data(), &QProcess::errorOccurred,
                      this, &FwUpdater::onFindDeviceErrorOccurred );
    QObject::connect( m_process.data(), &QProcess::readyReadStandardOutput,
                      this, &FwUpdater::onFindDeviceReadyReadStandardOutput );
    QObject::connect( m_process.data(), &QProcess::readyReadStandardError,
                      this, &FwUpdater::onFindDeviceReadyReadStandardError );

    QString program =  QDir( QApplication::applicationDirPath() ).filePath( "bossac.exe" );
    QStringList arguments = {"-i"};

    if( !std::filesystem::is_regular_file( std::filesystem::path( program.toStdString() ) ) )
    {
        setState( STATE_IDLE );
        QMessageBox::critical( this, "Error", "Cannot find bossac.exe" );
        close();
    }

    m_process->start( program, arguments );
}

/******************************************************************************/
void FwUpdater::onFindDeviceReadyReadStandardOutput()
{
    m_stdout += m_process->readAllStandardOutput();
    ui->txtOutput->setPlainText( m_stdout );
}

/******************************************************************************/
void FwUpdater::onFindDeviceReadyReadStandardError()
{
    m_stdout += m_process->readAllStandardError();
    ui->txtOutput->setPlainText( m_stdout );
}

/******************************************************************************/
void FwUpdater::onFindDeviceErrorOccurred( QProcess::ProcessError error )
{
    setState( STATE_IDLE );
}

/******************************************************************************/
void FwUpdater::onFindDeviceFinished( int exitCode, QProcess::ExitStatus exitStatus )
{
    if( exitCode != 0 || exitStatus != QProcess::NormalExit )
    {
        setState( STATE_IDLE );
        QMessageBox::critical( this, "Error", "No cleared device found." );
        close();
    }
    else
    {
        QString output = QString( m_stdout );
        QStringList lines = output.split( "\r\n" );
        QString line = lines[0];

        QStringList words = line.split( " " );

        for( int i = 0; i < words.length(); ++i )
        {
            QString word = words[i];
            int idx = word.indexOf( "COM" );

            if( idx == 0 )
            {
                m_uploadDeviceName = word;
                ui->lblUploadDeviceName->setText( m_uploadDeviceName );
            }
        }

        setState( STATE_UPLOAD );
    }
}


/******************************************************************************/
/******************************************************************************/
void FwUpdater::uploadDevice()
{
    m_stdout.clear();
    m_stderr.clear();
    m_process.reset( new QProcess( nullptr ) );
    QObject::connect( m_process.data(), &QProcess::finished,
                      this, &FwUpdater::onUploadDeviceFinished );
    QObject::connect( m_process.data(), &QProcess::errorOccurred,
                      this, &FwUpdater::onUploadDeviceErrorOccurred );
    QObject::connect( m_process.data(), &QProcess::readyReadStandardOutput,
                      this, &FwUpdater::onUploadDeviceReadyReadStandardOutput );
    QObject::connect( m_process.data(), &QProcess::readyReadStandardError,
                      this, &FwUpdater::onUploadDeviceReadyReadStandardError );

    QString program =  QDir( QApplication::applicationDirPath() ).filePath( "bossac.exe" );
    QString binary =  QDir( QApplication::applicationDirPath() ).filePath( "adcdac_firmware.ino.bin" );

    if( !std::filesystem::is_regular_file( std::filesystem::path( program.toStdString() ) ) )
    {
        setState( STATE_IDLE );
        QMessageBox::critical( this, "Error", "Cannot find bossac.exe" );
        close();
    }

    if( !std::filesystem::is_regular_file( std::filesystem::path( binary.toStdString() ) ) )
    {
        setState( STATE_IDLE );
        QMessageBox::critical( this, "Error", "Cannot find " + binary );
        close();
    }

    QStringList arguments = { "--port=" + m_uploadDeviceName, "--force_usb_port=true", "-e", "-w", "-v", binary, "-R"};
    m_process->start( program, arguments );
}

/******************************************************************************/
void FwUpdater::onUploadDeviceReadyReadStandardOutput()
{
    m_stdout += m_process->readAllStandardOutput();
    ui->txtOutput->setPlainText( m_stdout );
}

/******************************************************************************/
void FwUpdater::onUploadDeviceReadyReadStandardError()
{
    m_stdout += m_process->readAllStandardError();
    ui->txtOutput->setPlainText( m_stdout );
}

/******************************************************************************/
void FwUpdater::onUploadDeviceErrorOccurred( QProcess::ProcessError error )
{
    setState( STATE_IDLE );
}

/******************************************************************************/
void FwUpdater::onUploadDeviceFinished( int exitCode, QProcess::ExitStatus exitStatus )
{
    if( exitCode != 0 || exitStatus != QProcess::NormalExit )
    {
        setState( STATE_IDLE );
        QMessageBox::critical( this, "Error", "Upload failed." );
        close();
    }
    else
    {
        setState( STATE_IDLE );
        QMessageBox::information( this, "Success", "Upload successful." );
        close();
    }
}

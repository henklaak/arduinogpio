#pragma once

#include <set>
#include <QDialog>
#include <QProcess>

namespace Ui
{
class FwUpdater;
}

class FwUpdater : public QDialog
{
    Q_OBJECT

public:
    explicit FwUpdater( const QString &deviceName, QWidget *parent = nullptr );
    ~FwUpdater();

signals:
    void stateChanged();

private slots:
    void updateFw();
    void onStateChanged();

    void onClearDeviceFinished();

    void onFindDeviceFinished( int exitCode, QProcess::ExitStatus exitStatus );
    void onFindDeviceErrorOccurred( QProcess::ProcessError error );
    void onFindDeviceReadyReadStandardOutput();
    void onFindDeviceReadyReadStandardError();

    void onUploadDeviceFinished( int exitCode, QProcess::ExitStatus exitStatus );
    void onUploadDeviceErrorOccurred( QProcess::ProcessError error );
    void onUploadDeviceReadyReadStandardOutput();
    void onUploadDeviceReadyReadStandardError();

private:
    enum FwUpdateState
    {
        STATE_IDLE,
        STATE_CLEAR,
        STATE_FIND_DEVICE,
        STATE_UPLOAD,
        STATE_RESTART
    };

    void setState( FwUpdateState state );
    void clearDevice();
    void findDevice();
    void uploadDevice();


    Ui::FwUpdater *ui;
    QString m_initialDeviceName;
    QString m_uploadDeviceName;
    QString m_finalDeviceName;


    FwUpdateState m_state = STATE_IDLE;
    QSharedPointer<QProcess> m_process;
    QByteArray m_stdout;
    QByteArray m_stderr;
};


cmake_minimum_required(VERSION 3.20)

project(adcdac LANGUAGES CXX)

set(FWVERSION_MAJOR 2022)
set(FWVERSION_MINOR 11)
set(FWVERSION_PATCH 20)
set(FWVERSION_BUILD 0)

set(SWVERSION_MAJOR 2022)
set(SWVERSION_MINOR 11)
set(SWVERSION_PATCH 27)
set(SWVERSION_BUILD 0)

include(CTest)
enable_testing()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_LIST_DIR}/installer/packages/nl.delink.adcdac/data)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_subdirectory(adcdac_firmware)
add_subdirectory(adcdac_library)
add_subdirectory(adcdac_gui)
add_subdirectory(adcdac_xplane_plugin)
add_subdirectory(g1000_positioner)
add_subdirectory(force_diagnose)
add_subdirectory(installer)

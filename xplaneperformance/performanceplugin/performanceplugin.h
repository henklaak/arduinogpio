#ifndef PERFORMANCEPLUGIN_H
#define PERFORMANCEPLUGIN_H
#include <string>

void log(const std::string &txt);

void errorCallback( const char *msg );
void PerformancePluginMenuHandler( void *mRef, void *iRef );
float PerformancePluginFlightLoopCallback(
    float inElapsedSinceLastCall,
    float inElapsedTimeSinceLastFlightLoop,
    int   inCounter,
    void *inRefcon );

#endif

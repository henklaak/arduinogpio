SET (TARGET performanceplugin)

if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
    find_program(XPLANE_PROGRAM REQUIRED
        NAMES "X-Plane" "X-Plane-x86_64"
        NO_DEFAULT_PATH
        PATHS
            "/mnt/lvm/HDD/SteamLibrary/steamapps/common"
            "/mnt/lvm/HDD/SteamLibrary/steamapps/common/X-Plane 11"
    )

endif()

if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
    find_program(XPLANE_PROGRAM REQUIRED
        NAMES "X-Plane.exe" "X-Plane-x86_64.exe"
        NO_DEFAULT_PATH
        PATHS
            "C:/Program Files (x86)/Steam/steamapps/common/X-Plane 11"
            "D:/X-Plane 11"
    )
endif()


cmake_path(GET XPLANE_PROGRAM PARENT_PATH XPLANE_DIRECTORY)
cmake_path(APPEND XPLANE_DIRECTORY "Resources" "plugins" "Performance" "64" OUTPUT_VARIABLE XPLANE_PLUGIN_DIR)

message("X-Plane found in: \"" ${XPLANE_DIRECTORY} "\"")

include_directories(SDK/CHeaders/XPLM)
include_directories(SDK/CHeaders/Widgets)

add_library(${TARGET} SHARED
    performanceplugin.cpp
    performanceplugin.h
)
set_target_properties(${TARGET} PROPERTIES PREFIX "")
set_target_properties(${TARGET} PROPERTIES SUFFIX ".xpl")
target_compile_definitions(${TARGET} PRIVATE XPLM200 XPLM210 XPLM300 XPLM301 XPLM302 XPLM303)

if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
    target_compile_definitions(${TARGET} PRIVATE LIN)
    set_target_properties(${TARGET} PROPERTIES OUTPUT_NAME "lin")
endif()

if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
    target_compile_definitions(${TARGET} PRIVATE IBM)
    set_target_properties(${TARGET} PROPERTIES OUTPUT_NAME "win")
endif()

install(TARGETS ${TARGET} DESTINATION ${XPLANE_PLUGIN_DIR})

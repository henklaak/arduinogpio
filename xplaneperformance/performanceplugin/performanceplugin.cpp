//#include "performanceplugin.h"
//#if IBM
//#include <windows.h>
//#endif
//#include <string>
//#include <string.h>
//#include <stdio.h>
#include "performanceplugin.h"
#include <XPLMDataAccess.h>
#include <XPLMDisplay.h>
#include <XPLMMenus.h>
#include <XPLMProcessing.h>
#include <XPLMPlugin.h>
#include <XPUIGraphics.h>
#include <XPLMUtilities.h>
#include <XPLMGraphics.h>
#include <string.h>
#include <filesystem>
#include <fstream>

//#include "data_cmd.h"
//#include "debugwindow.h"

XPLMWindowID s_debug_window;

XPLMMenuID s_max_menu_id;

XPLMCommandRef cmdQuit;
XPLMDataRef refFps;

float fps_cum = 0;
int fps_count = 0;

/**********************************************************************/
void log( const std::string &txt )
{
    XPLMDebugString( ( "com.delink.performance: " + txt + "\n" ).c_str() );
}

/**********************************************************************/
PLUGIN_API int XPluginStart( char *outName,
                             char *outSig,
                             char *outDesc )
{
    log( "Starting plugin..." );

    strncpy( outName, "PerformancePlugin", 255 );
    strncpy( outSig, "com.delink.performance", 255 );
    strncpy( outDesc, "PerformancePlugin", 255 );

    XPLMSetErrorCallback( errorCallback );

    int item;
    item = XPLMAppendMenuItem( XPLMFindPluginsMenu(), "PerformancePlugin", nullptr, 1 );
    s_max_menu_id = XPLMCreateMenu( "PerformancePlugin", XPLMFindPluginsMenu(), item,
                                    PerformancePluginMenuHandler, nullptr );
    XPLMAppendMenuItem( s_max_menu_id, "Reload", ( void * )"Reload", 1 );

    log( "Started plugin" );

    return 1; // SUCCESS
}

/**********************************************************************/
PLUGIN_API void XPluginStop()
{
    log( "Stopping plugin..." );

    XPLMDestroyMenu( s_max_menu_id );

    log( "Stopped plugin" );
}

int cmdPauseHandler(
    XPLMCommandRef       inCommand,
    XPLMCommandPhase     inPhase,
    void *               inRefcon )
{
    char buf[256];
    snprintf( buf, 256, "Paused %d", inPhase );
    log( buf );
    return 1;
}

/**********************************************************************/
PLUGIN_API int XPluginEnable()
{
    log( "Enabling plugin..." );

    XPLMCreateFlightLoop_t fl;
    fl.structSize = sizeof( XPLMCreateFlightLoop_t );
    fl.callbackFunc = PerformancePluginFlightLoopCallback;
    fl.phase = xplm_FlightLoop_Phase_AfterFlightModel;
    fl.refcon = nullptr;

    XPLMFlightLoopID loopid = XPLMCreateFlightLoop( &fl );

    XPLMScheduleFlightLoop( loopid, -1, 0 ); //-1 call every frame

    cmdQuit = XPLMFindCommand( "sim/operation/quit" );
    refFps = XPLMFindDataRef( "sim/time/framerate_period" );

    fps_cum = 0;
    fps_count = 0;
    log( "Enabled plugin" );

    return 1; // SUCCESS
}

/**********************************************************************/
PLUGIN_API void XPluginDisable()
{
    log( "Disabling plugin..." );

    XPLMUnregisterFlightLoopCallback( PerformancePluginFlightLoopCallback, nullptr );

    log( "Disabled plugin" );
}

/**********************************************************************/
PLUGIN_API void XPluginReceiveMessage( XPLMPluginID inFromWho,
                                       int inMessage,
                                       void */*inParam*/ )
{
//    char buf[1024];
//    snprintf( buf, 1024, "inFromWho: %d", inFromWho );
//    log( buf );
//    snprintf( buf, 1024, "inMessage: %d", inMessage );
//    log( buf );

    switch( inMessage )
    {
    case XPLM_MSG_PLANE_CRASHED:
    case XPLM_MSG_PLANE_LOADED:
    case XPLM_MSG_AIRPORT_LOADED:
    case XPLM_MSG_SCENERY_LOADED:
    case XPLM_MSG_AIRPLANE_COUNT_CHANGED:
    case XPLM_MSG_PLANE_UNLOADED:
    case XPLM_MSG_WILL_WRITE_PREFS:
    case XPLM_MSG_LIVERY_LOADED:
    case XPLM_MSG_ENTERED_VR:
    case XPLM_MSG_EXITING_VR:
    case XPLM_MSG_RELEASE_PLANES:
    default:
        break;
    }
}

/**********************************************************************/
void errorCallback( const char *msg )
{
    char buf[1024];
    snprintf( buf, 1024, "ErrorCallback: %s", msg );
    log( buf );
}

/**********************************************************************/
void PerformancePluginMenuHandler( void */*mRef*/, void *iRef )
{
    if( !strncmp( ( char * )iRef, "Reload", 6 ) )
    {
        XPLMReloadPlugins();
    }
}

/**********************************************************************/
float PerformancePluginFlightLoopCallback(
    float /*inElapsedSinceLastCall*/,
    float /*inElapsedTimeSinceLastFlightLoop*/,
    int   inCounter,
    void */*inRefcon*/ )
{
    if( inCounter >= 2500 && inCounter < 4500 )
    {
        float fps = XPLMGetDataf( refFps );
        fps_cum += fps;
        fps_count++;
    }

    if( inCounter >= 4500 )
    {
        char path[256];
        XPLMGetPrefsPath( path );
        XPLMExtractFileAndPath(path);
        log(path);

        std::filesystem::path base( path );
        std::filesystem::path p = base / std::filesystem::path( "performance.txt" );
        log(p);

        std::ofstream newfile;

        char buf[256];
        snprintf( buf, 256, "Time:\t%.3f\tSamples:\t%d", fps_cum, fps_count );
        log( buf );

        newfile.open( p );
        newfile << buf << std::endl;

        newfile.close();

        XPLMCommandOnce( cmdQuit );
    }
    return -1; // Call in 1 frame
}

import os
import subprocess
from time import sleep

XPLANE_EXE = r"/mnt/lvm/HDD/SteamLibrary/steamapps/common/X-Plane 11/X-Plane-x86_64"
PERF_FILE = r"/mnt/lvm/HDD/SteamLibrary/steamapps/common/X-Plane 11/Output/preferences/performance.txt"
CONF_FILE = r"/mnt/lvm/HDD/SteamLibrary/steamapps/common/X-Plane 11/Output/preferences/X-Plane.prf"


def configure_xplane(sw=0):
    with open(CONF_FILE, 'rt') as fp:
        lines = fp.readlines()

    fixes = {"_tex_res": "5",  # 1 - 5, no fps effect
             "renopt_scenery_shadows": "1",  # 0,1
             "renopt_static_acf": "1",  # 0, 1
             "renopt_HDR_antial": "5",  # 0 - 5
             "_aniso_filter": "16",  # 1, 2, 4, 8, 16
             "renopt_boats": "1",  # 0,1
             "renopt_deer_birds": "1",  # 0,1
             "renopt_wat_05": "1",  # 0 - 5
             "renopt_effects_04": "3",  # 0 - 4
             "renopt_comp_texes": "1",  # 0,1
             "renopt_draw_3d_04": "2", ## int(sw),  # 0 - 4
             }

    newlines = []
    for line in lines:
        fixed = False
        for k, v in fixes.items():
            if line.startswith(k):
                newlines.append(f"{k} {v}\n")
                print(f"{k} {v}")
                fixed = True

        if not fixed:
            newlines.append(line)

    with open(CONF_FILE, 'wt') as fp:
        fp.writelines(newlines)


def run_xplane_cycle():
    cmdline = [XPLANE_EXE]
    output = subprocess.run(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_lines = output.stdout.decode('utf-8').splitlines()
    stderr_lines = output.stderr.decode('utf-8').splitlines()
    output.returncode


def measure_performance():
    if os.path.isfile(PERF_FILE):
        os.remove(PERF_FILE)
    run_xplane_cycle()
    while not os.path.isfile(PERF_FILE):
        sleep(1)

    with open(PERF_FILE, "rt") as fp:
        line = fp.readline().strip()
        elems = line.split("\t")

    return float(elems[3]) / float(elems[1])


if __name__ == '__main__':
    for sw in (0, 1, 2, 3, 4):
        configure_xplane(sw=sw)
        fps = measure_performance()
        print(f"------------- fps={fps:.1f}")

    print("Done")
